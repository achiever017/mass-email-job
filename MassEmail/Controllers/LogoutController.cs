﻿using MassEmail.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace MassEmail.Controllers
{
    public class LogoutController : ApiController
    {
        SIContext db = new SIContext();
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public HttpResponseMessage Post(LoginModel data)
        {
            try {
                String email = data.email;
                String password = data.password;
                
                    try
                {
                    if (!(email == string.Empty))
                    {
                        if (!(password == string.Empty))
                        {
                            String query = "select * from ApplicationUser where email = '" + email + "' and password = '" + password + "'";
                            SqlConnection con = new SqlConnection(getConStr("SIConnectionString"));
                            SqlConnection con2 = new SqlConnection(getConStr("SIConnectionString"));
                            SqlCommand cmd = new SqlCommand(query, con);
                            SqlDataReader dbr;
                            con.Open();
                            dbr = cmd.ExecuteReader();
                            int count = 0;
                            while (dbr.Read())
                            {
                                count = count + 1;
                            }
                            con.Close();
                            if (count == 1)
                            {
                                SqlCommand cmd2 = new System.Data.SqlClient.SqlCommand();
                                cmd2.CommandType = System.Data.CommandType.Text;
                                cmd2.CommandText = "UPDATE ApplicationUser SET [deviceID] = @deviceID WHERE [email] = @email AND [password] = @password";
                                cmd2.Parameters.AddWithValue("@deviceID", "");
                                cmd2.Parameters.AddWithValue("@email", data.email);
                                cmd2.Parameters.AddWithValue("@password", data.password);
                                cmd2.Connection = con2;

                                con2.Open();
                                cmd2.ExecuteNonQuery();
                                con2.Close();
                                return CreateJsonError(HttpStatusCode.Accepted, 1, "");
                            }
                        }
                    }
                    
                    // con.Close();

                }
                catch (Exception es)
                {
                    return CreateJsonError(HttpStatusCode.Accepted, 500, "Some thing wrong "+getConStr("SIConnectionString") +" - "+ es);
                }

              

                // code here
            }
            catch (SqlException odbcEx)
            {
                return CreateJsonError(HttpStatusCode.Accepted, 400, "Wrong login or passwo" + odbcEx);
                // Handle more specific SqlException exception here.
            }
            catch (Exception ex)
            {
                return CreateJsonError(HttpStatusCode.Accepted, 400, "Wrong login or passwo" + ex);
                // Handle generic ones here.
            }
            
            return CreateJsonError(HttpStatusCode.Accepted, 400, "Wrong login or passwo" + data.email);
        }
        private HttpResponseMessage CreateJsonError()
        {
            throw new NotImplementedException();
        }

        public String getConStr(String str)
        {
            System.Configuration.Configuration rootWebConfig =
                System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("/MyWebSiteRoot");
            System.Configuration.ConnectionStringSettings connString;
            if (rootWebConfig.ConnectionStrings.ConnectionStrings.Count > 0)
            {
                connString =
                    rootWebConfig.ConnectionStrings.ConnectionStrings[str];
                if (connString != null)
                    return connString.ConnectionString;
                else
                    Console.WriteLine("No Northwind connection string");
                return null;
            }
            return null;
        }

        public HttpResponseMessage CreateJsonError(HttpStatusCode status, int code, string message)
        {
            ResponseCode error = new ResponseCode();
            error.response_code = code;
            error.message = message;

            string json = string.Empty;
            HttpContext.Current.Request.InputStream.Position = 0;
            json = new StreamReader(HttpContext.Current.Request.InputStream).ReadToEnd();

            return Request.CreateResponse(status, error, this.Configuration);
        }

        public class ResponseCode
        {
            private int myVal = 1;

            public int response_code { get { return myVal; } set { myVal = value; } }
            public string message { get; set; }
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}