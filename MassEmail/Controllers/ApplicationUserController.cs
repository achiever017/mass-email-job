using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using MassEmail.Models;
using System.ComponentModel;
using System.Web.UI;
using ClosedXML.Excel;

namespace MassEmail.Controllers
{
    public class ApplicationUserController : Controller
    {
        // GET: /ApplicationUser/ suppose we want to remove ema�l and user name s�mply remove �t
        public ActionResult Index()
        {
            return ModelBind();
        }

        // var result = from c in tak select new string[] { c.Id.ToString(), Convert.ToString(c.Email), Convert.ToString(c.Username), Convert.ToString(c.FirstName), Convert.ToString(c.LastName), Convert.ToString(c.Department_DepartmentId != null ? c.Department_DepartmentId.Title : ""), Convert.ToString(c.IsActive), Convert.ToString(c.Supervisior) };

        public ActionResult GetGrid()
        {
            var tak = db.ApplicationUsers.ToArray();

            var result = from c in tak select new string[] { c.Id.ToString(), Convert.ToString(c.FirstName), Convert.ToString(c.LastName), Convert.ToString(c.Department_DepartmentId != null ? c.Department_DepartmentId.Title : ""), Convert.ToString(c.IsActive), Convert.ToString(c.Supervisior) };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        // Action of Export grid
        public ActionResult GetExcel()
        {
            var tak = db.ApplicationUsers.Select(x => new
            {
                ID = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                DepartmentID = x.DepartmentId,
                title = x.Department_DepartmentId.Title,
                active = x.IsActive,
                super = x.Supervisior
            }).ToList();

            List<execllist> el = new List<execllist>();
            foreach(var item in tak)
            {
                el.Add(new execllist
                {
                    ID = item.ID.ToString(),
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    DepartmentID = item.DepartmentID.ToString(),
                    title = item.title,
                    active = item.active.ToString(),
                    super = item.super != null ? "" : ""
                });
           
            }
            
            DataTable dt = new DataTable();
            dt.TableName = "Result";
            dt = el.ToDataTable();



            using (XLWorkbook wb = new XLWorkbook())
            {

                wb.Worksheets.Add(dt, "1");
                wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                wb.Style.Font.Bold = true;


                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename= Rapor.xlsx");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }  

            var result =Response;
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index", "GetGrid");
        }
     
        // GET: /ApplicationUser/ModelBindIndex
        public ActionResult ModelBindIndex()
        {
            return ModelBind();
        }
        // GET: /ApplicationUser/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser ObjApplicationUser = db.ApplicationUsers.Find(id);
            if (ObjApplicationUser == null)
            {
                return HttpNotFound();
            }
            return View(ObjApplicationUser);
        }

      
        // GET: /ApplicationUser/Create
        public ActionResult Create()
        {
            ViewBag.RoleId = new SelectList(db.Roles, "Id", "RoleName");
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Title");

            return View();
        }

        // POST: /ApplicationUser/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(ApplicationUser ObjApplicationUser, int? RoleId)
        {
            if (ModelState.IsValid)
            {
                if (!String.Equals(ObjApplicationUser.Username.Trim(), ObjApplicationUser.Email.Trim(),
                    StringComparison.OrdinalIgnoreCase))
                {
                    ModelState.AddModelError("Email", "Email adresleri birbiriyle uyu�muyor.");
                   
                }
                else //
                {
                    if (RoleId.HasValue && RoleId.Value > 0)
                    {
                        if (ObjApplicationUser.DepartmentId.HasValue && ObjApplicationUser.DepartmentId.Value > 0)
                        {

                            db.ApplicationUsers.Add(ObjApplicationUser);

                            RoleUser rol = new RoleUser();
                            rol.RoleId = RoleId;
                            rol.UserId = ObjApplicationUser.Id;
                            db.RoleUsers.Add(rol);
                            db.SaveChanges();


                            return RedirectToAction("Index");
                        }
                        else
                        {
                            ModelState.AddModelError("Department", "Kurum Zorunlu.");

                        }
                    }
                    else
                    {
                        ModelState.AddModelError("Role", "Rol Zorunlu.");
                    }
                }
            }
            ViewBag.RoleId = new SelectList(db.Roles, "Id", "RoleName", RoleId);
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Title", ObjApplicationUser.DepartmentId);

            return View(ObjApplicationUser);
        }

        //public ActionResult Create()
        //{
        //    ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Title");

        //    return View();
        //}

        //// POST: /ApplicationUser/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //[ValidateInput(false)]
        //public ActionResult Create(ApplicationUser ObjApplicationUser)
        //{
        //    if (ModelState.IsValid)
        //    {


        //        db.ApplicationUsers.Add(ObjApplicationUser);
        //        db.SaveChanges();
        //        return Redirect(TempData["ThisUrl"].ToString());
        //    }
        //    ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Title", ObjApplicationUser.DepartmentId);

        //    return View(ObjApplicationUser);
        //}















        // GET: /ApplicationUser/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser ObjApplicationUser = db.ApplicationUsers.Find(id);
            if (ObjApplicationUser == null)
            {
                return HttpNotFound();
            }
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Title", ObjApplicationUser.DepartmentId);

            return View(ObjApplicationUser);
        }

        // POST: /ApplicationUser/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(ApplicationUser ObjApplicationUser)
        {
            if (ModelState.IsValid)
            {


                db.Entry(ObjApplicationUser).State = EntityState.Modified;
                db.SaveChanges();
                return Redirect(TempData["ThisUrl"].ToString());
            }
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Title", ObjApplicationUser.DepartmentId);

            return View(ObjApplicationUser);
        }
        // GET: /ApplicationUser/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser ObjApplicationUser = db.ApplicationUsers.Find(id);
            if (ObjApplicationUser == null)
            {
                return HttpNotFound();
            }
            return View(ObjApplicationUser);
        }

        // POST: /ApplicationUser/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ApplicationUser ObjApplicationUser = db.ApplicationUsers.Find(id);
            BackupApplicationUser ba = new BackupApplicationUser()
            {
                DeleteOn = DateTime.Now,
                DeletedBy = Env.GetUserInfo("name"),
                DepartmentId = ObjApplicationUser.DepartmentId,
                CanEmail = ObjApplicationUser.CanEmail,
                CanMessage = ObjApplicationUser.CanMessage,
                Email = ObjApplicationUser.Email,
                FirstName = ObjApplicationUser.FirstName,
                IsActive = ObjApplicationUser.IsActive,
                LastName = ObjApplicationUser.LastName,
                Password = ObjApplicationUser.Password,
                Username = ObjApplicationUser.Username

            };
            db.BackupApplicationUsers.Add(ba);
            db.ApplicationUsers.Remove(ObjApplicationUser);
            db.SaveChanges();
            TempData["Message"]="Ba�ar�yla Silindi";
            return RedirectToAction("Index");
        }
        // GET: /ApplicationUser/MultiViewIndex/5
        public ActionResult MultiViewIndex(int? id)
        {
            ApplicationUser ObjApplicationUser = db.ApplicationUsers.Find(id);
            ViewBag.IsWorking = 0;
            if (id > 0)
            {
                ViewBag.IsWorking = 1;
                ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Title", ObjApplicationUser.DepartmentId);

            }

            return View(ObjApplicationUser);
        }

        // POST: /ApplicationUser/MultiViewIndex/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult MultiViewIndex(ApplicationUser ObjApplicationUser)
        {
            if (ModelState.IsValid)
            {


                db.Entry(ObjApplicationUser).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("MultiViewIndex", "ApplicationUser", new { id = ObjApplicationUser.Id });
            }
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Title", ObjApplicationUser.DepartmentId);

            return View(ObjApplicationUser);
        }
        private SIContext db = new SIContext();

        private ActionResult ModelBind()
        {
            var ApplicationUsers = db.ApplicationUsers;
            return View(ApplicationUsers.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

     

    }

    class execllist
    {
        public string ID {get;set;}
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DepartmentID { get; set; }
        public string title { get; set; }
        public string active { get; set; }
        public string super { get; set; }

            }

    public static class ListExtensions
    {
        public static DataTable ToDataTable<T>(this List<T> iList)
        {
            DataTable dataTable = new DataTable();
            PropertyDescriptorCollection propertyDescriptorCollection =
                TypeDescriptor.GetProperties(typeof(T));
            for (int i = 0; i < propertyDescriptorCollection.Count; i++)
            {
                PropertyDescriptor propertyDescriptor = propertyDescriptorCollection[i];
                Type type = propertyDescriptor.PropertyType;

                if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    type = Nullable.GetUnderlyingType(type);


                dataTable.Columns.Add(propertyDescriptor.Name, type);
            }
            object[] values = new object[propertyDescriptorCollection.Count];
            foreach (T iListItem in iList)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = propertyDescriptorCollection[i].GetValue(iListItem);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }
    }
}

