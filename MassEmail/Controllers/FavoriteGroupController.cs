﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using MassEmail.Models;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using Microsoft.AspNet.SignalR;
using MassEmail.ChatClass;
using MassEmail.Helper;
using MassEmail.Controllers.Filters;
using MassEmail.Models.Comparer;

namespace MassEmail.Controllers
{
    [AuthorizationAndRoleFilter(accessRole=AccessRole.FavoriteGroup)]
    public class FavoriteGroupController : Controller
    {
        private SIContext db;

        public FavoriteGroupController()
        {
            this.db = new SIContext();
        }
        // GET: FavoriteGroup
        public ActionResult Index()
        {
            var FavoriteGroups = db.FavoriteGroups.ToList();
            return View(FavoriteGroups);
        }
        public ActionResult Create()
        {
            var users = new SelectList(db.ApplicationUsers.ToList(), "Id", "CompleteName");
            ViewBag.UserId = users;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(FavoriteGroup objFavoriteGroup)
        {
            if (ModelState.IsValid)
            {
                var users = Request.Form["UserId"].Split(',');
                foreach (var user in users)
                {
                    objFavoriteGroup.Users.Add(db.ApplicationUsers.Find(Convert.ToInt32(user)));
                }
                db.FavoriteGroups.Add(objFavoriteGroup);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DepartmentId = new SelectList(db.ApplicationUsers.ToList(), "Id", "CompleteName");

            return View(objFavoriteGroup);
        }
        public ActionResult MyGroups()
        {
            return View();
        }
        public ActionResult GetMyGroups()
        {
            ApplicationUser user = db.ApplicationUsers.Find(Convert.ToInt32(Env.GetUserInfo("userid")));
            var groups = db.FavoriteGroups.OrderBy(x => x.Name).ToArray().Where(f => f.Users.Contains(user, new UserComparer())).ToList();
            var result = from c in groups select new string[] { c.Id.ToString(), Convert.ToString(c.Name), prepareUsersName(c.Users) };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetGrid()
        {
            var groups = db.FavoriteGroups.OrderBy(x => x.Name).ToArray();
            var result = from c in groups select new string[] { c.Id.ToString(), Convert.ToString(c.Name), prepareUsersName(c.Users) };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        private string prepareUsersName(ICollection<ApplicationUser> users)
        {
            string usersName = "";
            foreach (var user in users)
            {
                usersName += user.CompleteName + ", ";
            }
            return usersName.Remove(usersName.Length - 2);
        }
    }
}