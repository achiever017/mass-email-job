using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using MassEmail.Models;

namespace MassEmail.Controllers
{
    public class BlockListController : Controller
    { 
        // GET: /BlockList/
        public ActionResult Index()
        {
            return ModelBind();
        }
        
        // GET BlockList/GetGrid
        public ActionResult GetGrid()
        {
            var tak = db.BlockLists.ToArray();
             
            var result = from c in tak select new string[] { c.Id.ToString(), Convert.ToString(c.Id),Convert.ToString(c.ApplicationUser_UserId.Username),Convert.ToString(c.ApplicationUser_BlockedUserId.Username), };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }
        // GET: /BlockList/ModelBindIndex
        public ActionResult ModelBindIndex()
        {
            return ModelBind();
        }
        // GET: /BlockList/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BlockList ObjBlockList = db.BlockLists.Find(id);
            if (ObjBlockList == null)
            {
                return HttpNotFound();
            }
            return View(ObjBlockList);
        }
        // GET: /BlockList/Create
        public ActionResult Create()
        {
             ViewBag.UserId = new SelectList(db.ApplicationUsers, "Id", "Username");
ViewBag.BlockedUserId = new SelectList(db.ApplicationUsers, "Id", "Username");

             return View();
        }

        // POST: /BlockList/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(BlockList ObjBlockList )
        {
            if (ModelState.IsValid)
            {


                db.BlockLists.Add(ObjBlockList);
                db.SaveChanges();
                return Redirect(TempData["ThisUrl"].ToString());
            }
             ViewBag.UserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjBlockList.UserId);
ViewBag.BlockedUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjBlockList.BlockedUserId);

             return View(ObjBlockList);
        }
        // GET: /BlockList/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BlockList ObjBlockList = db.BlockLists.Find(id);
            if (ObjBlockList == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjBlockList.UserId);
ViewBag.BlockedUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjBlockList.BlockedUserId);

            return View(ObjBlockList);
        }

        // POST: /BlockList/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(BlockList ObjBlockList )
        {
            if (ModelState.IsValid)
            {
                

                db.Entry(ObjBlockList).State = EntityState.Modified;
                db.SaveChanges();
                return Redirect(TempData["ThisUrl"].ToString());
            }
            ViewBag.UserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjBlockList.UserId);
ViewBag.BlockedUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjBlockList.BlockedUserId);

            return View(ObjBlockList);
        }
        // GET: /BlockList/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BlockList ObjBlockList = db.BlockLists.Find(id);
            if (ObjBlockList == null)
            {
                return HttpNotFound();
            }
            return View(ObjBlockList);
        }

        // POST: /BlockList/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BlockList ObjBlockList = db.BlockLists.Find(id);
            db.BlockLists.Remove(ObjBlockList);
            db.SaveChanges();
            return Redirect(TempData["ThisUrl"].ToString());
        }
        // GET: /BlockList/MultiViewIndex/5
        public ActionResult MultiViewIndex(int? id)
        { 
            BlockList ObjBlockList = db.BlockLists.Find(id);
            ViewBag.IsWorking = 0;
            if (id > 0)
            {
                ViewBag.IsWorking = 1;
                ViewBag.UserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjBlockList.UserId);
ViewBag.BlockedUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjBlockList.BlockedUserId);

            }
            
            return View(ObjBlockList);
        }

        // POST: /BlockList/MultiViewIndex/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult MultiViewIndex(BlockList ObjBlockList )
        {
            if (ModelState.IsValid)
            {
                

                db.Entry(ObjBlockList).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("MultiViewIndex", "BlockList", new { id = ObjBlockList.Id });
            }
            ViewBag.UserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjBlockList.UserId);
ViewBag.BlockedUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjBlockList.BlockedUserId);

            return View(ObjBlockList);
        }
        private SIContext db = new SIContext();
		
		private ActionResult ModelBind()
        {
            var BlockLists = db.BlockLists;
            return View(BlockLists.ToList());
        }
		
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        
		 
    }
}

