﻿using MassEmail.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity;

using System.Threading;


namespace MassEmail.Controllers
{
    public class AccountController : Controller
    {
        //
        SIContext db = new SIContext();
        // GET: /Account/
        public ActionResult login()
        {
            if (Session["user"] != null)
            {
                if (Session["user"].ToString() != "false")
                {
                    string name = Session["user"].ToString();
                    var login = db.ApplicationUsers.FirstOrDefault(x => x.Username == name);
                    var claims = new List<Claim>();
                    claims.Add(new Claim(ClaimTypes.Name, login.Username.ToString())); // store username of user
                    claims.Add(new Claim(ClaimTypes.Role, login.RoleUser_UserIds.FirstOrDefault().RoleId.ToString()));
                    claims.Add(new Claim(ClaimTypes.Sid, login.Id.ToString())); // store id of user
                    claims.Add(new Claim(ClaimTypes.Surname, login.FirstName.ToString()+" "+login.LastName));

                    var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);
                    var authenticationManager = Request.GetOwinContext().Authentication;
                    authenticationManager.SignIn(identity);

                    var claimsPrincipal = new ClaimsPrincipal(identity);
                    Thread.CurrentPrincipal = claimsPrincipal;

                    return Redirect("~/Home/Index");
                }
            }
            return View();
        }

        [HttpPost]
        public ActionResult login(System.Web.Mvc.FormCollection frmCollection)
        {
            string email = frmCollection["email"].ToString();
            string password = frmCollection["password"].ToString();
            Session["email"] = email.ToString();
            Session["password"] = password.ToString();

            ViewBag.IframeSrc = "http://aykome.aydin.bel.tr:82/Account/login?email=" + email + "&pass=" + password;
            var login = db.ApplicationUsers.FirstOrDefault(i => i.Email == email && i.Password == password);
            if (login != null)
            {
                var claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.Name, login.Username.ToString())); // store username of user
                claims.Add(new Claim(ClaimTypes.Role, login.RoleUser_UserIds.FirstOrDefault().RoleId.ToString()));
                claims.Add(new Claim(ClaimTypes.Sid, login.Id.ToString())); // store id of user
                claims.Add(new Claim(ClaimTypes.Surname, login.FirstName.ToString() + " " + login.LastName));
                var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);
                var authenticationManager = Request.GetOwinContext().Authentication;
                authenticationManager.SignIn(identity);

                var claimsPrincipal = new ClaimsPrincipal(identity);
                Thread.CurrentPrincipal = claimsPrincipal;
                Session["user"] = email;
                ViewBag.username = email;
                ViewBag.password = password;
                return Redirect("~/Home/Index");
            }
            else
            {
                ViewBag.Msg = "Geçersiz kullanıcı adı veya şifre!";
            }
            return View();
        }

        public ActionResult register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult register(System.Web.Mvc.FormCollection frmCollection)
        {
            ApplicationUser user = new ApplicationUser();
            user.Username = frmCollection["name"];
            user.Password = frmCollection["password2"];
            db.ApplicationUsers.Add(user);

            //REGISTER USER ROLE
            RoleUser roleuser = new RoleUser();
            roleuser.RoleId = 1;
            roleuser.UserId = user.Id;
            db.RoleUsers.Add(roleuser);
            //db.SaveChanges();

            ViewBag.Msg = "Kayýt Tamamlandý.";
            return View();
        }

        public ActionResult signout()
        {


            Session["user"] = "false";
            AuthenticationManager.SignOut();
            HttpCookie c = new HttpCookie(".AspNet.ApplicationCookie");
            c.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(c);

            HttpCookie d = new HttpCookie("__RequestVerificationToken");
            d.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(d);

            return RedirectToAction("login", "Account");
        }


        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }


        public ActionResult LockScreen()
        {
            Session.Remove("password");
          
            return RedirectToAction("lockuser", "Account");
        }


        public ActionResult lockuser()
        {
            LockModel model = new LockModel();
            if (Session["email"]!=null)
            { model.email = Session["email"].ToString(); }
         
            return View(model);
          
        }

        [HttpPost]
        public ActionResult lockuser(LockModel model)
        {

            Session["email"] =model.email;
            Session["password"] = model.password;

            ViewBag.IframeSrc = "http://aykome.aydin.bel.tr:82/Account/login?email=" + model.email + "&pass=" + model.password; 
            var login = db.ApplicationUsers.FirstOrDefault(i => i.Email == model.email && i.Password == model.password);
            if (login != null)
            {
                var claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.Name, login.Username.ToString())); // store username of user
                claims.Add(new Claim(ClaimTypes.Role, login.RoleUser_UserIds.FirstOrDefault().RoleId.ToString()));
                claims.Add(new Claim(ClaimTypes.Sid, login.Id.ToString())); // store id of user
                claims.Add(new Claim(ClaimTypes.Surname, login.FirstName.ToString() + " " + login.LastName));
                var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);
                var authenticationManager = Request.GetOwinContext().Authentication;
                authenticationManager.SignIn(identity);

                var claimsPrincipal = new ClaimsPrincipal(identity);
                Thread.CurrentPrincipal = claimsPrincipal;
                Session["user"] = model.email;
                ViewBag.username = model.email;
                ViewBag.password = model.password;;
                return Redirect("~/Home/Index");
            }
            else
            {
                ViewBag.Msg = "Geçersiz kullanıcı adı veya şifre!";
            }
            return View();

        }

    }
}

