using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using MassEmail.Models;

namespace MassEmail.Controllers
{
    public class EmailMessageController : Controller
    { 
        // GET: /EmailMessage/
        public ActionResult Index()
        {
            return ModelBind();
        }
        
        // GET EmailMessage/GetGrid
        public ActionResult GetGrid()
        {
            var tak = db.EmailMessages.ToArray();
             
            var result = from c in tak select new string[] { c.Id.ToString(), Convert.ToString(c.Id),Convert.ToString(c.FromUserId),Convert.ToString(c.Subject),Convert.ToString(c.MailText),Convert.ToString(c.DateSend),Convert.ToString(c.IsDelete), };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }
        // GET: /EmailMessage/ModelBindIndex
        public ActionResult ModelBindIndex()
        {
            return ModelBind();
        }
        // GET: /EmailMessage/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailMessage ObjEmailMessage = db.EmailMessages.Find(id);
            if (ObjEmailMessage == null)
            {
                return HttpNotFound();
            }
            return View(ObjEmailMessage);
        }
        // GET: /EmailMessage/Create
        public ActionResult Create()
        {
             ViewBag.FromUserId = new SelectList(db.ApplicationUsers, "Id", "Username");

             return View();
        }

        // POST: /EmailMessage/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(EmailMessage ObjEmailMessage )
        {
            if (ModelState.IsValid)
            {


                db.EmailMessages.Add(ObjEmailMessage);
                db.SaveChanges();
                return Redirect(TempData["ThisUrl"].ToString());
            }
             ViewBag.FromUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjEmailMessage.FromUserId);

             return View(ObjEmailMessage);
        }
        // GET: /EmailMessage/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailMessage ObjEmailMessage = db.EmailMessages.Find(id);
            if (ObjEmailMessage == null)
            {
                return HttpNotFound();
            }
            ViewBag.FromUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjEmailMessage.FromUserId);

            return View(ObjEmailMessage);
        }

        // POST: /EmailMessage/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(EmailMessage ObjEmailMessage )
        {
            if (ModelState.IsValid)
            {
                

                db.Entry(ObjEmailMessage).State = EntityState.Modified;
                db.SaveChanges();
                return Redirect(TempData["ThisUrl"].ToString());
            }
            ViewBag.FromUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjEmailMessage.FromUserId);

            return View(ObjEmailMessage);
        }
        // GET: /EmailMessage/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailMessage ObjEmailMessage = db.EmailMessages.Find(id);
            if (ObjEmailMessage == null)
            {
                return HttpNotFound();
            }
            return View(ObjEmailMessage);
        }

        // POST: /EmailMessage/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EmailMessage ObjEmailMessage = db.EmailMessages.Find(id);
            db.EmailMessages.Remove(ObjEmailMessage);
            db.SaveChanges();
            return Redirect(TempData["ThisUrl"].ToString());
        }
        // GET: /EmailMessage/MultiViewIndex/5
        public ActionResult MultiViewIndex(int? id)
        { 
            EmailMessage ObjEmailMessage = db.EmailMessages.Find(id);
            ViewBag.IsWorking = 0;
            if (id > 0)
            {
                ViewBag.IsWorking = 1;
                ViewBag.FromUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjEmailMessage.FromUserId);

            }
            
            return View(ObjEmailMessage);
        }

        // POST: /EmailMessage/MultiViewIndex/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult MultiViewIndex(EmailMessage ObjEmailMessage )
        {
            if (ModelState.IsValid)
            {
                

                db.Entry(ObjEmailMessage).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("MultiViewIndex", "EmailMessage", new { id = ObjEmailMessage.Id });
            }
            ViewBag.FromUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjEmailMessage.FromUserId);

            return View(ObjEmailMessage);
        }
        private SIContext db = new SIContext();
		
		private ActionResult ModelBind()
        {
            var EmailMessages = db.EmailMessages;
            return View(EmailMessages.ToList());
        }
		
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        
		 
    }
}

