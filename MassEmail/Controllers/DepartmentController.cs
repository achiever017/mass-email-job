using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using MassEmail.Models;

namespace MassEmail.Controllers
{
    public class DepartmentController : Controller
    {
        // GET: /Department/
        public ActionResult Index()
        {
            return ModelBind();
        }

        // GET Department/GetGrid
        public ActionResult GetGrid()
        {
            var tak = db.Departments.ToArray();

            var result = from c in tak select new string[] { c.Id.ToString(), Convert.ToString(c.Title), Convert.ToString(c.IsActive), Convert.ToString(c.PhotoPath), Convert.ToString(c.Color), };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }
        // GET: /Department/ModelBindIndex
        public ActionResult ModelBindIndex()
        {
            return ModelBind();
        }
        // GET: /Department/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Department ObjDepartment = db.Departments.Find(id);
            if (ObjDepartment == null)
            {
                return HttpNotFound();
            }
            return View(ObjDepartment);
        }
        // GET: /Department/Create
        public ActionResult Create()
        {

            return View();
        }

        // POST: /Department/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(Department ObjDepartment)
        {
            if (ModelState.IsValid)
            {
                HttpPostedFileBase file = Request.Files["DocPath"];

                if(file.ContentLength == 0)
                {
                    ViewBag.Msg = "Logo Y�kleyin";
                    return View();
                    //
                }

                if (ObjDepartment.Color==String.Empty)
                {
                    ViewBag.Msg = "Renk Y�kleyin";
                    return View();
                }

                var image = System.Drawing.Image.FromStream(file.InputStream);

                var Width = (int)(image.Width);
                var Height = (int)(image.Height);

                if(Width >20 || Height >20) // 
                {
                    ViewBag.Msg = "20x20 Maksimum ��z�n�rl�k";//
                    return View();
                }

                var GetExt = Path.GetExtension(file.FileName);
                string filenameOutExt = Path.GetFileNameWithoutExtension(file.FileName);
                var fileName = Path.GetFileNameWithoutExtension(file.FileName) + GetExt;
                file.SaveAs(Path.Combine(Server.MapPath("~/Logo"), fileName));
                ModelState.Clear();
                ObjDepartment.PhotoPath = fileName; 

               // if (file.ContentLength != 0)
               // {
                 //   ObjDepartment.PhotoPath = Path.GetExtension(file.FileName);
                   // string renamedFilename = ObjDepartment.Title.ToString() + Path.GetExtension(file.FileName);
                    //string filepath = Path.Combine(CaseFolder, renamedFilename);
                   // file.SaveAs(file.FileName);
                //}

                db.Departments.Add(ObjDepartment);
                db.SaveChanges();
                ViewBag.Success = "Kurum Ba�ar�yla Olu�turuldu";
                return View("Create");
            }

            return View(ObjDepartment); 
        }
        // GET: /Department/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Department ObjDepartment = db.Departments.Find(id);
            if (ObjDepartment == null)
            {
                return HttpNotFound();
            }

            return View(ObjDepartment);
        }

        //
        // 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(Department ObjDepartment)
        {
            if (ModelState.IsValid)
            {
                HttpPostedFileBase file = Request.Files["DocPath"];
                if (file.ContentLength == 0)
                {
                    ViewBag.Msg = "Logo Y�kleyin";
                    return View(ObjDepartment);
                    //
                }
                var image = System.Drawing.Image.FromStream(file.InputStream);

                var Width = (int)(image.Width);
                var Height = (int)(image.Height);

                if (Width > 20 || Height > 20) // 
                {
                    ViewBag.Msg = "20x20 Maksimum ��z�n�rl�k";//
                    return View(ObjDepartment);
                }

                var GetExt = Path.GetExtension(file.FileName);
                string filenameOutExt = Path.GetFileNameWithoutExtension(file.FileName);
                var fileName = Path.GetFileNameWithoutExtension(file.FileName) + GetExt;
                file.SaveAs(Path.Combine(Server.MapPath("~/Logo"), fileName));
                ModelState.Clear();
                ObjDepartment.PhotoPath = fileName; 

                db.Entry(ObjDepartment).State = EntityState.Modified;
                db.SaveChanges();
                return Redirect(TempData["ThisUrl"].ToString());
            }

            return View(ObjDepartment);
        }
        // GET: /Department/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Department ObjDepartment = db.Departments.Find(id);
            if (ObjDepartment == null)
            {
                return HttpNotFound();
            }
            return View(ObjDepartment);
        }

        // POST: /Department/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Department ObjDepartment = db.Departments.Find(id);
            BackupDepartment bd = new BackupDepartment()
            {
                DeletedBy = Env.GetUserInfo("name"),
                DeleteOn = DateTime.Now,
                IsActive = ObjDepartment.IsActive,
                Title = ObjDepartment.Title
            };
            db.BackupDepartments.Add(bd);
            db.Departments.Remove(ObjDepartment);
            db.SaveChanges();
            TempData["Message"] = "Ba�ar�yla Silindi";
            return RedirectToAction("Index");
        }
        // GET: /Department/MultiViewIndex/5
        public ActionResult MultiViewIndex(int? id,string s)
        {
            Department ObjDepartment = db.Departments.Find(id);
            ViewBag.photo = ObjDepartment.PhotoPath;
            ViewBag.IsWorking = 0;
            ViewBag.msg = s;
            if (id > 0)
            {
                ViewBag.IsWorking = 1;

            }

            return View(ObjDepartment);
        }

        // 
        // 
        // http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult MultiViewIndex(Department ObjDepartment,string photo)
        {
            if (ModelState.IsValid)
            {

                HttpPostedFileBase file = Request.Files["DocPath"];
                if (file.ContentLength == 0)
                {

                    ObjDepartment.PhotoPath = photo;
                    db.Entry(ObjDepartment).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("MultiViewIndex", "Department", new { id = ObjDepartment.Id });
                    //
                }
                else
                {
                    var image = System.Drawing.Image.FromStream(file.InputStream);

                    var Width = (int)(image.Width);
                    var Height = (int)(image.Height);

                    if (Width > 20 || Height > 20) // 
                    {
                        ViewBag.Msg = "20x20 Maksimum ��z�n�rl�k";//
                        return RedirectToAction("MultiViewIndex", new { id = ObjDepartment.Id, s = "20x20 Maksimum ��z�n�rl�k" });
                    }

                    var GetExt = Path.GetExtension(file.FileName);
                    string filenameOutExt = Path.GetFileNameWithoutExtension(file.FileName);
                    var fileName = Path.GetFileNameWithoutExtension(file.FileName) + GetExt;
                    file.SaveAs(Path.Combine(Server.MapPath("~/Logo"), fileName));
                    ModelState.Clear();
                    ObjDepartment.PhotoPath = fileName;
                    db.Entry(ObjDepartment).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return RedirectToAction("MultiViewIndex", "Department", new { id = ObjDepartment.Id });
            }

            return View(ObjDepartment);
        }
        private SIContext db = new SIContext();

        private ActionResult ModelBind()
        {
            var Departments = db.Departments;
            return View(Departments.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }




        public string FileName { get; set; }

        public string filename { get; set; }
    }
}

