﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using MassEmail.Models;
using AjaxClassJson;
namespace MassEmail.Controllers
{
    public class SiMenuBarController : Controller
    {
        //
        // GET: /SiMenuBar/ 
        public ActionResult Index()
        {
            if (AjaxLoad.per())
            { ViewBag.bar = GetMenuBarPage(null); }
            return PartialView();
        }


        public MvcHtmlString GetMenuBarPage(Nullable<int> ParentId)
        {
            StringBuilder sb = new StringBuilder();
            SIContext db = new SIContext();
            var userId = Convert.ToInt32(Env.GetUserInfo("userid"));
            var RoleId = Convert.ToInt32(Env.GetUserInfo("roleid"));

            var q = db.MenuPermissions.Include("Menu_MenuId").Where(i => i.RoleId == RoleId || i.UserId == userId).ToArray();

            sb.Append("<ul class=\"nav nav-sidebar\">");
            sb.Append("<li><a href =\"" + MicrosoftHelper.MSHelper.GetSiteRoot() + "/Home\" ><i class=\"icon-home\"></i><span>Anasayfa</span></a></li>");
            sb.Append(GetMenuBar(ParentId, q));
            sb.Append("</ul>");
            return MvcHtmlString.Create(sb.ToString());
        }




        public MvcHtmlString GetMenuBar(Nullable<int> ParentId, MenuPermission[] q)
        {
            MenuPermission ordered_item = null;
            IList<MenuPermission> list = null;

            StringBuilder sb = new StringBuilder();
            if (q != null)
            {
                var t = q.Where(i => i.Menu_MenuId.ParentId == ParentId).ToArray();

                foreach (var item in t)
                {
                if (item.Menu_MenuId.MenuURL == "FavoriteGroup/")
                {
                    ordered_item = item;
                        list = t.Where(x => x.Id != item.Id).ToList();
                }
            }
                if (ordered_item != null)
            {
                    list.Insert(1, ordered_item);
                    t = list.ToArray();
                }

                foreach (var item in t)
                {
                    if (item.Menu_MenuId.MenuText != "Ayarlar")
                    {
                        var js = q;

                        if (js.Count(j => j.Menu_MenuId.ParentId == item.Menu_MenuId.Id) > 0)
                        {
                            if (item.Menu_MenuId.MenuText == "Kullanıcı")
                            {
                                if (item.Menu_MenuId.ParentId == null)
                                {
                                    sb.Append("<li class=\"nav-parent\"> <a href=\"#\"> <i class=\"fa fa-user\"></i> <span>" + item.Menu_MenuId.MenuText + "</span><span class=\"fa arrow\"></span></a><ul class=\"children collapse\">");
                                }
                                else
                                {
                                    sb.Append("<li class=\"nav-parent\"> <a href=\"#\"> <i class=\"fa fa-user\"></i> <span>" + item.Menu_MenuId.MenuText + "</span><span class=\"fa arrow\"></span></a><ul class=\"children collapse\">");
                                }
                            }

                            if (item.Menu_MenuId.MenuText == "Kurum")
                            {
                                if (item.Menu_MenuId.ParentId == null)
                                {
                                    sb.Append("<li class=\"nav-parent\"> <a href=\"#\"> <i class=\"fa fa-university\"></i> <span>" + item.Menu_MenuId.MenuText + "</span><span class=\"fa arrow\"></span></a><ul class=\"children collapse\">");
                                }
                                else
                                {
                                    sb.Append("<li class=\"nav-parent\"> <a href=\"#\"> <i class=\"fa fa-university\"></i> <span>" + item.Menu_MenuId.MenuText + "</span><span class=\"fa arrow\"></span></a><ul class=\"children collapse\">");
                                }
                            }

                            if (item.Menu_MenuId.MenuText == "Bildirim")
                            {
                                if (item.Menu_MenuId.ParentId == null)
                                {
                                    sb.Append("<li class=\"nav-parent\"> <a href=\"#\"> <i class=\"fa fa-bell\"></i> <span>" + item.Menu_MenuId.MenuText + "</span><span class=\"fa arrow\"></span></a><ul class=\"children collapse\">");
                                }
                                else
                                {
                                    sb.Append("<li class=\"nav-parent\"> <a href=\"#\"> <i class=\"fa fa-bell\"></i> <span>" + item.Menu_MenuId.MenuText + "</span><span class=\"fa arrow\"></span></a><ul class=\"children collapse\">");
                                }
                            }

                            if (item.Menu_MenuId.MenuText == "Etkinlik")
                            {
                                if (item.Menu_MenuId.ParentId == null)
                                {
                                    sb.Append("<li class=\"nav-parent\"> <a href=\"#\"> <i class=\"fa fa-calendar-o\"></i> <span>" + item.Menu_MenuId.MenuText + "</span><span class=\"fa arrow\"></span></a><ul class=\"children collapse\">");
                                }
                                else
                                {
                                    sb.Append("<li class=\"nav-parent\"> <a href=\"#\"> <i class=\"fa fa-calendar-o\"></i> <span>" + item.Menu_MenuId.MenuText + "</span><span class=\"fa arrow\"></span></a><ul class=\"children collapse\">");
                                }
                            }

                            if (item.Menu_MenuId.MenuText == "Mesaj")
                            {
                                if (item.Menu_MenuId.ParentId == null)
                                {
                                    sb.Append("<li class=\"nav-parent\"> <a href=\"#\"> <i class=\"fa fa-comments\"></i> <span>" + item.Menu_MenuId.MenuText + "</span><span class=\"fa arrow\"></span></a><ul class=\"children collapse\">");
                                }
                                else
                                {
                                    sb.Append("<li class=\"nav-parent\"> <a href=\"#\"> <i class=\"fa fa-comments\"></i> <span>" + item.Menu_MenuId.MenuText + "</span><span class=\"fa arrow\"></span></a><ul class=\"children collapse\">");
                                }
                            }

                            if (item.Menu_MenuId.MenuText == "E-posta")
                            {
                                if (item.Menu_MenuId.ParentId == null)
                                {
                                    sb.Append("<li class=\"nav-parent\"> <a href=\"#\"> <i class=\"fa fa-envelope\"></i> <span>" + item.Menu_MenuId.MenuText + "</span><span class=\"fa arrow\"></span></a><ul class=\"children collapse\">");
                                }
                                else
                                {
                                    sb.Append("<li class=\"nav-parent\"> <a href=\"#\"> <i class=\"fa fa-envelope\"></i> <span>" + item.Menu_MenuId.MenuText + "</span><span class=\"fa arrow\"></span></a><ul class=\"children collapse\">");
                                }
                            }
                            if (item.Menu_MenuId.MenuText == "Favori Grup")
                            {
                                if (item.Menu_MenuId.ParentId == null)
                                {
                                    sb.Append("<li class=\"nav-parent\"> <a href=\"#\"> <i class=\"fa fa-star\"></i> <span>" + item.Menu_MenuId.MenuText + "</span><span class=\"fa arrow\"></span></a><ul class=\"children collapse\">");
                                }
                                else
                                {
                                    sb.Append("<li class=\"nav-parent\"> <a href=\"#\"> <i class=\"fa fa-star\"></i> <span>" + item.Menu_MenuId.MenuText + "</span><span class=\"fa arrow\"></span></a><ul class=\"children collapse\">");
                                }
                            }

                            if (item.Menu_MenuId.MenuText == "Raporlama")
                            {
                                if (item.Menu_MenuId.ParentId == null)
                                {
                                    sb.Append("<li class=\"nav-parent\"> <a href=\"#\"> <i class=\"fa fa-bars\"></i> <span>" + item.Menu_MenuId.MenuText + "</span><span class=\"fa arrow\"></span></a><ul class=\"children collapse\">");
                                }
                                else
                                {
                                    sb.Append("<li class=\"nav-parent\"> <a href=\"#\"> <i class=\"fa fa-bars\"></i> <span>" + item.Menu_MenuId.MenuText + "</span><span class=\"fa arrow\"></span></a><ul class=\"children collapse\">");
                                }
                            }



                            sb.Append(GetMenuBar(item.Menu_MenuId.Id, q));
                        }
                        else
                        {
                            if (Convert.ToInt32(Env.GetUserInfo("roleid")) == 2)
                            {
                                var userId = Convert.ToInt32(Env.GetUserInfo("userid"));
                                var user = new SIContext().ApplicationUsers.FirstOrDefault(i => i.Id == userId);

                                if (!((user.EventCreat != true && (item.Menu_MenuId.MenuURL == "Event/Create"
                                    || item.Menu_MenuId.MenuURL == "Event/Index"))
                                    || (user.AnnouncementCreat != true && (item.Menu_MenuId.MenuURL == "Announcement/Create"
                                    || item.Menu_MenuId.MenuURL == "Announcement/Index"))
                                    || ((user.AnnouncementCreat != true || user.EventCreat != true) && 
                                    (item.Menu_MenuId.MenuURL == "FavoriteGroup/Index" 
                                    || item.Menu_MenuId.MenuURL == "FavoriteGroup/MyGroup" 
                                    || item.Menu_MenuId.MenuURL == "FavoriteGroup/Create"))))
                                {
                                    sb.Append("<li class=\"\"><a href =\"" + MicrosoftHelper.MSHelper.GetSiteRoot() + "/" + item.Menu_MenuId.MenuURL + "\"><span>" + item.Menu_MenuId.MenuText + "</span></a></li>");
                                }

                            }
                            else if (item.Menu_MenuId.ParentId == null)
                            {
                                sb.Append("<li class=\"\"><a href =\"" + MicrosoftHelper.MSHelper.GetSiteRoot() + "/" + item.Menu_MenuId.MenuURL + "\"><span>" + item.Menu_MenuId.MenuText + "</span></a></li>");
                            }
                            else
                            {
                                sb.Append("<li class=\"\"><a href =\"" + MicrosoftHelper.MSHelper.GetSiteRoot() + "/" + item.Menu_MenuId.MenuURL + "\"><span>" + item.Menu_MenuId.MenuText + "</span></a></li>");
                            }

                        }
                    }
                }
                sb.Append("</ul>");
            }


            return MvcHtmlString.Create(sb.ToString());
        }
    }
}
