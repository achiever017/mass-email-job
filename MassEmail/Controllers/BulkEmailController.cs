﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.IO;
using MassEmail.Models;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using System.Web;
//using System.Web.Mail;
using System.Net.Mail;


namespace MassEmail.Controllers
{
    public class BulkEmailController : Controller
    {
        // GET: /EmailMessage/
        public ActionResult History()
        {
            return ModelBind();
        }

        // GET EmailMessage/GetGrid
        public ActionResult GetGrid()
        {
            var tak = db.EmailMessages.OrderByDescending(x => x.DateSend).ToArray();
            var users = db.ApplicationUsers.ToArray();

            var result = from c in tak select new string[] { Convert.ToString(c.Id), Convert.ToString(users.FirstOrDefault(i => i.Id == c.FromUserId) != null ? users.FirstOrDefault(i => i.Id == c.FromUserId).FirstName + ' ' + users.FirstOrDefault(i => i.Id == c.FromUserId).LastName : ""), Convert.ToString(c.Id), Convert.ToString(c.Subject), Convert.ToString(c.DateSend) };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetGridEmailMembers(int id)
        {
            var tak = db.EmailMembers.Where(i => i.EmailMessageId == id).ToArray();
            var result = from c in tak select new string[] { Convert.ToString(c.Id), Convert.ToString(c.EmailMessage_EmailMessageId.Subject), Convert.ToString(c.ApplicationUser_ToUserId.FirstName + ' ' + c.ApplicationUser_ToUserId.LastName) };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetGridAttachment(int id)
        {
            var tak = db.EmailAttachments.Where(i => i.EmailMessageId == id).ToArray();

            var result = from c in tak select new string[] { Convert.ToString(c.Id), Convert.ToString(c.EmailMessage_EmailMessageId.Subject), Convert.ToString(c.FileUrl) };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        // GET: /EmailMessage/ModelBindIndex
        public ActionResult ModelBindIndex()
        {
            return ModelBind();
        }
        // GET: /EmailMessage/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailMessage ObjEmailMessage = db.EmailMessages.Find(id);
            if (ObjEmailMessage == null)
            {
                return HttpNotFound();
            }
            return View(ObjEmailMessage);
        }
        // GET: /EmailMessage/Create
        public ActionResult Create()
        {

            LoadBinding();

            return View();
        }

        private void LoadBinding()
        {
            var UsersList = db.ApplicationUsers.OrderBy(x => x.FirstName);
            int cUserId = Convert.ToInt32(Env.GetUserInfo("userid"));

            StringBuilder sbUser = new StringBuilder();
            sbUser.Append("[");
            //['IT,', 'Sales,', 'Technical,']
            foreach (var item in UsersList.Where(i => i.Id != cUserId).ToArray())
            {
                sbUser.Append("{\"id\":\"" + item.Email + "\", \"name\":\"" + item.FirstName + " " + item.LastName + "\"},");
            }
            sbUser.Append("]");
            ViewBag.UserList = sbUser.ToString();




            StringBuilder sbDepartment = new StringBuilder();
            sbDepartment.Append("[");
            //['IT,', 'Sales,', 'Technical,']
            foreach (var item in db.Departments.OrderBy(x => x.Title).ToArray())
            {
                sbDepartment.Append("{\"id\":\"" + item.Title + "\", \"name\":\"" + item.Title + "\"},");
            }
            sbDepartment.Append("]");
            ViewBag.Departments = sbDepartment.ToString();

            StringBuilder sbFavoriteGroups = new StringBuilder();
            sbFavoriteGroups.Append("[");

            foreach (var item in db.FavoriteGroups.OrderBy(x => x.Name).ToArray())
            {
                sbFavoriteGroups.Append("{\"id\":\"" + item.Id + "\", \"name\":\"" + item.Name + "(" + prepareUsersName(item.Users) + ")" + "\"},");
            }
            sbFavoriteGroups.Append("]");
            ViewBag.FavoriteGroups = sbFavoriteGroups.ToString();

            ViewBag.FromUserId = new SelectList(UsersList, "Id", "Username", cUserId);
        }

        private string prepareUsersName(ICollection<ApplicationUser> users)
        {
            string usersName = "";
            foreach (var user in users)
            {
                usersName += user.CompleteName + ", ";
            }
            return usersName.Remove(usersName.Length - 2);
        }

        // POST: /EmailMessage/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(EmailMessage ObjEmailMessage, string toEmails,string toFavoriteEmails, string toDepEmails, HttpPostedFileBase[] filesAttach)
        {
            LoadBinding();
            if (ModelState.IsValid)
            {
                var gs = db.GeneralSettings.ToArray();
                if ((toEmails.Length > 0 || toDepEmails.Length > 0 || toFavoriteEmails.Length > 0))
                {
                    bool isEmailAdded = false;
                    if (toEmails.Length > 0)
                    {
                        db.EmailMessages.Add(ObjEmailMessage);
                        db.SaveChanges();
                        isEmailAdded = true;
                        string filename = "";

                        if (filesAttach.Count() >= 1)
                        {
                            foreach (HttpPostedFileBase file in filesAttach)
                            {
                                if (file != null)
                                {
                                    /*Geting the file name*/
                                    filename = System.IO.Path.GetFileName(file.FileName);
                                    /*Saving the file in server folder*/
                                    file.SaveAs(Server.MapPath("~/Uploads/" + filename));

                                    /*HERE WILL BE YOUR CODE TO SAVE THE FILE DETAIL IN DATA BASE*/
                                    EmailAttachment attach = new EmailAttachment();
                                    attach.FileUrl = filename;
                                    attach.IsDeleted = false;
                                    attach.EmailMessageId = ObjEmailMessage.Id;
                                    db.EmailAttachments.Add(attach);
                                    db.SaveChanges();
                                }
                            }

                        }

                        var AllUsers = db.ApplicationUsers.ToArray();

                        string[] emails = toEmails.Split(',');
                        foreach (var item in emails)
                        {
                            if (item != "")
                            {
                                EmailMember member = new EmailMember();
                                member.EmailMessageId = ObjEmailMessage.Id;
                                member.ToUserId = AllUsers.FirstOrDefault(i => i.Username == item.Trim()).Id;
                                member.IsRead = false;
                                member.DateUpdated = DateTime.Now;
                                db.EmailMembers.Add(member);

                                SendAsync(Env.GetUserInfo("name"), item, ObjEmailMessage.Subject, ObjEmailMessage.MailText, gs, filesAttach);

                                db.SaveChanges();

                            }
                        }
                    }

                    if (toDepEmails.Length > 0)
                    {
                        if (!isEmailAdded)
                        {
                            db.EmailMessages.Add(ObjEmailMessage);
                            db.SaveChanges();
                        }
                        if (filesAttach.Count() > 1)
                        {
                            foreach (HttpPostedFileBase file in filesAttach)
                            {
                                /*Geting the file name*/
                                string filename = System.IO.Path.GetFileName(file.FileName);
                                /*Saving the file in server folder*/
                                file.SaveAs(Server.MapPath("~/Uploads/" + filename));

                                /*HERE WILL BE YOUR CODE TO SAVE THE FILE DETAIL IN DATA BASE*/
                                EmailAttachment attach = new EmailAttachment();
                                attach.FileUrl = filename;
                                attach.IsDeleted = false;
                                attach.EmailMessageId = ObjEmailMessage.Id;
                                db.EmailAttachments.Add(attach);
                                db.SaveChanges();
                            }
                        }
                        string[] departm = toDepEmails.Split(',');
                        foreach (var item in departm)
                        {
                            if (item != "")
                            {
                                var users = db.ApplicationUsers.Where(i => i.Department_DepartmentId.Title == item.Trim()).ToArray();
                                foreach (var user in users)
                                {
                                    EmailMember member = new EmailMember();
                                    member.EmailMessageId = ObjEmailMessage.Id;
                                    member.ToUserId = user.Id;
                                    member.IsRead = false;
                                    member.DateUpdated = DateTime.Now;
                                    db.EmailMembers.Add(member);

                                    SendAsync(Env.GetUserInfo("name"), user.Username, ObjEmailMessage.Subject, ObjEmailMessage.MailText, gs, filesAttach);

                                    db.SaveChanges();
                                }
                            }
                        }
                    }
                    if (toFavoriteEmails.Length > 0)
                    {
                        if (!isEmailAdded)
                        {
                            db.EmailMessages.Add(ObjEmailMessage);
                            db.SaveChanges();
                        }
                        if (filesAttach.Count() > 1)
                        {
                            foreach (HttpPostedFileBase file in filesAttach)
                            {
                                /*Geting the file name*/
                                string filename = System.IO.Path.GetFileName(file.FileName);
                                /*Saving the file in server folder*/
                                file.SaveAs(Server.MapPath("~/Uploads/" + filename));

                                /*HERE WILL BE YOUR CODE TO SAVE THE FILE DETAIL IN DATA BASE*/
                                EmailAttachment attach = new EmailAttachment();
                                attach.FileUrl = filename;
                                attach.IsDeleted = false;
                                attach.EmailMessageId = ObjEmailMessage.Id;
                                db.EmailAttachments.Add(attach);
                                db.SaveChanges();
                            }
                        }
                        String[] Favorites = toFavoriteEmails.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        List<Int32> FavaritesId = new List<int>();
                        if (Favorites.Length <= 0)
                        {
                            ViewBag.Msg = "Alıcı Seçiniz";
                            return View("Create");
                        }
                        Favorites.ToList().ForEach(x => FavaritesId.Add(Convert.ToInt32(x)));
                        var favgroups = db.FavoriteGroups.Where(x => FavaritesId.Contains(x.Id)).ToArray();
                        var favUsersId = getIdUsersFromFavoriteGroups(favgroups);
                        foreach (var user in favUsersId)
                        {
                            var u = db.ApplicationUsers.Find(user);
                            EmailMember member = new EmailMember();
                            member.EmailMessageId = ObjEmailMessage.Id;
                            member.ToUserId = u.Id;
                            member.IsRead = false;
                            member.DateUpdated = DateTime.Now;
                            db.EmailMembers.Add(member);

                            SendAsync(Env.GetUserInfo("name"), u.Username, ObjEmailMessage.Subject, ObjEmailMessage.MailText, gs, filesAttach);

                            db.SaveChanges();
                                
                        }
                    }
                    else if (toEmails.Length < 0)
                    {
                        ViewBag.Msg = "Kurum Seçiniz";
                        return View("Create");
                    }

                    ViewBag.Success = "Teşekkürler.";
                    return View("Create");
                }
                else
                {
                    ViewBag.Msg = "Kullanıcı veya Kurum Seçiniz";
                    return View("Create");
                }


            }

            return View("Create");
        }


        private int[] getIdUsersFromFavoriteGroups(ICollection<FavoriteGroup> favoritegroups)
        {
            IList<int> usersId = new List<int>();
            foreach (var group in favoritegroups)
            {
                foreach (var user in group.Users)
                    usersId.Add(user.Id);
            }
            return usersId.ToArray();
        }







        public Task SendAsync(string FromUser, string ToUser, string subject, string body, GeneralSetting[] gs, System.Web.HttpPostedFileBase[] filesAttach)
        {
            var result = Task.FromResult(0);
            var toAddress = new System.Net.Mail.MailAddress(ToUser, ToUser);


            try
            {

                System.Net.Mail.MailMessage emessage = new System.Net.Mail.MailMessage();
                if (filesAttach.Count() >= 1)
                {
                    foreach (HttpPostedFileBase f in filesAttach)
                    {
                        if (f == null) { }
                        else
                        {
                            System.Net.Mail.Attachment at = new System.Net.Mail.Attachment(f.InputStream, f.FileName);
                            emessage.Attachments.Add(at);
                        }
                    }
                }

                string Username = gs.FirstOrDefault(i => i.SettingKey == "Email").SettingValue;
                string Password = gs.FirstOrDefault(i => i.SettingKey == "EmailPassword").SettingValue;
                string port = gs.FirstOrDefault(i => i.SettingKey == "EmailPort").SettingValue;
                string SmptServer = gs.FirstOrDefault(i => i.SettingKey == "EmailServer").SettingValue;

                emessage.To.Add(toAddress);
                emessage.Subject = subject;
                emessage.From = new System.Net.Mail.MailAddress(FromUser);
                emessage.Body = body;
                emessage.IsBodyHtml = true;
                System.Net.Mail.SmtpClient sc = new System.Net.Mail.SmtpClient();
                var netCredential = new System.Net.NetworkCredential(Username, Password);
                sc.Host = SmptServer;
                sc.DeliveryMethod = SmtpDeliveryMethod.Network;
                sc.UseDefaultCredentials = false;
                sc.Credentials = netCredential;
                sc.Port = Convert.ToInt32(port);
                sc.Send(emessage);



                result = Task.FromResult(1);
            }
            catch (Exception)
            {
                result = Task.FromResult(0);
            }


            return result;


        }




        public void SendAsyncMessage(string FromUser, string ToUser, string subject, string body)
        {

            var toAddress = new System.Net.Mail.MailAddress(ToUser, ToUser);

            var gs = db.GeneralSettings.ToArray();

            try
            {

                System.Net.Mail.MailMessage emessage = new System.Net.Mail.MailMessage();


                string Username = gs.FirstOrDefault(i => i.SettingKey == "Email").SettingValue;
                string Password = gs.FirstOrDefault(i => i.SettingKey == "EmailPassword").SettingValue;
                string port = gs.FirstOrDefault(i => i.SettingKey == "EmailPort").SettingValue;
                string SmptServer = gs.FirstOrDefault(i => i.SettingKey == "EmailServer").SettingValue;

                emessage.Subject = subject;
                emessage.From = toAddress;
                emessage.Body = body;
                emessage.IsBodyHtml = true;
                System.Net.Mail.SmtpClient sc = new System.Net.Mail.SmtpClient();
                var netCredential = new System.Net.NetworkCredential(Username, Password);
                sc.Host = SmptServer;
                sc.DeliveryMethod = SmtpDeliveryMethod.Network;
                sc.UseDefaultCredentials = false;
                sc.Credentials = netCredential;
                sc.Port = Convert.ToInt32(port);
                sc.Send(emessage);

            }
            catch (Exception)
            {

            }


        }





















        // GET: /EmailMessage/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailMessage ObjEmailMessage = db.EmailMessages.Find(id);
            if (ObjEmailMessage == null)
            {
                return HttpNotFound();
            }
            ViewBag.FromUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjEmailMessage.FromUserId);

            return View(ObjEmailMessage);
        }

        // POST: /EmailMessage/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(EmailMessage ObjEmailMessage)
        {
            if (ModelState.IsValid)
            {


                db.Entry(ObjEmailMessage).State = EntityState.Modified;
                db.SaveChanges();
                return Redirect(TempData["ThisUrl"].ToString());
            }
            ViewBag.FromUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjEmailMessage.FromUserId);

            return View(ObjEmailMessage);
        }
        // GET: /EmailMessage/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailMessage ObjEmailMessage = db.EmailMessages.Find(id);
            if (ObjEmailMessage == null)
            {
                return HttpNotFound();
            }
            return View(ObjEmailMessage);
        }

        // POST: /EmailMessage/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EmailMessage ObjEmailMessage = db.EmailMessages.Find(id);
            db.EmailMessages.Remove(ObjEmailMessage);
            db.SaveChanges();
            return Redirect(TempData["ThisUrl"].ToString());
        }
        // GET: /EmailMessage/MultiViewIndex/5
        public ActionResult MultiViewIndex(int? id)
        {
            EmailMessage ObjEmailMessage = db.EmailMessages.Find(id);
            ViewBag.IsWorking = 0;
            if (id > 0)
            {
                ViewBag.IsWorking = id;
                ViewBag.FromUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjEmailMessage.FromUserId);

            }

            return View(ObjEmailMessage);
        }

        // POST: /EmailMessage/MultiViewIndex/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult MultiViewIndex(EmailMessage ObjEmailMessage)
        {
            if (ModelState.IsValid)
            {


                db.Entry(ObjEmailMessage).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("MultiViewIndex", "EmailMessage", new { id = ObjEmailMessage.Id });
            }
            ViewBag.FromUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjEmailMessage.FromUserId);

            return View(ObjEmailMessage);
        }
        private SIContext db = new SIContext();

        private ActionResult ModelBind()
        {
            var EmailMessages = db.EmailMessages;
            return View(EmailMessages.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}