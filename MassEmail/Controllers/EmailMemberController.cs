using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using MassEmail.Models;

namespace MassEmail.Controllers
{
    public class EmailMemberController : Controller
    { 
        // GET: /EmailMember/
        public ActionResult Index()
        {
            return ModelBind();
        }
        
        // GET EmailMember/GetGrid
        public ActionResult GetGrid()
        {
            var tak = db.EmailMembers.ToArray();
             
            var result = from c in tak select new string[] { c.Id.ToString(), Convert.ToString(c.Id),Convert.ToString(c.EmailMessage_EmailMessageId.Subject),Convert.ToString(c.ApplicationUser_ToUserId.Username),Convert.ToString(c.IsRead),Convert.ToString(c.DateUpdated), };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }
        // GET: /EmailMember/ModelBindIndex
        public ActionResult ModelBindIndex()
        {
            return ModelBind();
        }
        // GET: /EmailMember/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailMember ObjEmailMember = db.EmailMembers.Find(id);
            if (ObjEmailMember == null)
            {
                return HttpNotFound();
            }
            return View(ObjEmailMember);
        }
        // GET: /EmailMember/Create
        public ActionResult Create()
        {
             ViewBag.EmailMessageId = new SelectList(db.EmailMessages, "Id", "Subject");
ViewBag.ToUserId = new SelectList(db.ApplicationUsers, "Id", "Username");

             return View();
        }

        // POST: /EmailMember/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(EmailMember ObjEmailMember )
        {
            if (ModelState.IsValid)
            {


                db.EmailMembers.Add(ObjEmailMember);
                db.SaveChanges();
                return Redirect(TempData["ThisUrl"].ToString());
            }
             ViewBag.EmailMessageId = new SelectList(db.EmailMessages, "Id", "Subject", ObjEmailMember.EmailMessageId);
ViewBag.ToUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjEmailMember.ToUserId);

             return View(ObjEmailMember);
        }
        // GET: /EmailMember/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailMember ObjEmailMember = db.EmailMembers.Find(id);
            if (ObjEmailMember == null)
            {
                return HttpNotFound();
            }
            ViewBag.EmailMessageId = new SelectList(db.EmailMessages, "Id", "Subject", ObjEmailMember.EmailMessageId);
ViewBag.ToUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjEmailMember.ToUserId);

            return View(ObjEmailMember);
        }

        // POST: /EmailMember/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(EmailMember ObjEmailMember )
        {
            if (ModelState.IsValid)
            {
                

                db.Entry(ObjEmailMember).State = EntityState.Modified;
                db.SaveChanges();
                return Redirect(TempData["ThisUrl"].ToString());
            }
            ViewBag.EmailMessageId = new SelectList(db.EmailMessages, "Id", "Subject", ObjEmailMember.EmailMessageId);
ViewBag.ToUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjEmailMember.ToUserId);

            return View(ObjEmailMember);
        }
        // GET: /EmailMember/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailMember ObjEmailMember = db.EmailMembers.Find(id);
            if (ObjEmailMember == null)
            {
                return HttpNotFound();
            }
            return View(ObjEmailMember);
        }

        // POST: /EmailMember/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EmailMember ObjEmailMember = db.EmailMembers.Find(id);
            db.EmailMembers.Remove(ObjEmailMember);
            db.SaveChanges();
            return Redirect(TempData["ThisUrl"].ToString());
        }
        // GET: /EmailMember/MultiViewIndex/5
        public ActionResult MultiViewIndex(int? id)
        { 
            EmailMember ObjEmailMember = db.EmailMembers.Find(id);
            ViewBag.IsWorking = 0;
            if (id > 0)
            {
                ViewBag.IsWorking = 1;
                ViewBag.EmailMessageId = new SelectList(db.EmailMessages, "Id", "Subject", ObjEmailMember.EmailMessageId);
ViewBag.ToUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjEmailMember.ToUserId);

            }
            
            return View(ObjEmailMember);
        }

        // POST: /EmailMember/MultiViewIndex/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult MultiViewIndex(EmailMember ObjEmailMember )
        {
            if (ModelState.IsValid)
            {
                

                db.Entry(ObjEmailMember).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("MultiViewIndex", "EmailMember", new { id = ObjEmailMember.Id });
            }
            ViewBag.EmailMessageId = new SelectList(db.EmailMessages, "Id", "Subject", ObjEmailMember.EmailMessageId);
ViewBag.ToUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjEmailMember.ToUserId);

            return View(ObjEmailMember);
        }
        private SIContext db = new SIContext();
		
		private ActionResult ModelBind()
        {
            var EmailMembers = db.EmailMembers;
            return View(EmailMembers.ToList());
        }
		
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        
		 
    }
}

