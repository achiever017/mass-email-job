﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using MassEmail.Models;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Threading;
namespace MassEmail.Controllers
{
    public class EventMemberController : Controller
    {
        private SIContext db = new SIContext();

        public ActionResult Index()
        {
            return ModelBind();
        }

        private ActionResult ModelBind()
        {
            int intCurrentUserId = Convert.ToInt32(Env.GetUserInfo("userid"));
            int[] intEventIds = db.EventMembers.Where(e => e.ReceiverId == intCurrentUserId).Select(e => e.EventId).ToArray();
            var EventMembers = (from e in db.EventMembers
                          where intEventIds.Contains(e.EventId)
                          select e).ToList();
            return View(EventMembers.ToList());
        }

        // GET Announcement/GetGrid
        public ActionResult GetGrid()
        {
            int user = Convert.ToInt32(Env.GetUserInfo("userid"));

            var tak = db.EventMembers.Where(i => i.ReceiverId == user).ToArray().OrderByDescending(x => x.Obj_Event.SendDate);

            var result = from c in tak select new string[] { 
                Convert.ToString(c.EventMemberId), 
                Convert.ToString("<a style='cursor:pointer !important;' onclick='expand1(" + c.EventMemberId + ")'>"+ c.Obj_Event.Subject +"</a>"),
                Convert.ToString(c.Obj_Event.FileUrl),
                Convert.ToString(c.IsRead), fuid(c.Obj_Event.SenderId), 
                Convert.ToString(c.Obj_Event.SendDate), 
                Convert.ToString(c.EventStatus) };

            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        private string fuid(int? nullable)
        {
            string fu = "";
            try
            {
                var data = db.ApplicationUsers.FirstOrDefault(x => x.Id == nullable);
                fu = data.FirstName + ' ' + data.LastName;
            }
            catch (Exception ex)
            {
            }

            return fu;
        }

        // GET: /Event/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            EventMember eventMember = db.EventMembers.Find(id);
            
            if (eventMember == null)
            {
                return HttpNotFound();
            }
            
            eventMember.IsRead = true;
            db.Entry(eventMember).State = EntityState.Modified;
            db.SaveChanges();
           
            return View(eventMember);
        }

        [HttpPost]
        public async Task<ActionResult> EventAccepted(int Id)
        {
            try
            {
                EventMember myEvent = db.EventMembers.FirstOrDefault(e => e.EventMemberId == Id);

                DateTime d = DateTime.Now;
               d= d.AddHours(1);

                TimeSpan t =  myEvent.Obj_Event.SendDate-d;
                if (t.Minutes > 30)
                {
                    Thread thread = new Thread(() => sendemail(t));
                    thread.Start();
                }
             //  await ;
               
                myEvent.EventStatus = 2;
                db.Entry<EventMember>(myEvent).State = EntityState.Modified;
                db.SaveChanges();

         
                return  Content("Kabul");
            }
            catch (Exception)
            {
                return Content("False");
            }
        }
        
        public ActionResult GetGridAll1(AnnouncementMember d)
        {

            var data = d.Announcement_EmailMessageId;
            var tak = db.AnnouncementMembers.ToArray();

            int eventid = db.EventMembers.Where(x=>x.EventMemberId==d.Id).FirstOrDefault().EventId;

            var result = from c in db.Events
                              where c.EventId == eventid
                         select new
                         {
                             sub = c.Subject,
                             body = c.Body,
                         };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

       
        public void sendemail(TimeSpan a) {

           // System.Threading.Thread.SpinWait(1000000);
            //the above line is working as i want but it stop the whole functionalityo :( :'(
         //   await Task.Delay(a);

            Thread.Sleep(a);
            SIContext s = new SIContext();
            var gs=s.GeneralSettings.ToArray();
         SendAsync("admin@gmail.com",Env.GetUserInfo("name"),"Hatırlatma","1 Saat Sonra Etkinliğiniz Başlayacaktır. Teşekkürler",gs,null);
        
        }
        public Task SendAsync(string FromUser, string ToUser, string subject, string body, GeneralSetting[] gs, System.Web.HttpPostedFileBase[] filesAttach)
        {
            var result = Task.FromResult(0);
            var toAddress = new System.Net.Mail.MailAddress(ToUser, ToUser);


            try
            {

                System.Net.Mail.MailMessage emessage = new System.Net.Mail.MailMessage();
            

                string Username = gs.FirstOrDefault(i => i.SettingKey == "Email").SettingValue;
                string Password = gs.FirstOrDefault(i => i.SettingKey == "EmailPassword").SettingValue;
                string port = gs.FirstOrDefault(i => i.SettingKey == "EmailPort").SettingValue;
                string SmptServer = gs.FirstOrDefault(i => i.SettingKey == "EmailServer").SettingValue;

                emessage.To.Add(toAddress);
                emessage.Subject = subject;
                emessage.From = new System.Net.Mail.MailAddress(FromUser);
                emessage.Body = body;
                emessage.IsBodyHtml = true;
                System.Net.Mail.SmtpClient sc = new System.Net.Mail.SmtpClient();
                var netCredential = new System.Net.NetworkCredential(Username, Password);
                sc.Host = SmptServer;
                sc.DeliveryMethod = SmtpDeliveryMethod.Network;
                sc.UseDefaultCredentials = false;
                sc.Credentials = netCredential;
                sc.Port = Convert.ToInt32(port);
                sc.Send(emessage);



                result = Task.FromResult(1);
            }
            catch (Exception)
            {
                result = Task.FromResult(0);
            }


            return result;


        }




        [HttpPost]
        public ActionResult EventRejected(int Id, string Reason)
        {
            try
            {
                EventMember myEvent = db.EventMembers.FirstOrDefault(e => e.EventMemberId == Id);
                myEvent.EventStatus = 3;
                myEvent.RejectReason = Reason; //Save submitted rejection here
                db.Entry<EventMember>(myEvent).State = EntityState.Modified;
                db.SaveChanges();
               // return Redirect("EventMember");
                return Content("Red");

            }
            catch (Exception)
            {
                return Content("False");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
