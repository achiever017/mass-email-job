﻿using MassEmail.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MassEmail.Controllers.Filters
{
    public class AuthorizationAndRoleFilter : ActionFilterAttribute
    {

        public AccessRole accessRole { get; set; }
        private MassEmail.Models.ApplicationUser user;
        public ActionExecutingContext Context { get; set; }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            this.Context = filterContext;
            var userId = Convert.ToInt32(Env.GetUserInfo("userid"));
            this.user = new SIContext().ApplicationUsers.FirstOrDefault(i => i.Id == userId);
            if(this.user == null)
                SendToHome();
            else
            {
                if (user.RoleUser_UserIds.Where(f => f.RoleId == 2).FirstOrDefault() != null)
                {
                    if (!CheckPermission())
                        SendToHome();
                }
            }
        }
        private void SendToHome()
        {
            var url = new UrlHelper(Context.Controller.ControllerContext.RequestContext);
            Context.Result = new RedirectResult(url.Action("Index", "Home"));
        }
        private bool CheckPermission()
        {
            switch (accessRole)
            {
                case AccessRole.Announcement:
                    return this.user.AnnouncementCreat == true ? true : false;
                case AccessRole.Event:
                    return this.user.EventCreat == true ? true : false;
                case AccessRole.FavoriteGroup:
                    return this.user.EventCreat == true && this.user.AnnouncementCreat == true ? true : false;
                default:
                    return false;
            }
        }

    }
    public enum AccessRole
    {
        Announcement,
        Event,
        FavoriteGroup
    }
}