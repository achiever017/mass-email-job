﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using MassEmail.Models;
using System.Text;

namespace MassEmail.Controllers
{
    public class AnnouncementMemberController : Controller
    {
        // GET: /AnnouncementMember/
        public ActionResult Index()
        {
            return ModelBind();
        }

        // GET AnnouncementMember/GetGrid the code ıs perfect but not working :S
        public ActionResult GetGrid()
        {
            int user = Convert.ToInt32(Env.GetUserInfo("userid"));
            var tak = db.AnnouncementMembers.Where(i => i.ToUserId == user).OrderByDescending(x => x.Announcement_EmailMessageId.DateSend).Take(50).ToArray();
            var users = db.ApplicationUsers.ToArray();
            var fi = db.Announcements.ToArray();
            var result = from c in tak
                         select new string[] {
                               Convert.ToString(c.Id), 
                Convert.ToString("<a style='cursor:pointer !important;' onclick='expand1(" + c.Id + ")'>" + c.Announcement_EmailMessageId.Subject + "</a>"),
                Convert.ToString(c.Announcement_EmailMessageId.DateSend), 
                Convert.ToString(users.FirstOrDefault(i => i.Id == c.Announcement_EmailMessageId.FromUserId) != null ? users.FirstOrDefault(i => i.Id == c.Announcement_EmailMessageId.FromUserId).FirstName + ' ' + users.FirstOrDefault(i => i.Id == c.Announcement_EmailMessageId.FromUserId).LastName : ""), 
                Convert.ToString(c.IsRead),
                Convert.ToString(c.Announcement_EmailMessageId.FileUrl),    
            };


            //string[] result=null;
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        private string fuid(int? nullable)
        {
            string fu = "";
            try
            {
                var data = db.ApplicationUsers.FirstOrDefault(x => x.Id == nullable);
                fu = data.FirstName + ' ' + data.LastName;
            }
            catch (Exception)
            {
            }

            return fu;
        }

        public ActionResult GetGridAll1(AnnouncementMember d)
        {
            var data = d.Announcement_EmailMessageId;
            var tak = db.AnnouncementMembers.ToArray();

            var result = from c in db.AnnouncementMembers
                         where c.Id == d.Id
                         select new
                         {
                             sub = c.Announcement_EmailMessageId.Subject,
                             body = c.Announcement_EmailMessageId.MailText,
                         };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetGridAll(AnnouncementMember d)
        {

            var tak = db.AnnouncementMembers.ToArray();

            var result = from c in tak select new string[] { Convert.ToString(c.Announcement_EmailMessageId.Subject), Convert.ToString(c.IsRead), Convert.ToString(c.ApplicationUser_ToUserId.FirstName + ' ' + c.ApplicationUser_ToUserId.LastName), Convert.ToString(c.Announcement_EmailMessageId.DateSend), Convert.ToString(c.Announcement_EmailMessageId.FileUrl), };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }
        // GET: /AnnouncementMember/ModelBindIndex
        public ActionResult ModelBindIndex()
        {
            return ModelBind();
        }

        public ActionResult Notification()
        {
            StringBuilder sb = new StringBuilder();
            int user = Convert.ToInt32(Env.GetUserInfo("userid"));

            var takunread = db.AnnouncementMembers.Where(i => i.ToUserId == user && i.IsRead == false).ToArray();

            var tak = db.AnnouncementMembers.Where(i => i.ToUserId == user && i.IsRead == false).OrderByDescending(x => x.Announcement_EmailMessageId.DateSend).Take(50).ToArray();


            //    var events = db.EventMembers.Where(e => e.ReceiverId == user && e.IsRead == false).ToArray();

            int intCount = takunread.Count();

            sb.Append("<a href=\"#\" data-toggle=\"dropdown\" data-hover=\"dropdown\" data-close-others=\"true\"><i class=\"glyphicon glyphicon-warning-sign\"></i>");
            sb.Append("<span class=\"badge badge-danger badge-header\">" + intCount + "</span></a>");

            sb.Append("<ul class=\"dropdown-menu\" style=\"margin-right: -58px;\">");
            sb.Append("<li class=\"dropdown-header clearfix\"><p class=\"pull-left\">Bildirimler</p></li><li class=\"dropdown-body\"><ul style=\"margin-top:-30px\" class=\"dropdown-menu-list withScroll\" data-height=\"220\">");

            foreach (var item in tak)
            {

                sb.Append("<li class=\"clearfix\"><a href=\"/AnnouncementMember/Details/" + item.Id + "\"> <i class=\"fa fa-bullhorn \"></i>  " + (item.Announcement_EmailMessageId.Subject.Length > 20 ? item.Announcement_EmailMessageId.Subject.Substring(0, 25) + "..." : item.Announcement_EmailMessageId.Subject) + " </a> </li>");
            }

            //foreach (var item in events)
            //{
            //    sb.Append("<li> <a href=\"/EventMember/Details/" + item.EventMemberId + "\"> <i class=\"ion ion-ios7-person \"></i>" + item.Obj_Event.Subject + "</a> </li>");
            //}

            sb.Append("</ul></li><li class=\"dropdown-footer clearfix\"><a href=\"/AnnouncementMember/Index\">Tüm Bildirimleri Görüntüle</a></li> </ul>");

            ViewBag.sbs = sb.ToString();

            return PartialView();
        }




        // GET: /AnnouncementMember/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnnouncementMember ObjAnnouncementMember = db.AnnouncementMembers.Find(id);
            ObjAnnouncementMember.IsRead = true;
            db.Entry(ObjAnnouncementMember).State = EntityState.Modified;
            db.SaveChanges();

            if (ObjAnnouncementMember == null)
            {
                return HttpNotFound();
            }
            return View(ObjAnnouncementMember);
        }
        // GET: /AnnouncementMember/Create
        public ActionResult Create()
        {
            ViewBag.EmailMessageId = new SelectList(db.Announcements, "Id", "Subject");
            ViewBag.ToUserId = new SelectList(db.ApplicationUsers, "Id", "Username");

            return View();
        }

        // POST: /AnnouncementMember/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(AnnouncementMember ObjAnnouncementMember)
        {
            if (ModelState.IsValid)
            {


                db.AnnouncementMembers.Add(ObjAnnouncementMember);
                db.SaveChanges();
                return Redirect(TempData["ThisUrl"].ToString());
            }
            ViewBag.EmailMessageId = new SelectList(db.Announcements, "Id", "Subject", ObjAnnouncementMember.EmailMessageId);
            ViewBag.ToUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjAnnouncementMember.ToUserId);

            return View(ObjAnnouncementMember);
        }
        // GET: /AnnouncementMember/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnnouncementMember ObjAnnouncementMember = db.AnnouncementMembers.Find(id);
            if (ObjAnnouncementMember == null)
            {
                return HttpNotFound();
            }
            ViewBag.EmailMessageId = new SelectList(db.Announcements, "Id", "Subject", ObjAnnouncementMember.EmailMessageId);
            ViewBag.ToUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjAnnouncementMember.ToUserId);

            return View(ObjAnnouncementMember);
        }

        // POST: /AnnouncementMember/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(AnnouncementMember ObjAnnouncementMember)
        {
            if (ModelState.IsValid)
            {


                db.Entry(ObjAnnouncementMember).State = EntityState.Modified;
                db.SaveChanges();
                return Redirect(TempData["ThisUrl"].ToString());
            }
            ViewBag.EmailMessageId = new SelectList(db.Announcements, "Id", "Subject", ObjAnnouncementMember.EmailMessageId);
            ViewBag.ToUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjAnnouncementMember.ToUserId);

            return View(ObjAnnouncementMember);
        }
        // GET: /AnnouncementMember/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnnouncementMember ObjAnnouncementMember = db.AnnouncementMembers.Find(id);
            if (ObjAnnouncementMember == null)
            {
                return HttpNotFound();
            }
            return View(ObjAnnouncementMember);
        }

        // POST: /AnnouncementMember/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AnnouncementMember ObjAnnouncementMember = db.AnnouncementMembers.Find(id);
            db.AnnouncementMembers.Remove(ObjAnnouncementMember);
            db.SaveChanges();
            return Redirect(TempData["ThisUrl"].ToString());
        }
        // GET: /AnnouncementMember/MultiViewIndex/5
        public ActionResult MultiViewIndex(int? id)
        {
            AnnouncementMember ObjAnnouncementMember = db.AnnouncementMembers.Find(id);
            ViewBag.IsWorking = 0;
            if (id > 0)
            {
                ViewBag.IsWorking = 1;
                ViewBag.EmailMessageId = new SelectList(db.Announcements, "Id", "Subject", ObjAnnouncementMember.EmailMessageId);
                ViewBag.ToUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjAnnouncementMember.ToUserId);

            }

            return View(ObjAnnouncementMember);
        }

        // POST: /AnnouncementMember/MultiViewIndex/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult MultiViewIndex(AnnouncementMember ObjAnnouncementMember)
        {
            if (ModelState.IsValid)
            {


                db.Entry(ObjAnnouncementMember).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("MultiViewIndex", "AnnouncementMember", new { id = ObjAnnouncementMember.Id });
            }
            ViewBag.EmailMessageId = new SelectList(db.Announcements, "Id", "Subject", ObjAnnouncementMember.EmailMessageId);
            ViewBag.ToUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjAnnouncementMember.ToUserId);

            return View(ObjAnnouncementMember);
        }
        private SIContext db = new SIContext();

        private ActionResult ModelBind()
        {
            var AnnouncementMembers = db.AnnouncementMembers;
            return View(AnnouncementMembers.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }



    }
}

