﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using MassEmail.Models;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using Microsoft.AspNet.SignalR;
using MassEmail.ChatClass;
using MassEmail.Helper;
using MassEmail.Controllers.Filters;

namespace MassEmail.Controllers
{
    
    public class AnnouncementController : Controller
    {
        // GET: /Announcement/
        [AuthorizationAndRoleFilter(accessRole=AccessRole.Announcement)]
        public ActionResult Index()
        {
            return ModelBind();
        }

        // GET Announcement/GetGrid
        public ActionResult GetGrid()
        {
            var tak = db.Announcements.Take(50).ToArray().OrderByDescending(x => x.DateSend);
            var users = db.ApplicationUsers.ToArray();
            var us = db.AnnouncementMembers.ToArray();
            //BRB
            var result = from c in tak select new string[] { 
                Convert.ToString(c.Id), 
                Convert.ToString("<a style='cursor:pointer !important;' onclick='expand1(" + c.Id + ")'>" + c.Subject + "</a>"), 
                Convert.ToString(c.DateSend), 
                Convert.ToString(c.FileUrl), 
                Convert.ToString(users.FirstOrDefault(i => i.Id == c.FromUserId) != null ? users.FirstOrDefault(i => i.Id == c.FromUserId).FirstName + ' ' + users.FirstOrDefault(i => i.Id == c.FromUserId).LastName : ""), 
                newfunc(c), };

            var dsdsf = result = result.Skip(Math.Max(0, result.Count() - 20)).ToList();

            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        private string sfunc(Announcement c)
        {
            string tu = "";
            var data = c.AnnouncementMember_EmailMessageIds;
            foreach (var item in data)
            {
                
                tu += item.ApplicationUser_ToUserId.FirstName + ' ' + item.ApplicationUser_ToUserId.LastName;
                tu += " , ";
            }
            tu = tu.TrimEnd(' ');
            tu = tu.TrimEnd(',');
            

            return tu;
        }

        public ActionResult GetGridAll1(Announcement d)
        {
            var data = d.AnnouncementMember_EmailMessageIds;
            var tak = db.Announcements.ToArray();

            var result = from c in db.Announcements
                         where c.Id == d.Id
                         select new
                         {
                sub = c.Subject,
            body = c.MailText,
            };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetGridAll(Announcement d)
        {

            var data = d.AnnouncementMember_EmailMessageIds;
            var tak = from c in db.AnnouncementMembers where c.EmailMessageId==d.Id

                              select new
                              {
                                  name = c.ApplicationUser_ToUserId.FirstName + " " + c.ApplicationUser_ToUserId.LastName,
                                  read=c.IsRead,
                              };
            //var result = from c in tak select new string[] { Convert.ToString(c.ApplicationUser_ToUserId.FirstName + ' ' + c.ApplicationUser_ToUserId.LastName), Convert.ToString(c.IsRead),};
            return Json(new { aaData = tak }, JsonRequestBehavior.AllowGet);


        }
        private string newfunc(Announcement c)
        {
            string tu = "";
            var data = c.AnnouncementMember_EmailMessageIds;
            //foreach (var item in data)
            //{
                
            //    tu += item.ApplicationUser_ToUserId.FirstName + ' ' + item.ApplicationUser_ToUserId.LastName;
            //    tu += " , ";
            //}

            tu = "<a style='cursor:pointer !important;' onclick='expand(" + c.Id + ")'>" + "<i class='fa fa-check-square-o f18'>" + "</a>";
            //tu = tu.TrimEnd(' ');
            //tu = tu.TrimEnd(',');


            return tu;
        }

        //
        // GET: /Announcement/ModelBindIndex
        public ActionResult ModelBindIndex()
        {
            return ModelBind();
        }
        // GET: /Announcement/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Announcement ObjAnnouncement = db.Announcements.Find(id);
            if (ObjAnnouncement == null)
            {
                return HttpNotFound();
            }
            return View(ObjAnnouncement);
        }
        // GET: /Announcement/Create
        public ActionResult CreateOLd()
        {
            ViewBag.FromUserId = new SelectList(db.ApplicationUsers, "Id", "Username");

            return View();
        }

        // POST: /Announcement/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult CreateOLd(Announcement ObjAnnouncement)
        {
            if (ModelState.IsValid)
            {
                db.Announcements.Add(ObjAnnouncement);
                db.SaveChanges();
                return Redirect(TempData["ThisUrl"].ToString());
            }
            ViewBag.FromUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjAnnouncement.FromUserId);

            return View(ObjAnnouncement);
        }






        [AuthorizationAndRoleFilter(accessRole = AccessRole.Announcement)]
        public ActionResult Create()
        {
            LoadBinding();
            return View();
        }

        private void LoadBinding()
        {
            var UsersList = db.ApplicationUsers.OrderBy(i => i.Department_DepartmentId.Title).ToArray(); 
            int cUserId = Convert.ToInt32(Env.GetUserInfo("userid"));

            StringBuilder sbUser = new StringBuilder();
            sbUser.Append("[");

            foreach (var item in UsersList.Where(i => i.Id != cUserId).ToArray())
            {
                sbUser.Append("{\"id\":\"" + item.Email + "\", \"name\":\"" + item.FirstName + " " + item.LastName + " " + "( " + item.Department_DepartmentId.Title + " )" + "\"},");
            }
            sbUser.Append("]");
            ViewBag.UserList = sbUser.ToString();




            StringBuilder sbDepartment = new StringBuilder();
            sbDepartment.Append("[");

            foreach (var item in db.Departments.OrderBy(x => x.Title).ToArray())
            {
                sbDepartment.Append("{\"id\":\"" + item.Title + "\", \"name\":\"" + item.Title + "\"},");
            }
            sbDepartment.Append("]");
            ViewBag.Departments = sbDepartment.ToString();

            StringBuilder sbFavoriteGroups = new StringBuilder();
            sbFavoriteGroups.Append("[");

            foreach (var item in db.FavoriteGroups.OrderBy(x => x.Name).ToArray())
            {
                sbFavoriteGroups.Append("{\"id\":\"" + item.Id + "\", \"name\":\"" + item.Name + "(" + prepareUsersName(item.Users) + ")" + "\"},");
            }
            sbFavoriteGroups.Append("]");
            ViewBag.FavoriteGroups = sbFavoriteGroups.ToString();

            ViewBag.FromUserId = new SelectList(UsersList, "Id", "Username", cUserId);
        }

        private string prepareUsersName(ICollection<ApplicationUser> users)
        {
            string usersName = "";
            foreach (var user in users)
            {
                usersName += user.CompleteName + ", ";
            }
            return usersName.Remove(usersName.Length - 2);
        }

        private string notify(int? id)
        {
            StringBuilder sb = new StringBuilder();
            //   int user = Convert.ToInt32(Env.GetUserInfo("userid"));
            var tak = db.AnnouncementMembers.Where(i => i.ToUserId == id && i.IsRead == false).ToArray();

            int intCount = tak.Count();

            sb.Append("<a href=\"#\" data-toggle=\"dropdown\" data-hover=\"dropdown\" data-close-others=\"true\"><i class=\"glyphicon glyphicon-warning-sign\"></i>");
            sb.Append("<span class=\"badge badge-danger badge-header\">" + intCount + "</span></a>");

            sb.Append("<ul class=\"dropdown-menu\" style=\"margin-right: -58px;\">");
            sb.Append("<li class=\"dropdown-header clearfix\"><p class=\"pull-left\">Bildirimler</p></li><li class=\"dropdown-body\"><ul style=\"margin-top:-30px\" class=\"dropdown-menu-list withScroll\" data-height=\"220\">");

            foreach (var item in tak)
            {

                sb.Append("<li class=\"clearfix\"><a href=\"/AnnouncementMember/Details/" + item.Id + "\"> <i class=\"fa fa-bullhorn \"></i>  " + (item.Announcement_EmailMessageId.Subject.Length > 20 ? item.Announcement_EmailMessageId.Subject.Substring(0, 25) + "..." : item.Announcement_EmailMessageId.Subject) + " </a> </li>");
            }

            //foreach (var item in events)
            //{
            //    sb.Append("<li> <a href=\"/EventMember/Details/" + item.EventMemberId + "\"> <i class=\"ion ion-ios7-person \"></i>" + item.Obj_Event.Subject + "</a> </li>");
            //}

            sb.Append("</ul></li><li class=\"dropdown-footer clearfix\"><a href=\"/AnnouncementMember/Index\">Tüm Bildirimleri Görüntüle</a></li> </ul>");

            ViewBag.sbs = sb.ToString();

            return sb.ToString();
        }


        // POST: /EmailMessage/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [AuthorizationAndRoleFilter(accessRole = AccessRole.Announcement)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(Announcement ObjAnnouncement, string sl, string toEmails, string toDepEmails, string toFavoriteEmails, HttpPostedFileBase[] filesAttach)
        {
            LoadBinding();
            if (ModelState.IsValid)
            {
                var gs = db.GeneralSettings.ToArray();
                bool isAnnouncementAdded = false;
                var AllUsers = db.ApplicationUsers.ToArray();

                if (toEmails.Length > 0)
                {
                    string filename = "";
                    if (filesAttach.Count() >= 1)
                    {
                        foreach (HttpPostedFileBase file in filesAttach)
                        {
                            if (file != null)
                            {
                                /*Geting the file name*/
                                filename = System.IO.Path.GetFileName(file.FileName);
                                /*Saving the file in server folder*/
                                file.SaveAs(Server.MapPath("~/Uploads/" + filename));
                            }

                        }

                    }
                    ObjAnnouncement.FileUrl = filename;
                    db.Announcements.Add(ObjAnnouncement);
                    db.SaveChanges();
                    isAnnouncementAdded = true;





                    string[] emails = toEmails.Split(',');
                    if (emails.Length <= 0)
                    {
                        ViewBag.Msg = "Alıcı Seçiniz";
                        return View("Create");
                    }
                    foreach (var item in emails)
                    {
                        if (item != "")
                        {
                            AnnouncementMember member = new AnnouncementMember();
                            member.EmailMessageId = ObjAnnouncement.Id;
                            member.ToUserId = AllUsers.FirstOrDefault(i => i.Username == item.Trim()).Id;
                            member.IsRead = false;
                            db.AnnouncementMembers.Add(member);
                            db.SaveChanges();

                            string s = notify(member.ToUserId);
                            Help obj = new Help();
                            string dn = obj.dashnotification(member.ToUserId);
                            var hubContext = GlobalHost.ConnectionManager.GetHubContext<MyHub>();

                            hubContext.Clients.User(item.Trim()).testing(s, dn);

                            // PushNotification.SendNotification(member.ApplicationUser_ToUserId.deviceID, "Yeni Bir Bildiriminiz Var: " + Env.GetUserInfo("name") + " - " + ObjAnnouncement.Subject);
                            //TODO: Set correct Turkish text here.
                            SendAsync(Env.GetUserInfo("name"), item, ObjAnnouncement.Subject, ObjAnnouncement.MailText + " <a href='" + Request.Url.Scheme + Uri.SchemeDelimiter + Request.Url.Authority + "/AnnouncementMember/Details/" + member.Id.ToString() + "'>Detaylı Bilgi</a> için Tıklayın.", gs, filesAttach);
                        }
                    }
                }
                if (toDepEmails.Length > 0)
                {
                    if (!isAnnouncementAdded)
                    {
                        db.Announcements.Add(ObjAnnouncement);
                        db.SaveChanges();
                    }
                    string[] departm = toDepEmails.Split(',');
                    if (departm.Length <= 0)
                    {
                        ViewBag.Msg = "Alıcı Seçiniz";
                        return View("Create");
                    }
                    foreach (var item in departm)
                    {
                        if (item != "")
                        {
                            var users = db.ApplicationUsers.Where(i => i.Department_DepartmentId.Title == item.Trim()).ToArray();
                            foreach (var user in users)
                            {
                                AnnouncementMember member = new AnnouncementMember();
                                member.EmailMessageId = ObjAnnouncement.Id;
                                member.ToUserId = user.Id;
                                member.IsRead = false;
                                db.AnnouncementMembers.Add(member);
                                db.SaveChanges();
                                string s = notify(member.ToUserId);
                                Help obj = new Help();
                                string dn = obj.dashnotification(member.ToUserId);
                                var hubContext = GlobalHost.ConnectionManager.GetHubContext<MyHub>();

                                hubContext.Clients.User(user.Username).testing(s, dn);

                                //PushNotification.SendNotification(member.ApplicationUser_ToUserId.deviceID, "Yeni Bir Bildiriminiz Var: " + Env.GetUserInfo("name") + " - " + ObjAnnouncement.Subject);

                                //
                                SendAsync(Env.GetUserInfo("name"), item, ObjAnnouncement.Subject, ObjAnnouncement.MailText, gs, filesAttach);
                                //SendAsync(Env.GetUserInfo("name"), user.Username, "Aykome Yeni Etkinlik", "Etkinliğimize davetlisiniz. <a href='" + Request.Url.Scheme + Uri.SchemeDelimiter + Request.Url.Authority + "/AnnouncementMember/Details/" + member.Id.ToString() + "'>Detaylı Bilgi</a> için Tıklayın.", gs, filesAttach);
                            }
                        }
                    }
                }
                if (toFavoriteEmails.Length > 0)
                {
                    if (!isAnnouncementAdded)
                    {
                        db.Announcements.Add(ObjAnnouncement);
                        db.SaveChanges();
                    }
                    String[] Favorites = toFavoriteEmails.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    List<Int32> FavaritesId = new List<int>();
                    if (Favorites.Length <= 0)
                    {
                        ViewBag.Msg = "Alıcı Seçiniz";
                        return View("Create");
                    }
                    Favorites.ToList().ForEach(x => FavaritesId.Add(Convert.ToInt32(x)));
                    var favgroups = db.FavoriteGroups.Where(x => FavaritesId.Contains(x.Id)).ToArray();
                    var favUsersId = getIdUsersFromFavoriteGroups(favgroups);
                    foreach (var user in favUsersId)
                    {
                        var u = db.ApplicationUsers.Find(user);
                        AnnouncementMember member = new AnnouncementMember();
                        member.EmailMessageId = ObjAnnouncement.Id;
                        member.ToUserId = u.Id;
                        member.IsRead = false;
                        db.AnnouncementMembers.Add(member);
                        db.SaveChanges();
                        string s = notify(member.ToUserId);
                        Help obj = new Help();
                        string dn = obj.dashnotification(member.ToUserId);
                        var hubContext = GlobalHost.ConnectionManager.GetHubContext<MyHub>();

                        hubContext.Clients.User(u.Username).testing(s, dn);

                        //PushNotification.SendNotification(member.ApplicationUser_ToUserId.deviceID, "Yeni Bir Bildiriminiz Var: " + Env.GetUserInfo("name") + " - " + ObjAnnouncement.Subject);

                        //
                        SendAsync(Env.GetUserInfo("name"), u.CompleteName, ObjAnnouncement.Subject, ObjAnnouncement.MailText, gs, filesAttach);
                        //SendAsync(Env.GetUserInfo("name"), user.Username, "Aykome Yeni Etkinlik", "Etkinliğimize davetlisiniz. <a href='" + Request.Url.Scheme + Uri.SchemeDelimiter + Request.Url.Authority + "/AnnouncementMember/Details/" + member.Id.ToString() + "'>Detaylı Bilgi</a> için Tıklayın.", gs, filesAttach);
                    }

                }
                if (toEmails.Count() == 0 && toDepEmails.Count() == 0 && toFavoriteEmails.Count() == 0)
                {
                    ViewBag.Msg = "Kullanıcı veya Kurum Seçiniz";
                    return View("Create");
                }

                ViewBag.Success = "";
                return View("Create");

            }
            else
            {
                return View(ObjAnnouncement);
            }
        }
        private int[] getIdUsersFromFavoriteGroups(ICollection<FavoriteGroup> favoritegroups)
        {
            IList<int> usersId = new List<int>();
            foreach (var group in favoritegroups)
            {
                foreach (var user in group.Users)
                    usersId.Add(user.Id);
            }
            return usersId.ToArray();
        }
        public Task SendAsync(string FromUser, string ToUser, string subject, string body, GeneralSetting[] gs, System.Web.HttpPostedFileBase[] filesAttach)
        {
            var result = Task.FromResult(0);
            var toAddress = new System.Net.Mail.MailAddress(ToUser, ToUser);


            try
            {

                System.Net.Mail.MailMessage emessage = new System.Net.Mail.MailMessage();
                if (filesAttach.Count() >= 1)
                {
                    foreach (HttpPostedFileBase f in filesAttach)
                    {
                        if (f == null) { }
                        else
                        {
                            System.Net.Mail.Attachment at = new System.Net.Mail.Attachment(f.InputStream, f.FileName);
                            emessage.Attachments.Add(at);
                        }
                    }
                }

                string Username = gs.FirstOrDefault(i => i.SettingKey == "Email").SettingValue;
                string Password = gs.FirstOrDefault(i => i.SettingKey == "EmailPassword").SettingValue;
                string port = gs.FirstOrDefault(i => i.SettingKey == "EmailPort").SettingValue;
                string SmptServer = gs.FirstOrDefault(i => i.SettingKey == "EmailServer").SettingValue;

                emessage.To.Add(toAddress);
                emessage.Subject = subject;
                emessage.From = new System.Net.Mail.MailAddress(FromUser);
                emessage.Body = body;
                emessage.IsBodyHtml = true;
                System.Net.Mail.SmtpClient sc = new System.Net.Mail.SmtpClient();
                var netCredential = new System.Net.NetworkCredential(Username, Password);
                sc.Host = SmptServer;
                sc.DeliveryMethod = SmtpDeliveryMethod.Network;
                sc.UseDefaultCredentials = false;
                sc.Credentials = netCredential;
                sc.Port = Convert.ToInt32(port);
                sc.Send(emessage);



                result = Task.FromResult(1);
            }
            catch (Exception)
            {
                result = Task.FromResult(0);
            }


            return result;


        }
























        // GET: /Announcement/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Announcement ObjAnnouncement = db.Announcements.Find(id);
            if (ObjAnnouncement == null)
            {
                return HttpNotFound();
            }
            ViewBag.FromUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjAnnouncement.FromUserId);

            return View(ObjAnnouncement);
        }

        // POST: /Announcement/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(Announcement ObjAnnouncement)
        {
            if (ModelState.IsValid)
            {


                db.Entry(ObjAnnouncement).State = EntityState.Modified;
                db.SaveChanges();
                return Redirect(TempData["ThisUrl"].ToString());
            }
            ViewBag.FromUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjAnnouncement.FromUserId);

            return View(ObjAnnouncement);
        }
        // GET: /Announcement/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Announcement ObjAnnouncement = db.Announcements.Find(id);
            if (ObjAnnouncement == null)
            {
                return HttpNotFound();
            }
            return View(ObjAnnouncement);
        }

        // POST: /Announcement/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {

            Announcement ObjAnnouncement = db.Announcements.Find(id);
            BackupAnnouncement ba = new BackupAnnouncement()
            {
                DateSend = ObjAnnouncement.DateSend,
                FileUrl = ObjAnnouncement.FileUrl,
                DeletedBy = Env.GetUserInfo("name"),
                FromUserId = ObjAnnouncement.FromUserId,
                IsDelete = ObjAnnouncement.IsDelete,
                MailText = ObjAnnouncement.MailText,
                Subject = ObjAnnouncement.Subject,
                DeleteOn = DateTime.Now

            };
            db.BackupAnnouncements.Add(ba);
            var members = ObjAnnouncement.AnnouncementMember_EmailMessageIds.ToList();
            db.Announcements.Remove(ObjAnnouncement);

            // var data= db.AnnouncementMembers.Where(x=>x.)
            db.SaveChanges();
            foreach (var item in members)
            {
                string s = notify(item.ToUserId);
                Help obj = new Help();
                string dn = obj.dashnotification(item.ToUserId);
                var hubContext = GlobalHost.ConnectionManager.GetHubContext<MyHub>();
                var user = db.ApplicationUsers.FirstOrDefault(x => x.Id == item.ToUserId);
                hubContext.Clients.User(user.Email.Trim()).testing(s, dn);
            }
            return Redirect(TempData["ThisUrl"].ToString());
        }
        // GET: /Announcement/MultiViewIndex/5

        public ActionResult MultiViewIndex(int? id)
        {
            Announcement ObjAnnouncement = db.Announcements.Find(id);
            ViewBag.IsWorking = 0;
            if (id > 0)
            {
                ViewBag.IsWorking = 1;
                ViewBag.FromUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjAnnouncement.FromUserId);

            }

            return View(ObjAnnouncement);
        }

        // POST: /Announcement/MultiViewIndex/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult MultiViewIndex(Announcement ObjAnnouncement)
        {
            if (ModelState.IsValid)
            {


                db.Entry(ObjAnnouncement).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("MultiViewIndex", "Announcement", new { id = ObjAnnouncement.Id });
            }
            ViewBag.FromUserId = new SelectList(db.ApplicationUsers, "Id", "Username", ObjAnnouncement.FromUserId);

            return View(ObjAnnouncement);
        }
        private SIContext db = new SIContext();

        private ActionResult ModelBind()
        {
            var Announcements = db.Announcements;
            return View(Announcements.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }



    }
}