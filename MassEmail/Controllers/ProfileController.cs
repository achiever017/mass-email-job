﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MassEmail.Models;
using System.Data.Entity;

namespace MassEmail.Controllers
{
    public class ProfileController : Controller
    {
        private SIContext db = new SIContext();


        // GET: EditProfile
        public ActionResult EditProfile()
        {
            if(Session["email"]==null)
            {
                return RedirectToAction("login","Account");
            }
            string email = Session["email"].ToString();
           
            Profile model = new Profile();

            ApplicationUser ObjApplicationUser = db.ApplicationUsers.Where(x => x.Email == email).FirstOrDefault();

            model.FirstName = ObjApplicationUser.FirstName;
            model.LastName = ObjApplicationUser.LastName;
            model.Username = ObjApplicationUser.Username;
            model.id = ObjApplicationUser.Id;
            model.Password = ObjApplicationUser.Password;
            return View(model);
        }


        // POST: EditProfile
        [HttpPost]
        public ActionResult EditProfile(int?id,Profile model)
        {
                ApplicationUser ObjApplicationUser = db.ApplicationUsers.Find(model.id);
             
                ObjApplicationUser.FirstName = model.FirstName;

                ObjApplicationUser.LastName = model.LastName;
                ObjApplicationUser.Password = model.Password;
                ObjApplicationUser.Username = model.Username;

                db.Entry(ObjApplicationUser).State = EntityState.Modified;
                db.SaveChanges();
                ViewBag.Success = "Bilgileriniz başarıyla güncellendi.";
                
            return View(model);
        }
    }
}