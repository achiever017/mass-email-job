﻿using MassEmail.ChatClass;
using MassEmail.Models;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MassEmail.Controllers
{
    public class ViewDataUploadFilesResult
    {
        public string name { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public string url { get; set; }
        public string delete_url { get; set; }
        public string thumbnail_url { get; set; }
        public string delete_type { get; set; }
    }
    public class MessagesController : Controller
    {
        //
        // GET: /Messages/
        [HttpPost]
        public ActionResult FileUpload()
        {
            var r = new List<ViewDataUploadFilesResult>();
            string name = "";
            foreach (string file in Request.Files)
            {
                var statuses = new ViewDataUploadFilesResult();
                var headers = Request.Headers;

                if (string.IsNullOrEmpty(headers["X-File-Name"]))
                {
                 name=  UploadWholeFile(Request);
                }
                else
                {
                 //   UploadPartialFile(headers["X-File-Name"], Request, statuses);
                }

                JsonResult result = Json(name);
                result.ContentType = "text/plain";

                return result;
            }

            return Json(r);
        }

        private string UploadWholeFile(HttpRequestBase request)
        {
            string filename = "";
            for (int i = 0; i < request.Files.Count; i++)
            {
                var file = request.Files[i];
                Guid r = Guid.NewGuid();
               
                var fullPath = file.FileName;
                filename = r+file.FileName;
                /*Saving the file in server folder*/
                file.SaveAs(Server.MapPath("~/Uploads/" + filename));
               // file.SaveAs(fullPath);

               
            }
            return  filename;
        }
        
        
        public ActionResult To(int id = 0)
        {
            ViewBag.Message = "Aydın Büyükşehir Belediye Aykome";

            //HttpCookie userInfo = Request.Cookies["UserInfo"];
            //if (userInfo != null)
            //{

            using (var db = new SIContext())
            {
                ViewBag.username = Env.GetUserInfo("name");
                ViewBag.password = "Admin";
                ViewBag.UserId = Env.GetUserInfo("userid"); ;
                if (id > 0) { ViewBag.ToUserId = id; }

                var Listusers = db.ApplicationUsers.OrderBy(i => i.Department_DepartmentId.Title).ToArray(); 
                StringBuilder sbUsers = new StringBuilder();
               
                if (Listusers != null)
                {
                    foreach (var item in Listusers)
                    {
                        if (item.Username.Trim() != Env.GetUserInfo("name").Trim())
                            sbUsers.AppendLine("<div id='Us_" + item.Id + "' class=\"list-group\"  data-url=\"" + item.Id + "\"><span  class=\"list-group-item\">" + item.FirstName + " " + item.LastName + "<span style=\"color:red; position:relative; top:-4px; display:block;\" class=\"pull-right glyphicon glyphicon-one-fine-dot redoffline\"></span> ( " + ( item.Department_DepartmentId.Title.Length > 10 ? item.Department_DepartmentId.Title.Substring(0,10)+"..." :item.Department_DepartmentId.Title )+ " )<span  id=\"online_" + item.Id + "\" ></span></span></div>");
                    }
                }
                ViewBag.UsersList = sbUsers.ToString();

            }
            //}

            return View();
        }
        private SIContext db = new SIContext();
        public ActionResult BulkMessage(int id = 0)
        {
            LoadBinding();
            return View();
        }

        private void LoadBinding()
        {
            var UsersList = db.ApplicationUsers;
            int cUserId = Convert.ToInt32(Env.GetUserInfo("userid"));

            StringBuilder sbUser = new StringBuilder();
            sbUser.Append("["); 
            foreach (var item in UsersList.Where(i => i.Id != cUserId).ToArray())
            { 
                sbUser.Append("{\"id\":\"" + item.Email + "\", \"name\":\"" + item.FirstName+" "+item.LastName + "\"},");
                //sbUser.Append("'" + item.Email + ",',");
            }
            sbUser.Append("]");
            ViewBag.UserList = sbUser.ToString();




            StringBuilder sbDepartment = new StringBuilder();
            sbDepartment.Append("["); 
            foreach (var item in db.Departments.ToArray())
            {
                sbDepartment.Append("{\"id\":\"" + item.Title + "\", \"name\":\"" + item.Title + "\"},");
            }
            sbDepartment.Append("]");
            ViewBag.Departments = sbDepartment.ToString();


            ViewBag.FromUserId = new SelectList(UsersList, "Id", "Username", cUserId);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult BulkMessage(string chatMsg,string Subject, string sl, string toEmails, string toDepEmails)
        {
             
                LoadBinding();
                int CUserId = Convert.ToInt32(Env.GetUserInfo("userid"));
                if (sl.Length > 0)
                {
                    var AllUsers = db.ApplicationUsers.ToArray();
                    if (sl == "Users")
                    {
                        if (toEmails.Length > 0)
                        {
                            //
                            //
                            //
   
                            string[] emails = toEmails.Split(',');
                            foreach (var item in emails)
                            {
                                SendMessage(chatMsg, Subject, CUserId, AllUsers, item);
                            }
                        }
                        else
                        {
                            ViewBag.Msg = "Kullanıcı Seçiniz";
                            return View("BulkMessage");
                        }
                    }
                    else
                    {
                        if (toDepEmails.Length > 0)
                        {
                            
                            string[] departm = toDepEmails.Split(',');
                            foreach (var item in departm)
                            {
                                if (item != "")
                                {
                                    var users = db.ApplicationUsers.Where(i => i.Department_DepartmentId.Title == item.Trim()).ToArray();
                                    foreach (var user in users)
                                    {
                                        SendMessage(chatMsg, Subject, CUserId, AllUsers, user.Username);
                                    }
                                }
                            }
                        }
                        else
                        {
                            ViewBag.Msg = "Kurum Seçiniz";
                            return View("BulkMessage");
                        }
                    }
                     

                    //return Redirect(TempData["ThisUrl"].ToString());
                    return View("BulkMessage");
                }
                else
                {
                    ViewBag.Msg = "Kullanıcı veya Kurum Seçiniz.";
                    return View("BulkMessage");
                }

 

            ViewBag.Success = "Email Başarıyla Gönderildi.";

            return View("BulkMessage");
        }

        private void SendMessage(string chatMsg, string Subject, int CUserId, ApplicationUser[] AllUsers, string item)
        {
            if (item != "")
            {
                var Usr = AllUsers.FirstOrDefault(i => i.Username == item.Trim()).Id;

                int Groupid = 0;

                bool result = false;

                var QueryGetGroup = db.Database.SqlQuery<DtoGrouping>("select COUNT( ChatGroupId ) as GroupCount ,ChatGroupId from ChatGroupUser where ChatUserId in(" + CUserId + "," + Usr + ") group by ChatGroupId");

                if (QueryGetGroup != null)
                {
                    foreach (var item1 in QueryGetGroup)
                    {
                        if (item1.GroupCount > 1)
                        {
                            result = true;
                            Groupid = item1.ChatGroupId;
                        }
                    }
                }


                if (result == true)
                {
                    //
                }
                else
                {
                    //
                    string sqlCreateGroup = "declare @GroupId int=null \n ";
                    sqlCreateGroup += "insert into ChatGroup(GroupCreatorBy,IsActive,GroupName) values(" + CUserId + ",1,'" + Subject + "') set @GroupId=SCOPE_IDENTITY() \n ";
                    sqlCreateGroup += "insert into ChatGroupUser (ChatUserId,ChatGroupId) values(" + CUserId + ",@GroupId) \n ";
                    sqlCreateGroup += "insert into ChatGroupUser (ChatUserId,ChatGroupId) values(" + Usr + ",@GroupId) \n ";
                    sqlCreateGroup += "select @GroupId \n ";

                    var getgroupid = db.Database.SqlQuery<int>(sqlCreateGroup).FirstOrDefault();

                    Groupid = Convert.ToInt32(getgroupid.ToString());
                }

                 
                Guid messageId = System.Guid.NewGuid();

                ChatMessage con = new ChatMessage();
                con.Msg = chatMsg;
                con.ConnectionIDUser = messageId.ToString();
                con.SendDate = DateTime.Now;
                con.SendFrom = CUserId;
                con.ChatGroupId = Groupid;
                con.IsDeleted = false;
                //con.ImageFile=null //now not working
                con.IsSeen = false;
                db.ChatMessages.Add(con);

                db.SaveChanges();

            }
        }
        public ActionResult History()
        {
            return View();
        }

        // GET  Messages/GetGrid
        public ActionResult GetGrid()
        {
            var tak = db.ChatMessages.OrderByDescending(i=>i.Id).ToArray();

            var result = from c in tak select new string[] { c.Id.ToString()  ,     Convert.ToString(c.Msg), Convert.ToString(c.SendDate), Convert.ToString(c.ChatUser_SendFrom.Username), Convert.ToString(c.IsDeleted), Convert.ToString(c.IsSeen)};
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

         
    }
}