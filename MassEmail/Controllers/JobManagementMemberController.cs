﻿using MassEmail.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MassEmail.Controllers
{
    public class JobManagementMemberController : Controller
    {
        private SIContext db = new SIContext();
        public ActionResult Index()
        {
            int intCurrentUserId = Convert.ToInt32(Env.GetUserInfo("userid"));
            int[] intJobMemberIds = db.JobManagementMembers.Where(e => e.UserId == intCurrentUserId).Select(e => e.JobMemeberId).ToArray();
            var JobMembers = (from e in db.JobManagementMembers
                              where intJobMemberIds.Contains(e.JobMemeberId)
                                select e).ToList();
            return View(JobMembers.ToList());
        }
        // GET Announcement/GetGrid
        public ActionResult GetGrid()
        {
            int user = Convert.ToInt32(Env.GetUserInfo("userid"));

            var tak = db.JobManagementMembers.Where(i => i.UserId == user).ToArray().OrderByDescending(x => x.Obj_JobManagement.CreatedAt);

            var result = from c in tak
                         select new string[] {
                Convert.ToString(c.JobMemeberId),
                Convert.ToString("<a style='cursor:pointer !important;' onclick='expand1(" + c.JobMemeberId + ")'>"+ c.Obj_JobManagement.Title +"</a>"),
                Convert.ToString(c.Obj_JobManagement.FileUrl),
                Convert.ToString(c.IsRead), fuid(c.Obj_JobManagement.CreatedBy),
                Convert.ToString(c.Obj_JobManagement.StartTime),
                Convert.ToString(c.MemberJobStatus) };

            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        private string fuid(int? nullable)
        {
            string fu = "";
            try
            {
                var data = db.ApplicationUsers.FirstOrDefault(x => x.Id == nullable);
                fu = data.FirstName + ' ' + data.LastName;
            }
            catch (Exception ex)
            {
            }

            return fu;
        }

        // GET: /Event/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            JobManagementMember jobMember = db.JobManagementMembers.Find(id);
            if (jobMember == null)
            {
                return HttpNotFound();
            }
            jobMember.IsRead = true;
            db.Entry(jobMember).State = EntityState.Modified;
            db.SaveChanges();
            return View(jobMember);
        }

        //[HttpPost]
        //public async Task<ActionResult> JobAccepted(int Id)
        //{
        //    try
        //    {
        //        JobManagementMember myjob = db.JobManagementMembers.FirstOrDefault(e => e.JobMemeberId == Id);
        //        DateTime d = DateTime.Now;
        //        d = d.AddHours(1);

        //        TimeSpan t = myjob.Obj_JobManagement.CreatedAt- d;
        //        if (t.Minutes > 30)
        //        {
        //            Thread thread = new Thread(() => sendemail(t));
        //            thread.Start();
        //        }
        //        //  await ;

        //        myjob.MemberJobStatus = 2;
        //        db.Entry<JobManagementMember>(myjob).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return Content("Kabul");
        //    }
        //    catch (Exception)
        //    {
        //        return Content("False");
        //    }
        //}
        [HttpPost]
        public ActionResult JobAccepted(int Id)
        {
            try
            {
                JobManagementMember myjob = db.JobManagementMembers.FirstOrDefault(e => e.JobMemeberId == Id);
                DateTime d = DateTime.Now;
                d = d.AddHours(1);

                TimeSpan t = myjob.Obj_JobManagement.CreatedAt - d;
                if (t.Minutes > 30)
                {
                    Thread thread = new Thread(() => sendemail(t));
                    thread.Start();
                }
                //  await ;

                myjob.MemberJobStatus = 2;
                db.Entry<JobManagementMember>(myjob).State = EntityState.Modified;
                db.SaveChanges();
                return Content("Kabul");
            }
            catch (Exception)
            {
                return Content("False");
            }
        }

        public ActionResult GetGridAll1(AnnouncementMember d)
        {

            var data = d.Announcement_EmailMessageId;
            var tak = db.AnnouncementMembers.ToArray();

            int jobid = db.JobManagementMembers.Where(x => x.JobMemeberId == d.Id).FirstOrDefault().JobtId;
            var result = from c in db.JobManagements
                         where c.JobtId == jobid
                         select new
                         {
                             sub = c.Title,
                             body = c.Description,
                         };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }
        public void sendemail(TimeSpan a)
        {

            // System.Threading.Thread.SpinWait(1000000);
            //the above line is working as i want but it stop the whole functionalityo :( :'(
            //   await Task.Delay(a);

            Thread.Sleep(a);
            SIContext s = new SIContext();
            var gs = s.GeneralSettings.ToArray();
            SendAsync("admin@gmail.com", Env.GetUserInfo("name"), "Hatırlatma", "1 Saat Sonra Etkinliğiniz Başlayacaktır. Teşekkürler", gs, null);

        }
        public Task SendAsync(string FromUser, string ToUser, string subject, string body, GeneralSetting[] gs, System.Web.HttpPostedFileBase[] filesAttach)
        {
            var result = Task.FromResult(0);
            var toAddress = new System.Net.Mail.MailAddress(ToUser, ToUser);
            try
            {
                System.Net.Mail.MailMessage emessage = new System.Net.Mail.MailMessage();
                
                string Username = gs.FirstOrDefault(i => i.SettingKey == "Email").SettingValue;
                string Password = gs.FirstOrDefault(i => i.SettingKey == "EmailPassword").SettingValue;
                string port = gs.FirstOrDefault(i => i.SettingKey == "EmailPort").SettingValue;
                string SmptServer = gs.FirstOrDefault(i => i.SettingKey == "EmailServer").SettingValue;

                emessage.To.Add(toAddress);
                emessage.Subject = subject;
                emessage.From = new System.Net.Mail.MailAddress(FromUser);
                emessage.Body = body;
                emessage.IsBodyHtml = true;
                System.Net.Mail.SmtpClient sc = new System.Net.Mail.SmtpClient();
                var netCredential = new System.Net.NetworkCredential(Username, Password);
                sc.Host = SmptServer;
                sc.DeliveryMethod = SmtpDeliveryMethod.Network;
                sc.UseDefaultCredentials = false;
                sc.Credentials = netCredential;
                sc.Port = Convert.ToInt32(port);
                sc.Send(emessage);
                result = Task.FromResult(1);
            }
            catch (Exception)
            {
                result = Task.FromResult(0);
            }
            return result;
        }
        [HttpPost]
        public ActionResult JobRejected(int Id, string Reason)
        {
            try
            {
                JobManagementMember myJob = db.JobManagementMembers.FirstOrDefault(e => e.JobMemeberId == Id);
                myJob.MemberJobStatus = 3;
                myJob.RejectReason = Reason; //Save submitted rejection here
                db.Entry<JobManagementMember>(myJob).State = EntityState.Modified;
                db.SaveChanges();
                // return Redirect("EventMember");
                return Content("Red");

            }
            catch (Exception)
            {
                return Content("False");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}