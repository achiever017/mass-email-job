using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using MassEmail.Models;
using MassEmail.ChatClass;

namespace MassEmail.Controllers
{
    public class ChatGroupController : Controller
    { 
        // GET: /ChatGroup/
        public ActionResult Index()
        {
            return ModelBind();
        }
        
        // GET ChatGroup/GetGrid
        public ActionResult GetGrid()
        {
            var tak = db.ChatGroups.ToArray();
             
            var result = from c in tak select new string[] { c.Id.ToString(), Convert.ToString(c.Id),Convert.ToString(c.GroupCreatorBy),Convert.ToString(c.IsActive),Convert.ToString(c.GroupName), };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }
        // GET: /ChatGroup/ModelBindIndex
        public ActionResult ModelBindIndex()
        {
            return ModelBind();
        }
        // GET: /ChatGroup/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChatGroup ObjChatGroup = db.ChatGroups.Find(id);
            if (ObjChatGroup == null)
            {
                return HttpNotFound();
            }
            return View(ObjChatGroup);
        }
        // GET: /ChatGroup/Create
        public ActionResult Create()
        {
             ViewBag.GroupCreatorBy = new SelectList(db.ApplicationUsers, "Id", "Username");

             return View();
        }

        // POST: /ChatGroup/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(ChatGroup ObjChatGroup )
        {
            if (ModelState.IsValid)
            {


                db.ChatGroups.Add(ObjChatGroup);
                db.SaveChanges();
                return Redirect(TempData["ThisUrl"].ToString());
            }
             ViewBag.GroupCreatorBy = new SelectList(db.ApplicationUsers, "Id", "Username", ObjChatGroup.GroupCreatorBy);

             return View(ObjChatGroup);
        }
        // GET: /ChatGroup/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChatGroup ObjChatGroup = db.ChatGroups.Find(id);
            if (ObjChatGroup == null)
            {
                return HttpNotFound();
            }
            ViewBag.GroupCreatorBy = new SelectList(db.ApplicationUsers, "Id", "Username", ObjChatGroup.GroupCreatorBy);

            return View(ObjChatGroup);
        }

        // POST: /ChatGroup/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(ChatGroup ObjChatGroup )
        {
            if (ModelState.IsValid)
            {
                

                db.Entry(ObjChatGroup).State = EntityState.Modified;
                db.SaveChanges();
                return Redirect(TempData["ThisUrl"].ToString());
            }
            ViewBag.GroupCreatorBy = new SelectList(db.ApplicationUsers, "Id", "Username", ObjChatGroup.GroupCreatorBy);

            return View(ObjChatGroup);
        }
        // GET: /ChatGroup/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChatGroup ObjChatGroup = db.ChatGroups.Find(id);
            if (ObjChatGroup == null)
            {
                return HttpNotFound();
            }
            return View(ObjChatGroup);
        }

        // POST: /ChatGroup/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ChatGroup ObjChatGroup = db.ChatGroups.Find(id);
            db.ChatGroups.Remove(ObjChatGroup);
            db.SaveChanges();
            return Redirect(TempData["ThisUrl"].ToString());
        }
        // GET: /ChatGroup/MultiViewIndex/5
        public ActionResult MultiViewIndex(int? id)
        { 
            ChatGroup ObjChatGroup = db.ChatGroups.Find(id);
            ViewBag.IsWorking = 0;
            if (id > 0)
            {
                ViewBag.IsWorking = 1;
                ViewBag.GroupCreatorBy = new SelectList(db.ApplicationUsers, "Id", "Username", ObjChatGroup.GroupCreatorBy);

            }
            
            return View(ObjChatGroup);
        }

        // POST: /ChatGroup/MultiViewIndex/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult MultiViewIndex(ChatGroup ObjChatGroup )
        {
            if (ModelState.IsValid)
            {
                

                db.Entry(ObjChatGroup).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("MultiViewIndex", "ChatGroup", new { id = ObjChatGroup.Id });
            }
            ViewBag.GroupCreatorBy = new SelectList(db.ApplicationUsers, "Id", "Username", ObjChatGroup.GroupCreatorBy);

            return View(ObjChatGroup);
        }
        private SIContext db = new SIContext();
		
		private ActionResult ModelBind()
        {
            var ChatGroups = db.ChatGroups;
            return View(ChatGroups.ToList());
        }
		
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        
		 
    }
}

