using MassEmail.ChatClass;
using MassEmail.Models;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MassEmail.Controllers
{
    public class HomeController : Controller
    {
      
        public ActionResult Index()
        {

            if (Session["email"] != null && Session["password"] != null)
            {
                var email = Session["email"].ToString();
                var password = Session["password"].ToString();
                ViewBag.IframeSrc = "http://aykome.aydin.bel.tr:82/Account/login?email=" + email + "&pass=" + password;
                //  ViewData["IframeSrc"] = u.Replace("amp;", "");
                // Response.Write("<script>alert(" + ViewData["IframeSrc"] + ") </script>");
                // string EncodedUrl = Server.HtmlEncode(Url.Action("http://aykome.aydin.bel.tr:84/Account/login", "", new { email = email, pass = password }));
                // ViewData["IframeSrc"] = EncodedUrl;
                ViewBag.username = email;
                ViewBag.password = password;
            }
            else if (Session["email"] != null && Session["password"] == null)
            {
                return RedirectToAction("lockuser", "Account");
            }
            else if (Session["email"] == null && Session["password"] == null)
            {
                return RedirectToAction("login", "Account");
            }

            ViewBag.TotalDepartments = db.Departments.Count();
            ViewBag.TotalUsers = db.ApplicationUsers.Count();

            var d = db.ApplicationUsers.GroupBy(i => i.DepartmentId).ToArray();
            StringBuilder sb = new StringBuilder();
            foreach (var item in d)
            {
                sb.Append(item.FirstOrDefault().Department_DepartmentId.Title+" (" + item.Count()+")  ");
            }
            ViewBag.TotalUsersPerDepartment = sb.ToString();
            ViewBag.LatestRegisteredUser = 3;
            ViewBag.LatestMessages = db.ChatMessages.Count();
            ViewBag.LatestEmails = db.EmailMessages.Count();
            ViewBag.UserRoles = db.Roles.Count();

            int user=Convert.ToInt32(Env.GetUserInfo("userid"));
            var ann= db.AnnouncementMembers.Where(i => i.IsRead == false && i.ToUserId == user).ToList().LastOrDefault();
           // ViewBag.Announce = "&nbsp;";
            if(ann !=null)
            {
                ViewBag.AnnounceId = ann.Id;
                ViewBag.Announce =  ann.Announcement_EmailMessageId.Subject;
                var anno = db.Announcements.FirstOrDefault(x => x.Id == ann.Announcement_EmailMessageId.Id);
              
                    ViewBag.File = anno.FileUrl;
                    var NA = anno.FileUrl;
              
            }

            ViewBag.TotalAnnouncements = db.AnnouncementMembers.Count(a => a.ToUserId == user&&a.IsRead==false);
            ViewBag.TotalEvents = db.EventMembers.Count(e => e.ReceiverId == user&&e.IsRead==false);

            int intNewEventCount=db.EventMembers.Where(e => e.ReceiverId == user && e.IsRead == false).Count();

            ViewBag.NewEventCount = intNewEventCount;

            if (intNewEventCount > 0)
            {
                var evm = db.EventMembers.Where(e => e.ReceiverId == user && e.IsRead == false).ToList().LastOrDefault();
                ViewBag.NewEventSubject = evm.Obj_Event.Subject;
                ViewBag.NewEventMemberID = evm.EventMemberId;
                ViewBag.NewEventFile = evm.Obj_Event.FileUrl;
            }

            return View();
        }
        private SIContext db = new SIContext();
        public ActionResult About()
        {
            ViewBag.Message = "Bal�kesir B�y�k�ehir Belediye Aykome";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Bal�kesir B�y�k�ehir Belediye Aykome";

            return View();
        }


        public string  getonline() {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<MyHub>();
            hubContext.Clients.All.getonline("#online_" + Env.GetUserInfo("userid"));
            return "Yes";
        }

        public string getoffline()
        {
            try
            {
                var hubContext = GlobalHost.ConnectionManager.GetHubContext<MyHub>();
                string Id = "#online_" + Env.GetUserInfo("userid");

                if (ChatClass.MyHub.UsersList.Count > 0)
                {
                    var itemtoremove = ChatClass.MyHub.UsersList.Single(r => r.FromId == Convert.ToInt32(Env.GetUserInfo("userid")));
                    ChatClass.MyHub.UsersList.Remove(itemtoremove);
                }
                hubContext.Clients.All.getoffline("#online_" + Env.GetUserInfo("userid"));
                return "Yes";
            }
            catch(Exception ex)
            {
                return ex.Message.ToString();
            }
        }

    }
}
