using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using MassEmail.Models;
using MassEmail.ChatClass;

namespace MassEmail.Controllers
{
    public class ChatMessageController : Controller
    { 
        // GET: /ChatMessage/
        public ActionResult Index()
        {
            return ModelBind();
        }
        
        // GET ChatMessage/GetGrid
        public ActionResult GetGrid()
        {
            var tak = db.ChatMessages.OrderByDescending(x => x.SendDate).ToArray();
             
            var result = from c in tak select new string[] { c.Id.ToString(), Convert.ToString(c.Id),Convert.ToString(c.ConnectionIDUser),Convert.ToString(c.ChatGroup_ChatGroupId !=null?c.ChatGroup_ChatGroupId.GroupName:""),Convert.ToString(c.Msg),Convert.ToString(c.SendDate),Convert.ToString(c.ChatUser_SendFrom.Username),Convert.ToString(c.IsDeleted),Convert.ToString(c.ImageFile),Convert.ToString(c.IsSeen), };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }
        // GET: /ChatMessage/ModelBindIndex
        public ActionResult ModelBindIndex()
        {
            return ModelBind();
        }
        // GET: /ChatMessage/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChatMessage ObjChatMessage = db.ChatMessages.Find(id);
            if (ObjChatMessage == null)
            {
                return HttpNotFound();
            }
            return View(ObjChatMessage);
        }
        // GET: /ChatMessage/Create
        public ActionResult Create()
        {
             ViewBag.ChatGroupId = new SelectList(db.ChatGroups, "Id", "GroupName");
             ViewBag.SendFrom = new SelectList(db.ApplicationUsers, "Id", "Username");

             return View();
        }

        // POST: /ChatMessage/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(ChatMessage ObjChatMessage )
        {
            if (ModelState.IsValid)
            {


                db.ChatMessages.Add(ObjChatMessage);
                db.SaveChanges();
                return Redirect(TempData["ThisUrl"].ToString());
            }
             ViewBag.ChatGroupId = new SelectList(db.ChatGroups, "Id", "GroupName", ObjChatMessage.ChatGroupId);
             ViewBag.SendFrom = new SelectList(db.ApplicationUsers, "Id", "Username", ObjChatMessage.SendFrom);

             return View(ObjChatMessage);
        }
        // GET: /ChatMessage/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChatMessage ObjChatMessage = db.ChatMessages.Find(id);
            if (ObjChatMessage == null)
            {
                return HttpNotFound();
            }
            ViewBag.ChatGroupId = new SelectList(db.ChatGroups, "Id", "GroupName", ObjChatMessage.ChatGroupId);
            ViewBag.SendFrom = new SelectList(db.ApplicationUsers, "Id", "Username", ObjChatMessage.SendFrom);

            return View(ObjChatMessage);
        }

        // POST: /ChatMessage/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(ChatMessage ObjChatMessage )
        {
            if (ModelState.IsValid)
            {
                

                db.Entry(ObjChatMessage).State = EntityState.Modified;
                db.SaveChanges();
                return Redirect(TempData["ThisUrl"].ToString());
            }
            ViewBag.ChatGroupId = new SelectList(db.ChatGroups, "Id", "GroupName", ObjChatMessage.ChatGroupId);
            ViewBag.SendFrom = new SelectList(db.ApplicationUsers, "Id", "Username", ObjChatMessage.SendFrom);

            return View(ObjChatMessage);
        }
        // GET: /ChatMessage/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChatMessage ObjChatMessage = db.ChatMessages.Find(id);
            if (ObjChatMessage == null)
            {
                return HttpNotFound();
            }
            return View(ObjChatMessage);
        }

        // POST: /ChatMessage/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ChatMessage ObjChatMessage = db.ChatMessages.Find(id);
            //db.ChatMessages.Remove(ObjChatMessage);
            ObjChatMessage.IsDeleted = true;
            db.Entry(ObjChatMessage).State = EntityState.Modified;
            db.SaveChanges(); 
            return Redirect(TempData["ThisUrl"].ToString());
        }

        [HttpDelete]
        public JsonResult del(int id)
        {
            ChatMessage ObjChatMessage = db.ChatMessages.Find(id);
             
            ObjChatMessage.IsDeleted = true;
            db.Entry(ObjChatMessage).State = EntityState.Modified;
            db.SaveChanges();
            return Json("Deleted", JsonRequestBehavior.AllowGet);
        }


        // GET: /ChatMessage/MultiViewIndex/5
        public ActionResult MultiViewIndex(int? id)
        { 
            ChatMessage ObjChatMessage = db.ChatMessages.Find(id);
            ViewBag.IsWorking = 0;
            if (id > 0)
            {
                ViewBag.IsWorking = 1;
                ViewBag.ChatGroupId = new SelectList(db.ChatGroups, "Id", "GroupName", ObjChatMessage.ChatGroupId);
                ViewBag.SendFrom = new SelectList(db.ApplicationUsers, "Id", "Username", ObjChatMessage.SendFrom);

            }
            
            return View(ObjChatMessage);
        }

        // POST: /ChatMessage/MultiViewIndex/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult MultiViewIndex(ChatMessage ObjChatMessage )
        {
            if (ModelState.IsValid)
            {
                

                db.Entry(ObjChatMessage).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("MultiViewIndex", "ChatMessage", new { id = ObjChatMessage.Id });
            }
            ViewBag.ChatGroupId = new SelectList(db.ChatGroups, "Id", "GroupName", ObjChatMessage.ChatGroupId);
            ViewBag.SendFrom = new SelectList(db.ApplicationUsers, "Id", "Username", ObjChatMessage.SendFrom);

            return View(ObjChatMessage);
        }
        private SIContext db = new SIContext();
		
		private ActionResult ModelBind()
        {
            var ChatMessages = db.ChatMessages;
            return View(ChatMessages.ToList());
        }
		
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        
		 
    }
}

