using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using MassEmail.Models;

namespace MassEmail.Controllers
{
    public class EmailAttachmentController : Controller
    { 
        // GET: /EmailAttachment/
        public ActionResult Index()
        {
            return ModelBind();
        }
        
        // GET EmailAttachment/GetGrid
        public ActionResult GetGrid()
        {
            var tak = db.EmailAttachments.ToArray();
             
            var result = from c in tak select new string[] { c.Id.ToString(), Convert.ToString(c.Id),Convert.ToString(c.EmailMessage_EmailMessageId.Subject),Convert.ToString(c.FileUrl),Convert.ToString(c.IsDeleted), };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }
        // GET: /EmailAttachment/ModelBindIndex
        public ActionResult ModelBindIndex()
        {
            return ModelBind();
        }
        // GET: /EmailAttachment/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailAttachment ObjEmailAttachment = db.EmailAttachments.Find(id);
            if (ObjEmailAttachment == null)
            {
                return HttpNotFound();
            }
            return View(ObjEmailAttachment);
        }
        // GET: /EmailAttachment/Create
        public ActionResult Create()
        {
             ViewBag.EmailMessageId = new SelectList(db.EmailMessages, "Id", "Subject");

             return View();
        }

        // POST: /EmailAttachment/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(EmailAttachment ObjEmailAttachment ,HttpPostedFileBase FileUrl,string HideImage1)
        {
            if (ModelState.IsValid)
            {
                 if (FileUrl != null) 
                 {
                    var GetExt = Path.GetExtension(FileUrl.FileName); 
                    string filenameOutExt = Path.GetFileNameWithoutExtension(FileUrl.FileName); 
                    var fileName = Path.GetFileNameWithoutExtension(FileUrl.FileName) + DateTime.Now.ToString("ddMMyyyyhhmmss") + GetExt; 
                    FileUrl.SaveAs(Path.Combine(Server.MapPath("~/Uploads"), fileName)); 
                    ModelState.Clear(); 
                    ObjEmailAttachment.FileUrl = fileName; 
                } 
                else 
                { 
                    ObjEmailAttachment.FileUrl = HideImage1;  
                }


                db.EmailAttachments.Add(ObjEmailAttachment);
                db.SaveChanges();
                return Redirect(TempData["ThisUrl"].ToString());
            }
             ViewBag.EmailMessageId = new SelectList(db.EmailMessages, "Id", "Subject", ObjEmailAttachment.EmailMessageId);

             return View(ObjEmailAttachment);
        }
        // GET: /EmailAttachment/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailAttachment ObjEmailAttachment = db.EmailAttachments.Find(id);
            if (ObjEmailAttachment == null)
            {
                return HttpNotFound();
            }
            ViewBag.EmailMessageId = new SelectList(db.EmailMessages, "Id", "Subject", ObjEmailAttachment.EmailMessageId);

            return View(ObjEmailAttachment);
        }

        // POST: /EmailAttachment/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(EmailAttachment ObjEmailAttachment ,HttpPostedFileBase FileUrl,string HideImage1)
        {
            if (ModelState.IsValid)
            {
                                 if (FileUrl != null) 
                 {
                    var GetExt = Path.GetExtension(FileUrl.FileName); 
                    string filenameOutExt = Path.GetFileNameWithoutExtension(FileUrl.FileName); 
                    var fileName = Path.GetFileNameWithoutExtension(FileUrl.FileName) + DateTime.Now.ToString("ddMMyyyyhhmmss") + GetExt; 
                    FileUrl.SaveAs(Path.Combine(Server.MapPath("~/Uploads"), fileName)); 
                    ModelState.Clear(); 
                    ObjEmailAttachment.FileUrl = fileName; 
                } 
                else 
                { 
                    ObjEmailAttachment.FileUrl = HideImage1;  
                }


                db.Entry(ObjEmailAttachment).State = EntityState.Modified;
                db.SaveChanges();
                return Redirect(TempData["ThisUrl"].ToString());
            }
            ViewBag.EmailMessageId = new SelectList(db.EmailMessages, "Id", "Subject", ObjEmailAttachment.EmailMessageId);

            return View(ObjEmailAttachment);
        }
        // GET: /EmailAttachment/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailAttachment ObjEmailAttachment = db.EmailAttachments.Find(id);
            if (ObjEmailAttachment == null)
            {
                return HttpNotFound();
            }
            return View(ObjEmailAttachment);
        }

        // POST: /EmailAttachment/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EmailAttachment ObjEmailAttachment = db.EmailAttachments.Find(id);
            db.EmailAttachments.Remove(ObjEmailAttachment);
            db.SaveChanges();
            return Redirect(TempData["ThisUrl"].ToString());
        }
        // GET: /EmailAttachment/MultiViewIndex/5
        public ActionResult MultiViewIndex(int? id)
        { 
            EmailAttachment ObjEmailAttachment = db.EmailAttachments.Find(id);
            ViewBag.IsWorking = 0;
            if (id > 0)
            {
                ViewBag.IsWorking = 1;
                ViewBag.EmailMessageId = new SelectList(db.EmailMessages, "Id", "Subject", ObjEmailAttachment.EmailMessageId);

            }
            
            return View(ObjEmailAttachment);
        }

        // POST: /EmailAttachment/MultiViewIndex/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult MultiViewIndex(EmailAttachment ObjEmailAttachment ,HttpPostedFileBase FileUrl,string HideImage1)
        {
            if (ModelState.IsValid)
            {
                                 if (FileUrl != null) 
                 {
                    var GetExt = Path.GetExtension(FileUrl.FileName); 
                    string filenameOutExt = Path.GetFileNameWithoutExtension(FileUrl.FileName); 
                    var fileName = Path.GetFileNameWithoutExtension(FileUrl.FileName) + DateTime.Now.ToString("ddMMyyyyhhmmss") + GetExt; 
                    FileUrl.SaveAs(Path.Combine(Server.MapPath("~/Uploads"), fileName)); 
                    ModelState.Clear(); 
                    ObjEmailAttachment.FileUrl = fileName; 
                } 
                else 
                { 
                    ObjEmailAttachment.FileUrl = HideImage1;  
                }


                db.Entry(ObjEmailAttachment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("MultiViewIndex", "EmailAttachment", new { id = ObjEmailAttachment.Id });
            }
            ViewBag.EmailMessageId = new SelectList(db.EmailMessages, "Id", "Subject", ObjEmailAttachment.EmailMessageId);

            return View(ObjEmailAttachment);
        }
        private SIContext db = new SIContext();
		
		private ActionResult ModelBind()
        {
            var EmailAttachments = db.EmailAttachments;
            return View(EmailAttachments.ToList());
        }
		
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        
		 
    }
}

