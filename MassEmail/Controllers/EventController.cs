﻿using MassEmail.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using MassEmail.ChatClass;
using MassEmail.Helper;
using MassEmail.Controllers.Filters;

namespace MassEmail.Controllers
{
    public class EventController : Controller
    {
          int count=0;
        SIContext db = new SIContext();
        [AuthorizationAndRoleFilter(accessRole=AccessRole.Event)]
        public ActionResult Index()
        {
            return ModelBind();
        }

        public ActionResult MultiViewIndex(int Id)
        {
            var EventDetail = db.Events.FirstOrDefault(e => e.EventId == Id);
            string[] EventMembers = db.EventMembers.Where(e => e.EventId == Id).Select(e => e.ReceiverUser.Email).ToArray();
            ViewBag.Receivers = EventMembers;

            ViewBag.EventDetail = EventDetail; //new { Subject = EventDetail.Subject, Sender = EventDetail.ApplicationUser.Email, Body = EventDetail.Body, SendDate = EventDetail.Body  };
            return View(ViewBag);
        }
        public ActionResult MultiViewIndex1(int Id)
        {
            var EventDetail = db.Events.FirstOrDefault(e => e.EventId == Id);
            string[] EventMembers = db.EventMembers.Where(e => e.EventId == Id).Select(e => e.ReceiverUser.Email).ToArray();
            ViewBag.Receivers = EventMembers;

            var a = EventDetail.Body;
            var b = EventDetail.Subject;
            return Json(new { aaData = a,body=b }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DetailViewIndex(int Id)
        {
            
            var EventDetail = db.Events.FirstOrDefault(e => e.EventId == Id);
            
          
           var Result = sfunc(EventDetail);

            return Json(new { aaData = Result }, JsonRequestBehavior.AllowGet);
        }
        private ActionResult ModelBind()
        {
            var Events = db.Events;
            return View(Events.ToList());
        }

        public ActionResult GetGrid()
        {
            var tak = db.Events.ToArray().OrderByDescending(x => x.SendDate);
            var users = db.ApplicationUsers.ToArray();
            var us = db.EventMembers.ToArray();

            var result = from c in tak
                         select new string[] 
                                { Convert.ToString(c.EventId), 
                                    Convert.ToString("<a style='cursor:pointer !important;' onclick='expand1(" + c.EventId + ")'>" + c.Subject + "</a>"),
                                    //Convert.ToString(c.Subject),
                                    Convert.ToString(c.SendDate),
                                    Convert.ToString(c.CreateDate),
                                    Convert.ToString(c.FileUrl),
                                    c.ApplicationUser.FirstName + ' ' + c.ApplicationUser.LastName,
                                    newsfunc(c) };
            return Json(new { aaData = result = result.Skip(Math.Max(0, result.Count() - 30)).ToList() }, JsonRequestBehavior.AllowGet);

        }
        private string newsfunc(Event e)
        {
            count++;
            string tu = "";
            string su = "";
            var data = db.EventMembers.Where(m => m.EventId == e.EventId);
           // tu += "<a href='/Event/DetailViewIndex/"+ e.EventId +"' data-ajax-update=\"#SkDelete\" data-ajax-success=\"openModalDialog('SkDelete', 'Etkinlik Detay')\" data-ajax-mode=\"replace\" data-ajax-method=\"GET\" data-ajax-failure=\"clearModalDialog('SkDelete');alert('Ajax call failed')\" data-ajax-begin=\"prepareModalDialog('SkDelete')\" data-ajax=\"true\">" + data.First().ReceiverUser.FirstName + " " + data.First().ReceiverUser.LastName + "</a>";

          
            foreach (var item in data)
            {

                tu += item.ReceiverUser.FirstName + ' ' + item.ReceiverUser.LastName + (item.EventStatus == 1 ? " (Beklemede)" :
                    //tu += item.ReceiverUser.Email + (item.EventStatus == 1 ? " (Beklemede)" : 

                    (item.EventStatus == 2 ? " (Onaylandı)" :
                    (!String.IsNullOrEmpty(item.RejectReason)) ? " (Rededildi)" + " - " + item.RejectReason : " (Rededildi)"));
                tu += " ,  ";
            }
            //tu = tu.TrimEnd(' ');
            //tu = tu.TrimEnd(',');
            su += "<a style='cursor:pointer !important;' onclick='expand(" + e.EventId + ")'>" + "<i class='fa fa-check-square-o f18'>" + "</a>";

            return su;
        }

        private string sfunc(Event e)
        {
            count++;
            string tu = "";
            var data = db.EventMembers.Where(m => m.EventId == e.EventId).ToList();
            int i = 1;
            foreach (var item in data)
            {
                tu += i + ". ";
                tu += item.ReceiverUser.FirstName + ' ' + item.ReceiverUser.LastName + (item.EventStatus == 1 ? " (Beklemede)" :
                    //tu += item.ReceiverUser.Email + (item.EventStatus == 1 ? " (Beklemede)" : 

                    (item.EventStatus == 2 ? " (Onaylandı)" :
                    (!String.IsNullOrEmpty(item.RejectReason)) ? " (Rededildi)" + " - " + item.RejectReason : " (Rededildi)"));
                tu += " </br> ";
                i++;
            }
            tu = tu.TrimEnd(' ');
            tu = tu.TrimEnd(',');
            return tu;
        }

        [HttpGet]
        [AuthorizationAndRoleFilter(accessRole = AccessRole.Event)]
        public ActionResult Create()
        {
            LoadBinding();

            return View();
        }
        private string notify(int id)
        {
            StringBuilder sb = new StringBuilder();
            //   int user = Convert.ToInt32(Env.GetUserInfo("userid"));

            var events = db.EventMembers.Where(e => e.ReceiverId == id && e.IsRead == false).ToArray();

            int intCount = events.Count();

            sb.Append("<a href=\"#\" data-toggle=\"dropdown\" data-hover=\"dropdown\" data-close-others=\"true\"><i class=\"fa fa-calendar\"></i>");
            if(intCount > 0)
                sb.Append("<span class=\"badge badge-danger badge-header\">" + intCount + "</span></a>");
            else
                sb.Append("<span class=\"badge badge-danger badge-header\">" + intCount + "</span></a>");

            sb.Append("<li class=\"dropdown-header clearfix\"><p class=\"pull-left\">Etkinlikler</p></li><li class=\"dropdown-body\"><ul style=\"margin-top:-30px\" class=\"dropdown-menu-list withScroll\" data-height=\"220\">");

            foreach (var item in events)
            {
                sb.Append("<li> <a href=\"/EventMember/Details/" + item.EventMemberId + "\"> <h4><i class=\"fa fa-calendar-o\"></i> " + item.Obj_Event.Subject + "</h4></a> </li> <li class=\"divider\"></li>");
            }

            sb.Append("</ul></li> </li>  <li class=\"footer\"><a href=\"/EventMember/Index\">Tüm Etkinlikleri Görüntüle</a></li>  </ul>");


            return sb.ToString();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        [AuthorizationAndRoleFilter(accessRole = AccessRole.Event)]
        public ActionResult Create(Event newevent, string sl, string toEmails, string toDepEmails, string toFavoriteEmails, HttpPostedFileBase filesAttach)
        {

            LoadBinding();
            if (ModelState.IsValid)
            {
                var gs = db.GeneralSettings.ToArray();
                int[] strUserIds = null;
                int cUserId = Convert.ToInt32(Env.GetUserInfo("userid"));
                string filename = "";
                if (filesAttach != null)
                {
                    filename = System.IO.Path.GetFileName(filesAttach.FileName);
                    filesAttach.SaveAs(Server.MapPath("~/Uploads/" + filename));
                }

                if (!string.IsNullOrEmpty(toEmails))
                {
                    String[] users = toEmails.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    List<Int32> userIds = new List<int>();
                    if (users.Length <= 0)
                    {
                        ViewBag.Msg = "Alıcı Seçiniz";
                        return View("Create");
                    }
                    users.ToList().ForEach(x => userIds.Add(Convert.ToInt32(x)));
                    strUserIds = userIds.ToArray();
                }
                if (!string.IsNullOrEmpty(toDepEmails))
                {
                    String[] Depts = toDepEmails.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    List<Int32> DeptIDS = new List<int>();
                    if (Depts.Length <= 0)
                    {
                        ViewBag.Msg = "Alıcı Seçiniz";
                        return View("Create");
                    }
                    Depts.ToList().ForEach(x => DeptIDS.Add(Convert.ToInt32(x)));
                    if (strUserIds != null)
                    {
                        var depUserIds = db.ApplicationUsers.Where(x => DeptIDS.Contains(x.Department_DepartmentId.Id)).Select(x => x.Id).ToArray();
                        strUserIds = strUserIds.Concat(depUserIds).ToArray();
                    }
                    else
                        strUserIds = db.ApplicationUsers.Where(x => DeptIDS.Contains(x.Department_DepartmentId.Id)).Select(x => x.Id).ToArray();

                }
                
                if (!string.IsNullOrEmpty(toFavoriteEmails))
                {
                    String[] Favorites = toFavoriteEmails.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    List<Int32> FavaritesId = new List<int>();
                    if (Favorites.Length <= 0)
                    {
                        ViewBag.Msg = "Alıcı Seçiniz";
                        return View("Create");
                    }
                    Favorites.ToList().ForEach(x => FavaritesId.Add(Convert.ToInt32(x)));
                    if (strUserIds != null)
                    {
                        var favoritegroups = db.FavoriteGroups.Where(x => FavaritesId.Contains(x.Id)).ToArray();
                        var favUsersId = getIdUsersFromFavoriteGroups(favoritegroups); 
                        strUserIds = strUserIds.Concat(favUsersId).ToArray();
                    }
                    else
                    {
                        var favoritegroups = db.FavoriteGroups.Where(x => FavaritesId.Contains(x.Id)).ToArray();
                        strUserIds = getIdUsersFromFavoriteGroups(favoritegroups);
                    }

                }

                if (strUserIds != null && strUserIds.Count() > 0)
                {
                    strUserIds = strUserIds.Distinct().ToArray();
                    if (DateTime.Now.AddHours(24) > newevent.SendDate) // 
                    {
                        ViewBag.Msg = "24 Saat Sonrası için Etkinlik Oluşturabilirsiniz.";
                        return View("Create");
                    }
                    //DateTime minEventTime = newevent.SendDate.AddHours(-24); // 
                    //DateTime maxEventTime = newevent.SendDate.AddHours(24); // 

                    //int intersectCount = db.Events.Where(w => w.SendDate >= minEventTime && w.SendDate <= maxEventTime && w.SenderId == cUserId).Count();

                    //if (intersectCount > 0)
                    //{
                    //    ViewBag.Msg = "24 Saat Sonrası için Etkinlik Oluşturabilirsiniz.";
                    //    return View("Create");
                    //}


                    Event mynewEvent = new Event();
                    mynewEvent.SendDate = newevent.SendDate;
                    mynewEvent.FileUrl = filename;
                    mynewEvent.SenderId = cUserId;
                    mynewEvent.Body = newevent.Body;
                    mynewEvent.Subject = newevent.Subject;
                    mynewEvent.CreateDate = DateTime.Now;
                    db.Events.Add(mynewEvent);



                    foreach (int item in strUserIds)
                    {
                        EventMember member = new EventMember();
                        member.Obj_Event = mynewEvent;
                        member.EventStatus = 1;
                        member.IsRead = false;
                        member.ReceiverId = item;
                        var user = db.ApplicationUsers.Where(i => i.Id == item).FirstOrDefault();
                        db.EventMembers.Add(member);
                        db.SaveChanges();

                        string s = notify(user.Id);
                        Help obj = new Help();
                        string de = obj.devent(user.Id);
                        var hubContext1 = GlobalHost.ConnectionManager.GetHubContext<MyHub>();

                        hubContext1.Clients.User(user.Username).testingmu(s, de);

                        //PushNotification.SendNotification(user.deviceID, "Yeni Bir Etkinliğiniz Var:" + Env.GetUserInfo("name") + " - " + newevent.Subject);
                        SendAsync(Env.GetUserInfo("name"), user.Username, mynewEvent.SendDate + "  " + newevent.Subject , newevent.Body + " <a href='" + Request.Url.Scheme + Uri.SchemeDelimiter + Request.Url.Authority + "/EventMember/Details/" + member.EventMemberId.ToString() + "'>Detaylı Bilgi</a> için Tıklayınız.", gs, filesAttach);
                       
                    }

                    db.SaveChanges();
                    ViewBag.Success = "";
                    return View("Create");
                }
                else
                {
                    ViewBag.Msg = "Alıcı Boş Geçilemez.";
                    return View("Create");
                }
            }
            else
            {
                return View(newevent);
            }
        }

        private int[] getIdUsersFromFavoriteGroups(ICollection<FavoriteGroup> favoritegroups)
        {
            IList<int> usersId = new List<int>();
            foreach (var group in favoritegroups)
            {
                foreach (var user in group.Users)
                    usersId.Add(user.Id);
            }
            return usersId.ToArray();
        }

        public Task SendAsync(string FromUser, string ToUser, string subject, string body, GeneralSetting[] gs, System.Web.HttpPostedFileBase filesAttach)
        {
            var result = Task.FromResult(0);
            var toAddress = new System.Net.Mail.MailAddress(ToUser, ToUser);

            try
            {
                System.Net.Mail.MailMessage emessage = new System.Net.Mail.MailMessage();
                if (filesAttach != null)
                {
                    System.Net.Mail.Attachment at = new System.Net.Mail.Attachment(filesAttach.InputStream, filesAttach.FileName);
                    emessage.Attachments.Add(at);
                }

                string Username = gs.FirstOrDefault(i => i.SettingKey == "Email").SettingValue;
                string Password = gs.FirstOrDefault(i => i.SettingKey == "EmailPassword").SettingValue;
                string port = gs.FirstOrDefault(i => i.SettingKey == "EmailPort").SettingValue;
                string SmptServer = gs.FirstOrDefault(i => i.SettingKey == "EmailServer").SettingValue;

                emessage.To.Add(toAddress);
                emessage.Subject = subject;
                emessage.From = new System.Net.Mail.MailAddress(FromUser);
                emessage.Body = body;
                emessage.IsBodyHtml = true;
                System.Net.Mail.SmtpClient sc = new System.Net.Mail.SmtpClient();
                var netCredential = new System.Net.NetworkCredential(Username, Password);
                sc.Host = SmptServer;
                sc.DeliveryMethod = SmtpDeliveryMethod.Network;
                sc.UseDefaultCredentials = false;
                sc.Credentials = netCredential;
                sc.Port = Convert.ToInt32(port);
                sc.Send(emessage);



                result = Task.FromResult(1);
            }
            catch (Exception)
            {
                result = Task.FromResult(0);
            }


            return result;


        }

        private void LoadBinding()
        {
            var UsersList = db.ApplicationUsers.OrderBy(i => i.Department_DepartmentId.Title).ToArray(); 
            int cUserId = Convert.ToInt32(Env.GetUserInfo("userid"));

            StringBuilder sbUser = new StringBuilder();
            sbUser.Append("[");
            
            foreach (var item in UsersList.Where(i => i.Id != cUserId).ToArray())
            {
                sbUser.Append("{\"email\":\"" + item.Email + "\",\"id\":\"" + item.Id + "\", \"name\":\"" + item.FirstName + " " + item.LastName + " " + "( " + item.Department_DepartmentId.Title + " )" + "\"},");
            }
            sbUser.Append("]");
            ViewBag.UserList = sbUser.ToString();

            StringBuilder sbDepartment = new StringBuilder();
            sbDepartment.Append("[");

            foreach (var item in db.Departments.OrderBy(x => x.Title).ToArray())
            {
                sbDepartment.Append("{\"id\":\"" + item.Id + "\", \"name\":\"" + item.Title + "\"},");
            }
            sbDepartment.Append("]");
            ViewBag.Departments = sbDepartment.ToString();

            StringBuilder sbFavoriteGroups = new StringBuilder();
            sbFavoriteGroups.Append("[");

            foreach (var item in db.FavoriteGroups.OrderBy(x => x.Name).ToArray())
            {
                sbFavoriteGroups.Append("{\"id\":\"" + item.Id + "\", \"name\":\"" + item.Name + "(" + prepareUsersName(item.Users) + ")" + "\"},");
            }
            sbFavoriteGroups.Append("]");
            ViewBag.FavoriteGroups = sbFavoriteGroups.ToString();


            ViewBag.ToUserId = new SelectList(UsersList, "Id", "Username", cUserId);

            var myUserList = db.ApplicationUsers.ToList();
            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var item in myUserList)
            {
                items.Add(new SelectListItem() { Text = item.Username, Value = item.Id.ToString() });
            }

            ViewBag.MyList = items;
        }
        private string prepareUsersName(ICollection<ApplicationUser> users)
        {
            string usersName = "";
            foreach (var user in users)
            {
                usersName += user.CompleteName + ", ";
            }
            return usersName.Remove(usersName.Length - 2);
        }

        public ActionResult EventAnnouncement()
        {
            int cUserId = Convert.ToInt32(Env.GetUserInfo("userid"));

            var tak = db.EventMembers.Where(e => e.ReceiverId == cUserId && e.IsRead == false).ToArray();

            StringBuilder sb = new StringBuilder();
            foreach (var item in tak)
            {
                sb.Append("<li class=\"clearfix\"> <a href=\"/EventMember/Details/" + item.EventMemberId + "\"> <i class=\"fa fa-calendar \"></i> " + (item.Obj_Event.Subject.Length > 20 ? item.Obj_Event.Subject.Substring(0, 25) + "..." : item.Obj_Event.Subject) + " </a> </li>");
            }
            ViewBag.sbs = sb.ToString();

            return PartialView();
        }

        public ActionResult EventCounter()
        {
            try
            {
                int cUserId = Convert.ToInt32(Env.GetUserInfo("userid"));

                int intEventCount = db.EventMembers.Where(e => e.ReceiverId == cUserId && e.IsRead == false).Select(e => e.EventId).Count();

                return Content(intEventCount.ToString());
            }
            catch (Exception)
            {

                return Content("False");
            }

        }

        [HttpPost]
        public ActionResult DeleteEvent(int Id)
        {
            try
            {
                List<EventMember> members = db.EventMembers.Where(e => e.EventId == Id).ToList();
                foreach (var item in members)
                {
                    db.EventMembers.Remove(item);
                }
                db.SaveChanges();
                Event dEvent = db.Events.FirstOrDefault(e => e.EventId == Id);
                if (!String.IsNullOrEmpty(dEvent.FileUrl))
                    System.IO.File.Delete(System.IO.Path.Combine(Server.MapPath("~/Uploads"), dEvent.FileUrl));
                BackupEvent be = new BackupEvent()
                {
                    Body = dEvent.Body,
                    CreateDate = dEvent.CreateDate,
                    DeletedBy = Env.GetUserInfo("name"),
                    DeleteOn = DateTime.Now,
                    FileUrl = dEvent.FileUrl,
                    IsDeleted = dEvent.IsDeleted,
                    SendDate = dEvent.SendDate,
                    SenderId = dEvent.SenderId,
                    Subject = dEvent.Subject

                };
                db.BackupEvents.Add(be);
                db.Events.Remove(dEvent);
                db.SaveChanges();
                foreach (var item in members)
                {
                    string s = notify(item.ReceiverId);
                    Help obj = new Help();
                    string de = obj.devent(item.ReceiverId);
                    var hubContext1 = GlobalHost.ConnectionManager.GetHubContext<MyHub>();
                    var user = db.ApplicationUsers.FirstOrDefault(x => x.Id == item.ReceiverId);
                    hubContext1.Clients.User(user.Username).testingmu(s, de);

                }



                return Content("True");
            }
            catch (Exception)
            {
                return Content("False");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}