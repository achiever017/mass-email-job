﻿using MassEmail.ChatClass;
using MassEmail.Controllers.Filters;
using MassEmail.Helper;
using MassEmail.Models;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MassEmail.Controllers
{
    public class JobManagementController : Controller
    {
        int count = 0;
        SIContext db = new SIContext();
        [AuthorizationAndRoleFilter(accessRole = AccessRole.JobManagement)]
        public ActionResult Index()
        {
            return View(db.JobManagements.ToList());
        }
        public ActionResult GetGrid()
        {
            var tak = db.JobManagements.ToArray().OrderByDescending(x => x.StartTime);
            var users = db.ApplicationUsers.ToArray();
            var us = db.JobManagementMembers.ToArray();

            var result = from c in tak
                         select new string[]
                                { Convert.ToString(c.JobtId),
                                    Convert.ToString("<a style='cursor:pointer !important;' onclick='expand1(" + c.JobtId + ")'>" + c.Title + "</a>"),
                                    //Convert.ToString(c.Subject),
                                    Convert.ToString(c.JobStatus),
                                    Convert.ToString(c.StartTime),
                                    Convert.ToString(c.EndTime),
                                    Convert.ToString(c.FileUrl),
                                    c.ApplicationUser.FirstName + ' ' + c.ApplicationUser.LastName,
                                newsfunc(c) };
            return Json(new { aaData = result = result.Skip(Math.Max(0, result.Count() - 30)).ToList() }, JsonRequestBehavior.AllowGet);

        }
        private string newsfunc(JobManagement e)
        {
            count++;
            string tu = "";
            string su = "";
            var data = db.JobManagementMembers.Where(m => m.JobtId == e.JobtId);
            // tu += "<a href='/Event/DetailViewIndex/"+ e.EventId +"' data-ajax-update=\"#SkDelete\" data-ajax-success=\"openModalDialog('SkDelete', 'Etkinlik Detay')\" data-ajax-mode=\"replace\" data-ajax-method=\"GET\" data-ajax-failure=\"clearModalDialog('SkDelete');alert('Ajax call failed')\" data-ajax-begin=\"prepareModalDialog('SkDelete')\" data-ajax=\"true\">" + data.First().ReceiverUser.FirstName + " " + data.First().ReceiverUser.LastName + "</a>";


            foreach (var item in data)
            {

                tu += item.ReceiverUser.FirstName + ' ' + item.ReceiverUser.LastName + (item.MemberJobStatus == 1 ? " (Beklemede)" :
                    //tu += item.ReceiverUser.Email + (item.EventStatus == 1 ? " (Beklemede)" : 

                    (item.MemberJobStatus == 2 ? " (Onaylandı)" :
                    (!String.IsNullOrEmpty(item.RejectReason)) ? " (Rededildi)" + " - " + item.RejectReason : " (Rededildi)"));
                tu += " ,  ";
            }
            //tu = tu.TrimEnd(' ');
            //tu = tu.TrimEnd(',');
            su += "<a style='cursor:pointer !important;' onclick='expand(" + e.JobtId + ")'>" + "<i class='fa fa-check-square-o f18'>" + "</a>";

            return su;
        }

        public ActionResult DetailViewIndex(int Id)
        {
            var JobDetail = db.JobManagements.FirstOrDefault(e => e.JobtId == Id);
            var Result = sfunc(JobDetail);
            return Json(new { aaData = Result }, JsonRequestBehavior.AllowGet);
        }
        private string sfunc(JobManagement e)
        {
            count++;
            string tu = "";
            var data = db.JobManagementMembers.Where(m => m.JobtId == e.JobtId).ToList();
            int i = 1;
            foreach (var item in data)
            {
                tu += i + ". ";
                tu += item.ReceiverUser.FirstName + ' ' + item.ReceiverUser.LastName + (item.MemberJobStatus == 1 ? " (Beklemede)" :
                    //tu += item.ReceiverUser.Email + (item.EventStatus == 1 ? " (Beklemede)" : 

                    (item.MemberJobStatus == 2 ? " (Onaylandı)" :
                    (!String.IsNullOrEmpty(item.RejectReason)) ? " (Rededildi)" + " - " + item.RejectReason : " (Rededildi)"));
                tu += " </br> ";
                i++;
            }
            tu = tu.TrimEnd(' ');
            tu = tu.TrimEnd(',');
            return tu;
        }

        public ActionResult MultiViewIndex1(int Id)
        {
            var JobDetail = db.JobManagements.FirstOrDefault(e => e.JobtId == Id);
            string[] JobMembers = db.JobManagementMembers.Where(e => e.JobtId == Id).Select(e => e.ReceiverUser.Email).ToArray();
            ViewBag.Receivers = JobMembers;

            var a = JobDetail.Description;
            var b = JobDetail.Title;
            return Json(new { aaData = a, body = b }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            LoadBinding();

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        [AuthorizationAndRoleFilter(accessRole = AccessRole.JobManagement)]
        public ActionResult Create(JobManagement Model, string sl, string toEmails, string toDepEmails, string toFavoriteEmails, HttpPostedFileBase filesAttach)
        {

            LoadBinding();
            if (ModelState.IsValid)
            {
                var gs = db.GeneralSettings.ToArray();
                int[] strUserIds = null;
                int cUserId = Convert.ToInt32(Env.GetUserInfo("userid"));
                string filename = "";
                if (filesAttach != null)
                {
                    filename = System.Guid.NewGuid().ToString() + "_" + System.IO.Path.GetFileName(filesAttach.FileName);
                    filesAttach.SaveAs(Server.MapPath("~/Uploads/" + filename));
                }

                if (!string.IsNullOrEmpty(toEmails))
                {
                    String[] users = toEmails.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    List<Int32> userIds = new List<int>();
                    if (users.Length <= 0)
                    {
                        ViewBag.Msg = "Alıcı Seçiniz";
                        return View("Create");
                    }
                    users.ToList().ForEach(x => userIds.Add(Convert.ToInt32(x)));
                    strUserIds = userIds.ToArray();
                }
                if (!string.IsNullOrEmpty(toDepEmails))
                {
                    String[] Depts = toDepEmails.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    List<Int32> DeptIDS = new List<int>();
                    if (Depts.Length <= 0)
                    {
                        ViewBag.Msg = "Alıcı Seçiniz";
                        return View("Create");
                    }
                    Depts.ToList().ForEach(x => DeptIDS.Add(Convert.ToInt32(x)));
                    if (strUserIds != null)
                    {
                        var depUserIds = db.ApplicationUsers.Where(x => DeptIDS.Contains(x.Department_DepartmentId.Id)).Select(x => x.Id).ToArray();
                        strUserIds = strUserIds.Concat(depUserIds).ToArray();
                    }
                    else
                        strUserIds = db.ApplicationUsers.Where(x => DeptIDS.Contains(x.Department_DepartmentId.Id)).Select(x => x.Id).ToArray();

                }

                if (!string.IsNullOrEmpty(toFavoriteEmails))
                {
                    String[] Favorites = toFavoriteEmails.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    List<Int32> FavaritesId = new List<int>();
                    if (Favorites.Length <= 0)
                    {
                        ViewBag.Msg = "Alıcı Seçiniz";
                        return View("Create");
                    }
                    Favorites.ToList().ForEach(x => FavaritesId.Add(Convert.ToInt32(x)));
                    if (strUserIds != null)
                    {
                        var favoritegroups = db.FavoriteGroups.Where(x => FavaritesId.Contains(x.Id)).ToArray();
                        var favUsersId = getIdUsersFromFavoriteGroups(favoritegroups);
                        strUserIds = strUserIds.Concat(favUsersId).ToArray();
                    }
                    else
                    {
                        var favoritegroups = db.FavoriteGroups.Where(x => FavaritesId.Contains(x.Id)).ToArray();
                        strUserIds = getIdUsersFromFavoriteGroups(favoritegroups);
                    }

                }

                if (strUserIds != null && strUserIds.Count() > 0)
                {
                    strUserIds = strUserIds.Distinct().ToArray();
                    //if (DateTime.Now.AddHours(24) > Model.CreatedAt) // 
                    //{
                    //    ViewBag.Msg = "24 Saat Sonrası için Etkinlik Oluşturabilirsiniz.";
                    //    return View("Create");
                    //}
                    //DateTime minEventTime = newevent.SendDate.AddHours(-24); // 
                    //DateTime maxEventTime = newevent.SendDate.AddHours(24); // 

                    //int intersectCount = db.Events.Where(w => w.SendDate >= minEventTime && w.SendDate <= maxEventTime && w.SenderId == cUserId).Count();

                    //if (intersectCount > 0)
                    //{
                    //    ViewBag.Msg = "24 Saat Sonrası için Etkinlik Oluşturabilirsiniz.";
                    //    return View("Create");
                    //}


                    Model.IsDeleted = false;
                    Model.CreatedBy = cUserId;
                    Model.FileUrl = filename;
                    Model.CreatedAt = DateTime.Now;
                    db.JobManagements.Add(Model);
                    db.SaveChanges();


                    foreach (int item in strUserIds)
                    {
                        JobManagementMember objMember = new JobManagementMember();
                        objMember.Obj_JobManagement = Model;
                        objMember.JobtId = Model.JobtId;
                        objMember.JobStatus = Model.JobStatus;
                        objMember.IsRead = false;
                        objMember.JobtId = Model.JobtId;
                        objMember.UserId = item;
                        objMember.MemberJobStatus = 1;
                        objMember.CreatedBy = cUserId;
                        objMember.CreatedAt = DateTime.Now;
                        db.JobManagementMembers.Add(objMember);
                        db.SaveChanges();

                        var user = db.ApplicationUsers.Where(i => i.Id == item).FirstOrDefault();
                        string s = notify(user.Id);
                        Help obj = new Help();
                        string de = obj.djob(user.Id);
                        var hubContext1 = GlobalHost.ConnectionManager.GetHubContext<MyHub>();

                        hubContext1.Clients.User(user.Username).testingmu(s, de);

                        //PushNotification.SendNotification(user.deviceID, "Yeni Bir Etkinliğiniz Var:" + Env.GetUserInfo("name") + " - " + newevent.Subject);
                        SendAsync(Env.GetUserInfo("name"), user.Username, Model.CreatedAt + "  " + Model.Title, Model.Description + " <a href='" + Request.Url.Scheme + Uri.SchemeDelimiter + Request.Url.Authority + "/JobManagementMember/Details/" + objMember.JobMemeberId.ToString() + "'>Detaylı Bilgi</a> için Tıklayınız.", gs, filesAttach);

                    }

                    db.SaveChanges();
                    ViewBag.Success = "";
                    return View("Create");
                }
                else
                {
                    ViewBag.Msg = "Alıcı Boş Geçilemez.";
                    return View("Create");
                }
            }
            else
            {
                return View(Model);
            }
        }
        private void LoadBinding()
        {
            var UsersList = db.ApplicationUsers.OrderBy(i => i.Department_DepartmentId.Title).ToArray();
            int cUserId = Convert.ToInt32(Env.GetUserInfo("userid"));

            StringBuilder sbUser = new StringBuilder();
            sbUser.Append("[");

            foreach (var item in UsersList.Where(i => i.Id != cUserId).ToArray())
            {
                sbUser.Append("{\"email\":\"" + item.Email + "\",\"id\":\"" + item.Id + "\", \"name\":\"" + item.FirstName + " " + item.LastName + " " + "( " + item.Department_DepartmentId.Title + " )" + "\"},");
            }
            sbUser.Append("]");
            ViewBag.UserList = sbUser.ToString();

            StringBuilder sbDepartment = new StringBuilder();
            sbDepartment.Append("[");

            foreach (var item in db.Departments.OrderBy(x => x.Title).ToArray())
            {
                sbDepartment.Append("{\"id\":\"" + item.Id + "\", \"name\":\"" + item.Title + "\"},");
            }
            sbDepartment.Append("]");
            ViewBag.Departments = sbDepartment.ToString();

            StringBuilder sbFavoriteGroups = new StringBuilder();
            sbFavoriteGroups.Append("[");

            foreach (var item in db.FavoriteGroups.OrderBy(x => x.Name).ToArray())
            {
                sbFavoriteGroups.Append("{\"id\":\"" + item.Id + "\", \"name\":\"" + item.Name + "(" + prepareUsersName(item.Users) + ")" + "\"},");
            }
            sbFavoriteGroups.Append("]");
            ViewBag.FavoriteGroups = sbFavoriteGroups.ToString();


            ViewBag.ToUserId = new SelectList(UsersList, "Id", "Username", cUserId);

            var myUserList = db.ApplicationUsers.ToList();
            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var item in myUserList)
            {
                items.Add(new SelectListItem() { Text = item.Username, Value = item.Id.ToString() });
            }

            ViewBag.MyList = items;
        }
        private int[] getIdUsersFromFavoriteGroups(ICollection<FavoriteGroup> favoritegroups)
        {
            IList<int> usersId = new List<int>();
            foreach (var group in favoritegroups)
            {
                foreach (var user in group.Users)
                    usersId.Add(user.Id);
            }
            return usersId.ToArray();
        }
        private string notify(int id)
        {
            StringBuilder sb = new StringBuilder();
            //   int user = Convert.ToInt32(Env.GetUserInfo("userid"));

            var events = db.EventMembers.Where(e => e.ReceiverId == id && e.IsRead == false).ToArray();

            int intCount = events.Count();

            sb.Append("<a href=\"#\" data-toggle=\"dropdown\" data-hover=\"dropdown\" data-close-others=\"true\"><i class=\"fa fa-calendar\"></i>");
            if (intCount > 0)
                sb.Append("<span class=\"badge badge-danger badge-header\">" + intCount + "</span></a>");
            else
                sb.Append("<span class=\"badge badge-danger badge-header\">" + intCount + "</span></a>");

            sb.Append("<li class=\"dropdown-header clearfix\"><p class=\"pull-left\">Etkinlikler</p></li><li class=\"dropdown-body\"><ul style=\"margin-top:-30px\" class=\"dropdown-menu-list withScroll\" data-height=\"220\">");

            foreach (var item in events)
            {
                sb.Append("<li> <a href=\"/EventMember/Details/" + item.EventMemberId + "\"> <h4><i class=\"fa fa-calendar-o\"></i> " + item.Obj_Event.Subject + "</h4></a> </li> <li class=\"divider\"></li>");
            }

            sb.Append("</ul></li> </li>  <li class=\"footer\"><a href=\"/EventMember/Index\">Tüm Etkinlikleri Görüntüle</a></li>  </ul>");


            return sb.ToString();
        }
        public Task SendAsync(string FromUser, string ToUser, string subject, string body, GeneralSetting[] gs, System.Web.HttpPostedFileBase filesAttach)
        {
            var result = Task.FromResult(0);
            var toAddress = new System.Net.Mail.MailAddress(ToUser, ToUser);

            try
            {
                System.Net.Mail.MailMessage emessage = new System.Net.Mail.MailMessage();
                if (filesAttach != null)
                {
                    System.Net.Mail.Attachment at = new System.Net.Mail.Attachment(filesAttach.InputStream, filesAttach.FileName);
                    emessage.Attachments.Add(at);
                }

                string Username = gs.FirstOrDefault(i => i.SettingKey == "Email").SettingValue;
                string Password = gs.FirstOrDefault(i => i.SettingKey == "EmailPassword").SettingValue;
                string port = gs.FirstOrDefault(i => i.SettingKey == "EmailPort").SettingValue;
                string SmptServer = gs.FirstOrDefault(i => i.SettingKey == "EmailServer").SettingValue;

                emessage.To.Add(toAddress);
                emessage.Subject = subject;
                emessage.From = new System.Net.Mail.MailAddress(FromUser);
                emessage.Body = body;
                emessage.IsBodyHtml = true;
                System.Net.Mail.SmtpClient sc = new System.Net.Mail.SmtpClient();
                var netCredential = new System.Net.NetworkCredential(Username, Password);
                sc.Host = SmptServer;
                sc.DeliveryMethod = SmtpDeliveryMethod.Network;
                sc.UseDefaultCredentials = false;
                sc.Credentials = netCredential;
                sc.Port = Convert.ToInt32(port);
                sc.Send(emessage);



                result = Task.FromResult(1);
            }
            catch (Exception)
            {
                result = Task.FromResult(0);
            }


            return result;


        }
        private string prepareUsersName(ICollection<ApplicationUser> users)
        {
            string usersName = "";
            foreach (var user in users)
            {
                usersName += user.CompleteName + ", ";
            }
            return usersName.Remove(usersName.Length - 2);
        }

        [HttpPost]
        public ActionResult DeleteJob(int Id)
        {
            try
            {
                List<JobManagementMember> members = db.JobManagementMembers.Where(e => e.JobtId == Id).ToList();
                foreach (var item in members)
                {
                    db.JobManagementMembers.Remove(item);
                }
                db.SaveChanges();
                JobManagement dJob = db.JobManagements.FirstOrDefault(e => e.JobtId == Id);
                if (!String.IsNullOrEmpty(dJob.FileUrl))
                    System.IO.File.Delete(System.IO.Path.Combine(Server.MapPath("~/Uploads"), dJob.FileUrl));
                BackupJobManagement be = new BackupJobManagement()
                {
                    Title=dJob.Title,
                    Description=dJob.Description,
                    StartTime=dJob.StartTime,
                    EndTime=dJob.EndTime,
                    JobStatus=dJob.JobStatus,
                    CreatedAt=dJob.CreatedAt,
                    CreatedBy=dJob.CreatedBy,
                    DeletedBy = Env.GetUserInfo("name"),
                    DeletedAt = DateTime.Now,
                    FileUrl = dJob.FileUrl,
                    IsDeleted = dJob.IsDeleted,

                };
                db.BackupJobManagements.Add(be);
                db.JobManagements.Remove(dJob);
                db.SaveChanges();
                foreach (var item in members)
                {
                    string s = notify(item.UserId);
                    Help obj = new Help();
                    string de = obj.djob(item.UserId);
                    var hubContext1 = GlobalHost.ConnectionManager.GetHubContext<MyHub>();
                    var user = db.ApplicationUsers.FirstOrDefault(x => x.Id == item.UserId);
                    hubContext1.Clients.User(user.Username).testingmu(s, de);

                }
                return Content("True");
            }
            catch (Exception)
            {
                return Content("False");
            }
        }

        public ActionResult JobAnnouncement()
        {
            int cUserId = Convert.ToInt32(Env.GetUserInfo("userid"));

            var tak = db.JobManagementMembers.Where(e => e.UserId == cUserId && e.IsRead == false).ToArray();

            StringBuilder sb = new StringBuilder();
            foreach (var item in tak)
            {
                sb.Append("<li class=\"clearfix\"> <a href=\"/JobManagementMember/Details/" + item.JobMemeberId + "\"> <i class=\"fa fa-calendar \"></i> " + (item.Obj_JobManagement.Title.Length > 20 ? item.Obj_JobManagement.Title.Substring(0, 25) + "..." : item.Obj_JobManagement.Title) + " </a> </li>");
            }
            ViewBag.sbs = sb.ToString();

            return PartialView();
        }
        public ActionResult JobCounter()
        {
            try
            {
                int cUserId = Convert.ToInt32(Env.GetUserInfo("userid"));

                int intJobCount = db.JobManagementMembers.Where(e => e.UserId == cUserId && e.IsRead == false).Select(e => e.JobtId).Count();

                return Content(intJobCount.ToString());
            }
            catch (Exception)
            {

                return Content("False");
            }

        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}