using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using MassEmail.Models;

namespace MassEmail.Controllers
{
    public class GeneralSettingController : Controller
    { 
        // GET: /GeneralSetting/
        public ActionResult Index()
        {
            return ModelBind();
        }
        
        // GET GeneralSetting/GetGrid
        public ActionResult GetGrid()
        {
            var tak = db.GeneralSettings.ToArray();
             
            var result = from c in tak select new string[] { c.Id.ToString(), Convert.ToString(c.Id),Convert.ToString(c.SettingKey),Convert.ToString(c.SettingValue),Convert.ToString(c.IsActive), };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }
        // GET: /GeneralSetting/ModelBindIndex
        public ActionResult ModelBindIndex()
        {
            return ModelBind();
        }
        // GET: /GeneralSetting/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GeneralSetting ObjGeneralSetting = db.GeneralSettings.Find(id);
            if (ObjGeneralSetting == null)
            {
                return HttpNotFound();
            }
            return View(ObjGeneralSetting);
        }
        // GET: /GeneralSetting/Create
        public ActionResult Create()
        {
             
             return View();
        }

        // POST: /GeneralSetting/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(GeneralSetting ObjGeneralSetting )
        {
            if (ModelState.IsValid)
            {


                db.GeneralSettings.Add(ObjGeneralSetting);
                db.SaveChanges();
                return Redirect(TempData["ThisUrl"].ToString());
            }
             
             return View(ObjGeneralSetting);
        }
        // GET: /GeneralSetting/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GeneralSetting ObjGeneralSetting = db.GeneralSettings.Find(id);
            if (ObjGeneralSetting == null)
            {
                return HttpNotFound();
            }
            
            return View(ObjGeneralSetting);
        }

        // POST: /GeneralSetting/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(GeneralSetting ObjGeneralSetting )
        {
            if (ModelState.IsValid)
            {
                

                db.Entry(ObjGeneralSetting).State = EntityState.Modified;
                db.SaveChanges();
                return Redirect(TempData["ThisUrl"].ToString());
            }
            
            return View(ObjGeneralSetting);
        }
        // GET: /GeneralSetting/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GeneralSetting ObjGeneralSetting = db.GeneralSettings.Find(id);
            if (ObjGeneralSetting == null)
            {
                return HttpNotFound();
            }
            return View(ObjGeneralSetting);
        }

        // POST: /GeneralSetting/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GeneralSetting ObjGeneralSetting = db.GeneralSettings.Find(id);
            db.GeneralSettings.Remove(ObjGeneralSetting);
            db.SaveChanges();
            return Redirect(TempData["ThisUrl"].ToString());
        }
        // GET: /GeneralSetting/MultiViewIndex/5
        public ActionResult MultiViewIndex(int? id)
        { 
            GeneralSetting ObjGeneralSetting = db.GeneralSettings.Find(id);
            ViewBag.IsWorking = 0;
            if (id > 0)
            {
                ViewBag.IsWorking = 1;
                
            }
            
            return View(ObjGeneralSetting);
        }

        // POST: /GeneralSetting/MultiViewIndex/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult MultiViewIndex(GeneralSetting ObjGeneralSetting )
        {
            if (ModelState.IsValid)
            {
                

                db.Entry(ObjGeneralSetting).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("MultiViewIndex", "GeneralSetting", new { id = ObjGeneralSetting.Id });
            }
            
            return View(ObjGeneralSetting);
        }
        private SIContext db = new SIContext();
		
		private ActionResult ModelBind()
        {
            var GeneralSettings = db.GeneralSettings;
            return View(GeneralSettings.ToList());
        }
		
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        
		 
    }
}

