using MassEmail.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace MassEmail.Maping
{
    public class ApplicationUserMap : EntityTypeConfiguration<ApplicationUser> 
    {
        public ApplicationUserMap()
        {
             HasKey(o => o.Id);
             Property(o => o.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
             Property(o => o.Username).HasMaxLength(100);
             Property(o => o.Password).HasMaxLength(100);
             Property(o => o.Email).HasMaxLength(50);
             Property(o => o.FirstName).HasMaxLength(100);
             Property(o => o.LastName).HasMaxLength(100);
             Property(o => o.AnnouncementCreat).IsOptional();
             Property(o => o.EventCreat).IsOptional();
             HasOptional(c => c.Department_DepartmentId).WithMany(o => o.ApplicationUser_DepartmentIds).HasForeignKey(o => o.DepartmentId).WillCascadeOnDelete(true);
             ToTable("ApplicationUser");
 

        }
    }
}
