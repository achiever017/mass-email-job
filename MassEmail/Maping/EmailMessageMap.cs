using MassEmail.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace MassEmail.Maping
{
    public class EmailMessageMap : EntityTypeConfiguration<EmailMessage> 
    {
        public EmailMessageMap()
        {
             HasKey(o => o.Id);
             Property(o => o.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
             Property(o => o.Subject).HasMaxLength(200);
             Property(o => o.MailText);
             ToTable("EmailMessage");
 

        }
    }
}
