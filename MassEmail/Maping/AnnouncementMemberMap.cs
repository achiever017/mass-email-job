using MassEmail.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace MassEmail.Maping
{
    public class AnnouncementMemberMap : EntityTypeConfiguration<AnnouncementMember> 
    {
        public AnnouncementMemberMap()
        {
             HasKey(o => o.Id);
             Property(o => o.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
             HasRequired(c => c.Announcement_EmailMessageId).WithMany(o => o.AnnouncementMember_EmailMessageIds).HasForeignKey(o => o.EmailMessageId).WillCascadeOnDelete(true);
             HasRequired(c => c.ApplicationUser_ToUserId).WithMany(o => o.AnnouncementMember_ToUserIds).HasForeignKey(o => o.ToUserId).WillCascadeOnDelete(true);
             ToTable("AnnouncementMember");
 

        }
    }
}
