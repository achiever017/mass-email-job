using MassEmail.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace MassEmail.Maping
{
    public class BlockListMap : EntityTypeConfiguration<BlockList> 
    {
        public BlockListMap()
        {
             HasKey(o => o.Id);
             Property(o => o.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
             HasRequired(c => c.ApplicationUser_UserId).WithMany(o => o.BlockList_UserIds).HasForeignKey(o => o.UserId).WillCascadeOnDelete(true);
             HasRequired(c => c.ApplicationUser_BlockedUserId).WithMany(o => o.BlockList_BlockedUserIds).HasForeignKey(o => o.BlockedUserId).WillCascadeOnDelete(false);
             ToTable("BlockList");
 

        }
    }
}
