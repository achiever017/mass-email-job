using MassEmail.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace MassEmail.Maping
{
    public class EmailMemberMap : EntityTypeConfiguration<EmailMember> 
    {
        public EmailMemberMap()
        {
             HasKey(o => o.Id);
             Property(o => o.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
             HasRequired(c => c.EmailMessage_EmailMessageId).WithMany(o => o.EmailMember_EmailMessageIds).HasForeignKey(o => o.EmailMessageId).WillCascadeOnDelete(true);
             HasRequired(c => c.ApplicationUser_ToUserId).WithMany(o => o.EmailMember_ToUserIds).HasForeignKey(o => o.ToUserId).WillCascadeOnDelete(true);
             ToTable("EmailMember");
 

        }
    }
}
