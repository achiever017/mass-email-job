using MassEmail.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace MassEmail.Maping
{
    public class EmailAttachmentMap : EntityTypeConfiguration<EmailAttachment> 
    {
        public EmailAttachmentMap()
        {
             HasKey(o => o.Id);
             Property(o => o.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
             HasRequired(c => c.EmailMessage_EmailMessageId).WithMany(o => o.EmailAttachment_EmailMessageIds).HasForeignKey(o => o.EmailMessageId).WillCascadeOnDelete(true);
             Property(o => o.FileUrl).HasMaxLength(300);
             ToTable("EmailAttachment");
 

        }
    }
}
