using MassEmail.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace MassEmail.Maping
{
    public class GeneralSettingMap : EntityTypeConfiguration<GeneralSetting> 
    {
        public GeneralSettingMap()
        {
             HasKey(o => o.Id);
             Property(o => o.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
             Property(o => o.SettingKey).HasMaxLength(100);
             Property(o => o.SettingValue);
             ToTable("GeneralSetting");
 

        }
    }
}
