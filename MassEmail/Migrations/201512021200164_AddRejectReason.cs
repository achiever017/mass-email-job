namespace MassEmail.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRejectReason : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EventMembers", "RejectReason", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.EventMembers", "RejectReason");
        }
    }
}
