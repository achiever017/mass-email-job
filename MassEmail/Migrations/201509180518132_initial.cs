namespace MassEmail.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ApplicationUser",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(nullable: false, maxLength: 100),
                        Password = c.String(nullable: false, maxLength: 100),
                        Email = c.String(nullable: false, maxLength: 50),
                        FirstName = c.String(maxLength: 100),
                        LastName = c.String(maxLength: 100),
                        DepartmentId = c.Int(),
                        IsActive = c.Boolean(),
                        CanEmail = c.Boolean(),
                        CanMessage = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Department", t => t.DepartmentId)
                .Index(t => t.DepartmentId);
            
            CreateTable(
                "dbo.ChatGroup",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GroupCreatorBy = c.Int(nullable: false),
                        IsActive = c.Boolean(),
                        GroupName = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.GroupCreatorBy)
                .Index(t => t.GroupCreatorBy);
            
            CreateTable(
                "dbo.ChatGroupUser",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ChatUserId = c.Int(nullable: false),
                        ChatGroupId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.ChatUserId)
                .ForeignKey("dbo.ChatGroup", t => t.ChatGroupId, cascadeDelete: true)
                .Index(t => t.ChatUserId)
                .Index(t => t.ChatGroupId);
            
            CreateTable(
                "dbo.ChatMessage",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ConnectionIDUser = c.String(maxLength: 200),
                        ChatGroupId = c.Int(nullable: false),
                        Msg = c.String(nullable: false, maxLength: 2000),
                        SendDate = c.DateTime(nullable: false),
                        SendFrom = c.Int(nullable: false),
                        IsDeleted = c.Boolean(),
                        ImageFile = c.String(maxLength: 200),
                        IsSeen = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ChatGroup", t => t.ChatGroupId)
                .ForeignKey("dbo.ApplicationUser", t => t.SendFrom)
                .Index(t => t.ChatGroupId)
                .Index(t => t.SendFrom);
            
            CreateTable(
                "dbo.Department",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 200),
                        IsActive = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EmailMember",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmailMessageId = c.Int(nullable: false),
                        ToUserId = c.Int(nullable: false),
                        IsRead = c.Boolean(),
                        DateUpdated = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.ToUserId)
                .ForeignKey("dbo.EmailMessage", t => t.EmailMessageId)
                .Index(t => t.ToUserId)
                .Index(t => t.EmailMessageId);
            
            CreateTable(
                "dbo.EmailMessage",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FromUserId = c.Int(nullable: false),
                        Subject = c.String(maxLength: 200),
                        MailText = c.String(),
                        DateSend = c.DateTime(),
                        IsDelete = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.FromUserId)
                .Index(t => t.FromUserId);
            
            CreateTable(
                "dbo.EmailAttachment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmailMessageId = c.Int(nullable: false),
                        FileUrl = c.String(maxLength: 300),
                        IsDeleted = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EmailMessage", t => t.EmailMessageId, cascadeDelete: true)
                .Index(t => t.EmailMessageId);
            
            CreateTable(
                "dbo.MenuPermission",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MenuId = c.Int(),
                        RoleId = c.Int(nullable: false),
                        UserId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.UserId)
                .ForeignKey("dbo.Menu", t => t.MenuId)
                .ForeignKey("dbo.Role", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.MenuId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Menu",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MenuText = c.String(nullable: false, maxLength: 100),
                        MenuURL = c.String(nullable: false, maxLength: 400),
                        ParentId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Menu", t => t.ParentId)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RoleName = c.String(nullable: false, maxLength: 50),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RoleUser",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RoleId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.Role", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MenuPermission", "RoleId", "dbo.Role");
            DropForeignKey("dbo.RoleUser", "RoleId", "dbo.Role");
            DropForeignKey("dbo.RoleUser", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.MenuPermission", "MenuId", "dbo.Menu");
            DropForeignKey("dbo.Menu", "ParentId", "dbo.Menu");
            DropForeignKey("dbo.MenuPermission", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.EmailMember", "EmailMessageId", "dbo.EmailMessage");
            DropForeignKey("dbo.EmailAttachment", "EmailMessageId", "dbo.EmailMessage");
            DropForeignKey("dbo.EmailMessage", "FromUserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.EmailMember", "ToUserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.ApplicationUser", "DepartmentId", "dbo.Department");
            DropForeignKey("dbo.ChatGroup", "GroupCreatorBy", "dbo.ApplicationUser");
            DropForeignKey("dbo.ChatMessage", "SendFrom", "dbo.ApplicationUser");
            DropForeignKey("dbo.ChatMessage", "ChatGroupId", "dbo.ChatGroup");
            DropForeignKey("dbo.ChatGroupUser", "ChatGroupId", "dbo.ChatGroup");
            DropForeignKey("dbo.ChatGroupUser", "ChatUserId", "dbo.ApplicationUser");
            DropIndex("dbo.MenuPermission", new[] { "RoleId" });
            DropIndex("dbo.RoleUser", new[] { "RoleId" });
            DropIndex("dbo.RoleUser", new[] { "UserId" });
            DropIndex("dbo.MenuPermission", new[] { "MenuId" });
            DropIndex("dbo.Menu", new[] { "ParentId" });
            DropIndex("dbo.MenuPermission", new[] { "UserId" });
            DropIndex("dbo.EmailMember", new[] { "EmailMessageId" });
            DropIndex("dbo.EmailAttachment", new[] { "EmailMessageId" });
            DropIndex("dbo.EmailMessage", new[] { "FromUserId" });
            DropIndex("dbo.EmailMember", new[] { "ToUserId" });
            DropIndex("dbo.ApplicationUser", new[] { "DepartmentId" });
            DropIndex("dbo.ChatGroup", new[] { "GroupCreatorBy" });
            DropIndex("dbo.ChatMessage", new[] { "SendFrom" });
            DropIndex("dbo.ChatMessage", new[] { "ChatGroupId" });
            DropIndex("dbo.ChatGroupUser", new[] { "ChatGroupId" });
            DropIndex("dbo.ChatGroupUser", new[] { "ChatUserId" });
            DropTable("dbo.RoleUser");
            DropTable("dbo.Role");
            DropTable("dbo.Menu");
            DropTable("dbo.MenuPermission");
            DropTable("dbo.EmailAttachment");
            DropTable("dbo.EmailMessage");
            DropTable("dbo.EmailMember");
            DropTable("dbo.Department");
            DropTable("dbo.ChatMessage");
            DropTable("dbo.ChatGroupUser");
            DropTable("dbo.ChatGroup");
            DropTable("dbo.ApplicationUser");
        }
    }
}
