namespace MassEmail.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BackupAnnouncements",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FromUserId = c.Int(nullable: false),
                        Subject = c.String(nullable: false, maxLength: 200),
                        MailText = c.String(nullable: false),
                        DateSend = c.DateTime(),
                        IsDelete = c.Boolean(),
                        FileUrl = c.String(),
                        DeletedBy = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.BackupAnnouncements");
        }
    }
}
