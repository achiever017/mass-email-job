namespace MassEmail.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BlockList",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        BlockedUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.BlockedUserId)
                .ForeignKey("dbo.ApplicationUser", t => t.UserId)
                .Index(t => t.BlockedUserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.GeneralSetting",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SettingKey = c.String(nullable: false, maxLength: 100),
                        SettingValue = c.String(),
                        IsActive = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BlockList", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.BlockList", "BlockedUserId", "dbo.ApplicationUser");
            DropIndex("dbo.BlockList", new[] { "UserId" });
            DropIndex("dbo.BlockList", new[] { "BlockedUserId" });
            DropTable("dbo.GeneralSetting");
            DropTable("dbo.BlockList");
        }
    }
}
