namespace MassEmail.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Outsource : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ApplicationUser", "FirstName", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.ApplicationUser", "LastName", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ApplicationUser", "LastName", c => c.String(maxLength: 100));
            AlterColumn("dbo.ApplicationUser", "FirstName", c => c.String(maxLength: 100));
        }
    }
}
