namespace MassEmail.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class oks12 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ChatGroup", "GroupCreatorBy", "dbo.ApplicationUser");
            DropIndex("dbo.ChatGroup", new[] { "GroupCreatorBy" });
            AlterColumn("dbo.ChatGroup", "GroupCreatorBy", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ChatGroup", "GroupCreatorBy", c => c.Int(nullable: false));
            CreateIndex("dbo.ChatGroup", "GroupCreatorBy");
            AddForeignKey("dbo.ChatGroup", "GroupCreatorBy", "dbo.ApplicationUser", "Id");
        }
    }
}
