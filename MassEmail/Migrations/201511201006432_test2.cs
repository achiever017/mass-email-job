namespace MassEmail.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BackupEvents",
                c => new
                    {
                        EventId = c.Int(nullable: false, identity: true),
                        SenderId = c.Int(nullable: false),
                        SendDate = c.DateTime(nullable: false),
                        Subject = c.String(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        Body = c.String(nullable: false),
                        FileUrl = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedBy = c.String(),
                        DeleteOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.EventId);
            
            AddColumn("dbo.BackupAnnouncements", "DeleteOn", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.BackupAnnouncements", "DeleteOn");
            DropTable("dbo.BackupEvents");
        }
    }
}
