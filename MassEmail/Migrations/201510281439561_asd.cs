namespace MassEmail.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class asd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Announcement", "FileUrl", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Announcement", "FileUrl");
        }
    }
}
