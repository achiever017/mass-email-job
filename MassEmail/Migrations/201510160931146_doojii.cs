namespace MassEmail.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class doojii : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BlockList", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.ChatGroupUser", "ChatUserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.ChatMessage", "ChatGroupId", "dbo.ChatGroup");
            DropForeignKey("dbo.ChatMessage", "SendFrom", "dbo.ApplicationUser");
            DropForeignKey("dbo.ApplicationUser", "DepartmentId", "dbo.Department");
            DropForeignKey("dbo.EmailMember", "ToUserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.EmailMessage", "FromUserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.Announcement", "FromUserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.AnnouncementMember", "ToUserId", "dbo.ApplicationUser");
            DropIndex("dbo.BlockList", new[] { "UserId" });
            DropIndex("dbo.ChatGroupUser", new[] { "ChatUserId" });
            DropIndex("dbo.ChatMessage", new[] { "ChatGroupId" });
            DropIndex("dbo.ChatMessage", new[] { "SendFrom" });
            DropIndex("dbo.ApplicationUser", new[] { "DepartmentId" });
            DropIndex("dbo.EmailMember", new[] { "ToUserId" });
            DropIndex("dbo.EmailMessage", new[] { "FromUserId" });
            DropIndex("dbo.Announcement", new[] { "FromUserId" });
            DropIndex("dbo.AnnouncementMember", new[] { "ToUserId" });
            CreateIndex("dbo.BlockList", "UserId");
            CreateIndex("dbo.ChatGroupUser", "ChatUserId");
            CreateIndex("dbo.ChatMessage", "ChatGroupId");
            CreateIndex("dbo.ChatMessage", "SendFrom");
            CreateIndex("dbo.ApplicationUser", "DepartmentId");
            CreateIndex("dbo.EmailMember", "ToUserId");
            CreateIndex("dbo.EmailMessage", "FromUserId");
            CreateIndex("dbo.Announcement", "FromUserId");
            CreateIndex("dbo.AnnouncementMember", "ToUserId");
            AddForeignKey("dbo.BlockList", "UserId", "dbo.ApplicationUser", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ChatGroupUser", "ChatUserId", "dbo.ApplicationUser", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ChatMessage", "ChatGroupId", "dbo.ChatGroup", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ChatMessage", "SendFrom", "dbo.ApplicationUser", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ApplicationUser", "DepartmentId", "dbo.Department", "Id", cascadeDelete: true);
            AddForeignKey("dbo.EmailMember", "ToUserId", "dbo.ApplicationUser", "Id", cascadeDelete: true);
            AddForeignKey("dbo.EmailMessage", "FromUserId", "dbo.ApplicationUser", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Announcement", "FromUserId", "dbo.ApplicationUser", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AnnouncementMember", "ToUserId", "dbo.ApplicationUser", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AnnouncementMember", "ToUserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.Announcement", "FromUserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.EmailMessage", "FromUserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.EmailMember", "ToUserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.ApplicationUser", "DepartmentId", "dbo.Department");
            DropForeignKey("dbo.ChatMessage", "SendFrom", "dbo.ApplicationUser");
            DropForeignKey("dbo.ChatMessage", "ChatGroupId", "dbo.ChatGroup");
            DropForeignKey("dbo.ChatGroupUser", "ChatUserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.BlockList", "UserId", "dbo.ApplicationUser");
            DropIndex("dbo.AnnouncementMember", new[] { "ToUserId" });
            DropIndex("dbo.Announcement", new[] { "FromUserId" });
            DropIndex("dbo.EmailMessage", new[] { "FromUserId" });
            DropIndex("dbo.EmailMember", new[] { "ToUserId" });
            DropIndex("dbo.ApplicationUser", new[] { "DepartmentId" });
            DropIndex("dbo.ChatMessage", new[] { "SendFrom" });
            DropIndex("dbo.ChatMessage", new[] { "ChatGroupId" });
            DropIndex("dbo.ChatGroupUser", new[] { "ChatUserId" });
            DropIndex("dbo.BlockList", new[] { "UserId" });
            CreateIndex("dbo.AnnouncementMember", "ToUserId");
            CreateIndex("dbo.Announcement", "FromUserId");
            CreateIndex("dbo.EmailMessage", "FromUserId");
            CreateIndex("dbo.EmailMember", "ToUserId");
            CreateIndex("dbo.ApplicationUser", "DepartmentId");
            CreateIndex("dbo.ChatMessage", "SendFrom");
            CreateIndex("dbo.ChatMessage", "ChatGroupId");
            CreateIndex("dbo.ChatGroupUser", "ChatUserId");
            CreateIndex("dbo.BlockList", "UserId");
            AddForeignKey("dbo.AnnouncementMember", "ToUserId", "dbo.ApplicationUser", "Id");
            AddForeignKey("dbo.Announcement", "FromUserId", "dbo.ApplicationUser", "Id");
            AddForeignKey("dbo.EmailMessage", "FromUserId", "dbo.ApplicationUser", "Id");
            AddForeignKey("dbo.EmailMember", "ToUserId", "dbo.ApplicationUser", "Id");
            AddForeignKey("dbo.ApplicationUser", "DepartmentId", "dbo.Department", "Id");
            AddForeignKey("dbo.ChatMessage", "SendFrom", "dbo.ApplicationUser", "Id");
            AddForeignKey("dbo.ChatMessage", "ChatGroupId", "dbo.ChatGroup", "Id");
            AddForeignKey("dbo.ChatGroupUser", "ChatUserId", "dbo.ApplicationUser", "Id");
            AddForeignKey("dbo.BlockList", "UserId", "dbo.ApplicationUser", "Id");
        }
    }
}
