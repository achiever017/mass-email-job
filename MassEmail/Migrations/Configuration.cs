namespace MassEmail.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MassEmail.Models.SIContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(MassEmail.Models.SIContext context)
        {
             //string query = string.Format(@""); context.Database.ExecuteSqlCommand(query);
        }
    }
}
