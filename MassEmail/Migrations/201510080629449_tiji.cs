namespace MassEmail.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tiji : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AnnouncementMember",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmailMessageId = c.Int(nullable: false),
                        ToUserId = c.Int(nullable: false),
                        IsRead = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Announcement", t => t.EmailMessageId)
                .ForeignKey("dbo.ApplicationUser", t => t.ToUserId)
                .Index(t => t.EmailMessageId)
                .Index(t => t.ToUserId);
            
            CreateTable(
                "dbo.Announcement",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FromUserId = c.Int(nullable: false),
                        Subject = c.String(maxLength: 200),
                        MailText = c.String(),
                        DateSend = c.DateTime(),
                        IsDelete = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.FromUserId)
                .Index(t => t.FromUserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AnnouncementMember", "ToUserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.AnnouncementMember", "EmailMessageId", "dbo.Announcement");
            DropForeignKey("dbo.Announcement", "FromUserId", "dbo.ApplicationUser");
            DropIndex("dbo.AnnouncementMember", new[] { "ToUserId" });
            DropIndex("dbo.AnnouncementMember", new[] { "EmailMessageId" });
            DropIndex("dbo.Announcement", new[] { "FromUserId" });
            DropTable("dbo.Announcement");
            DropTable("dbo.AnnouncementMember");
        }
    }
}
