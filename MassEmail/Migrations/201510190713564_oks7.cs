namespace MassEmail.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class oks7 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AnnouncementMember", "EmailMessageId", "dbo.Announcement");
            DropForeignKey("dbo.AnnouncementMember", "ToUserId", "dbo.ApplicationUser");
            DropIndex("dbo.AnnouncementMember", new[] { "EmailMessageId" });
            DropIndex("dbo.AnnouncementMember", new[] { "ToUserId" });
            AlterColumn("dbo.AnnouncementMember", "EmailMessageId", c => c.Int());
            AlterColumn("dbo.AnnouncementMember", "ToUserId", c => c.Int());
            CreateIndex("dbo.AnnouncementMember", "EmailMessageId");
            CreateIndex("dbo.AnnouncementMember", "ToUserId");
            AddForeignKey("dbo.AnnouncementMember", "EmailMessageId", "dbo.Announcement", "Id");
            AddForeignKey("dbo.AnnouncementMember", "ToUserId", "dbo.ApplicationUser", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AnnouncementMember", "ToUserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.AnnouncementMember", "EmailMessageId", "dbo.Announcement");
            DropIndex("dbo.AnnouncementMember", new[] { "ToUserId" });
            DropIndex("dbo.AnnouncementMember", new[] { "EmailMessageId" });
            AlterColumn("dbo.AnnouncementMember", "ToUserId", c => c.Int(nullable: false));
            AlterColumn("dbo.AnnouncementMember", "EmailMessageId", c => c.Int(nullable: false));
            CreateIndex("dbo.AnnouncementMember", "ToUserId");
            CreateIndex("dbo.AnnouncementMember", "EmailMessageId");
            AddForeignKey("dbo.AnnouncementMember", "ToUserId", "dbo.ApplicationUser", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AnnouncementMember", "EmailMessageId", "dbo.Announcement", "Id");
        }
    }
}
