namespace MassEmail.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPhoto : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Department", "PhotoPath", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Department", "PhotoPath");
        }
    }
}
