namespace MassEmail.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class oks9 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.EmailMessage", "FromUserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.EmailMember", "ToUserId", "dbo.ApplicationUser");
            DropIndex("dbo.EmailMessage", new[] { "FromUserId" });
            DropIndex("dbo.EmailMember", new[] { "ToUserId" });
            AlterColumn("dbo.EmailMember", "ToUserId", c => c.Int(nullable: false));
            CreateIndex("dbo.EmailMember", "ToUserId");
            AddForeignKey("dbo.EmailMember", "ToUserId", "dbo.ApplicationUser", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EmailMember", "ToUserId", "dbo.ApplicationUser");
            DropIndex("dbo.EmailMember", new[] { "ToUserId" });
            AlterColumn("dbo.EmailMember", "ToUserId", c => c.Int());
            CreateIndex("dbo.EmailMember", "ToUserId");
            CreateIndex("dbo.EmailMessage", "FromUserId");
            AddForeignKey("dbo.EmailMember", "ToUserId", "dbo.ApplicationUser", "Id");
            AddForeignKey("dbo.EmailMessage", "FromUserId", "dbo.ApplicationUser", "Id", cascadeDelete: true);
        }
    }
}
