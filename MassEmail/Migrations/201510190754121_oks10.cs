namespace MassEmail.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class oks10 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Announcement", "FromUserId", "dbo.ApplicationUser");
            DropIndex("dbo.Announcement", new[] { "FromUserId" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.Announcement", "FromUserId");
            AddForeignKey("dbo.Announcement", "FromUserId", "dbo.ApplicationUser", "Id", cascadeDelete: true);
        }
    }
}
