namespace MassEmail.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class oks4 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.EmailMember", "ToUserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.EmailMember", "EmailMessageId", "dbo.EmailMessage");
            DropIndex("dbo.EmailMember", new[] { "ToUserId" });
            DropIndex("dbo.EmailMember", new[] { "EmailMessageId" });
            AlterColumn("dbo.EmailMember", "ToUserId", c => c.Int());
            CreateIndex("dbo.EmailMember", "ToUserId");
            CreateIndex("dbo.EmailMember", "EmailMessageId");
            AddForeignKey("dbo.EmailMember", "ToUserId", "dbo.ApplicationUser", "Id");
            AddForeignKey("dbo.EmailMember", "EmailMessageId", "dbo.EmailMessage", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EmailMember", "EmailMessageId", "dbo.EmailMessage");
            DropForeignKey("dbo.EmailMember", "ToUserId", "dbo.ApplicationUser");
            DropIndex("dbo.EmailMember", new[] { "EmailMessageId" });
            DropIndex("dbo.EmailMember", new[] { "ToUserId" });
            AlterColumn("dbo.EmailMember", "ToUserId", c => c.Int(nullable: false));
            CreateIndex("dbo.EmailMember", "EmailMessageId");
            CreateIndex("dbo.EmailMember", "ToUserId");
            AddForeignKey("dbo.EmailMember", "EmailMessageId", "dbo.EmailMessage", "Id");
            AddForeignKey("dbo.EmailMember", "ToUserId", "dbo.ApplicationUser", "Id", cascadeDelete: true);
        }
    }
}
