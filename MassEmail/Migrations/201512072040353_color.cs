namespace MassEmail.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class color : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Department", "Color", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Department", "Color");
        }
    }
}
