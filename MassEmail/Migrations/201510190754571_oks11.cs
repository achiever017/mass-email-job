namespace MassEmail.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class oks11 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AnnouncementMember", "EmailMessageId", "dbo.Announcement");
            DropIndex("dbo.AnnouncementMember", new[] { "EmailMessageId" });
            CreateIndex("dbo.AnnouncementMember", "EmailMessageId");
            AddForeignKey("dbo.AnnouncementMember", "EmailMessageId", "dbo.Announcement", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AnnouncementMember", "EmailMessageId", "dbo.Announcement");
            DropIndex("dbo.AnnouncementMember", new[] { "EmailMessageId" });
            CreateIndex("dbo.AnnouncementMember", "EmailMessageId");
            AddForeignKey("dbo.AnnouncementMember", "EmailMessageId", "dbo.Announcement", "Id");
        }
    }
}
