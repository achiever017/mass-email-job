namespace MassEmail.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class oks8 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ChatGroup", "GroupCreatorBy", "dbo.ApplicationUser");
            DropForeignKey("dbo.AnnouncementMember", "EmailMessageId", "dbo.Announcement");
            DropForeignKey("dbo.AnnouncementMember", "ToUserId", "dbo.ApplicationUser");
            DropIndex("dbo.ChatGroup", new[] { "GroupCreatorBy" });
            DropIndex("dbo.AnnouncementMember", new[] { "EmailMessageId" });
            DropIndex("dbo.AnnouncementMember", new[] { "ToUserId" });
            AlterColumn("dbo.AnnouncementMember", "EmailMessageId", c => c.Int(nullable: false));
            AlterColumn("dbo.AnnouncementMember", "ToUserId", c => c.Int(nullable: false));
            AlterColumn("dbo.ChatGroup", "GroupCreatorBy", c => c.Int(nullable: false));
            CreateIndex("dbo.ChatGroup", "GroupCreatorBy");
            CreateIndex("dbo.AnnouncementMember", "EmailMessageId");
            CreateIndex("dbo.AnnouncementMember", "ToUserId");
            AddForeignKey("dbo.ChatGroup", "GroupCreatorBy", "dbo.ApplicationUser", "Id");
            AddForeignKey("dbo.AnnouncementMember", "EmailMessageId", "dbo.Announcement", "Id");
            AddForeignKey("dbo.AnnouncementMember", "ToUserId", "dbo.ApplicationUser", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AnnouncementMember", "ToUserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.AnnouncementMember", "EmailMessageId", "dbo.Announcement");
            DropForeignKey("dbo.ChatGroup", "GroupCreatorBy", "dbo.ApplicationUser");
            DropIndex("dbo.AnnouncementMember", new[] { "ToUserId" });
            DropIndex("dbo.AnnouncementMember", new[] { "EmailMessageId" });
            DropIndex("dbo.ChatGroup", new[] { "GroupCreatorBy" });
            AlterColumn("dbo.ChatGroup", "GroupCreatorBy", c => c.Int());
            AlterColumn("dbo.AnnouncementMember", "ToUserId", c => c.Int());
            AlterColumn("dbo.AnnouncementMember", "EmailMessageId", c => c.Int());
            CreateIndex("dbo.AnnouncementMember", "ToUserId");
            CreateIndex("dbo.AnnouncementMember", "EmailMessageId");
            CreateIndex("dbo.ChatGroup", "GroupCreatorBy");
            AddForeignKey("dbo.AnnouncementMember", "ToUserId", "dbo.ApplicationUser", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AnnouncementMember", "EmailMessageId", "dbo.Announcement", "Id");
            AddForeignKey("dbo.ChatGroup", "GroupCreatorBy", "dbo.ApplicationUser", "Id");
        }
    }
}
