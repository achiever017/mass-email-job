using AjaxClassJson;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MassEmail.Startup))]
namespace MassEmail
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
            if (AjaxLoad.per()) { }//craete cookies for login
        }
    }
}

