﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MassEmail.Models
{
    public class JobManagement
    {
        [Key]
        public int JobtId { get; set; }

        [ForeignKey("ApplicationUser")]
        public int CreatedBy { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy HH:mm}")]
        public DateTime StartTime { get; set; }

        //[Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy HH:mm}")]
        public DateTime? EndTime { get; set; }

        [Required]
        [DisplayName("Title")]
        public string Title { get; set; }

        public DateTime CreatedAt { get; set; }


        [Required]
        [DisplayName("Description")]
        public string Description { get; set; }

        [Required]
        [DisplayName("JobStatus")]
        public string JobStatus { get; set; }

        public string FileUrl { get; set; }

        public bool IsDeleted { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}