﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MassEmail.Models
{
    public class BackupAnnouncement
    {

        [DisplayName("S.No")]
        public int Id { get; set; }
        [Required]
        [DisplayName("Gönderen")]
        public int? FromUserId { get; set; }

        [Required]
        [StringLength(200)]
        [DisplayName("Konu")]
        public string Subject { get; set; }
        [Required]
        [DisplayName("Bildirim Mesaj")]
        public string MailText { get; set; }
        [DisplayName("Tarih")]
        public Nullable<DateTime> DateSend { get; set; }
        [DisplayName("Sil")]
        public Nullable<bool> IsDelete { get; set; }

        [DisplayName("Dosya")]
        public string FileUrl { get; set; }

        public string DeletedBy { get; set; }

        public Nullable<DateTime> DeleteOn { get; set; }

    }
}