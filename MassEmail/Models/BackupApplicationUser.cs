﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MassEmail.Models
{
    public class BackupApplicationUser
    {
        [DisplayName("S.No")]
        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        [DisplayName("Email")]
        public string Username { get; set; }
        [Required]
        [StringLength(100)]
        [DisplayName("Şifre")]
        public string Password { get; set; }
        [Required]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Please enter correct email")]
        [StringLength(50)]
        [DisplayName("Email Tekrar")]

        public string Email { get; set; }
        [StringLength(100)]
        [DisplayName("Ad")]
        public string FirstName { get; set; }
        [StringLength(100)]
        [DisplayName("Soyad")]
        public string LastName { get; set; }
        [DisplayName("Kurum")]
        public int? DepartmentId { get; set; }
        [DisplayName("Aktif")]
        public Nullable<bool> IsActive { get; set; }
        [DisplayName("Email Yetki")]
        public Nullable<bool> CanEmail { get; set; }
        [DisplayName("Mesaj Yetki")]
        public Nullable<bool> CanMessage { get; set; }
        public string DeletedBy { get; set; }

        public Nullable<DateTime> DeleteOn { get; set; }

    }
}