﻿using MassEmail.Models;
using System;
using System.Collections.Generic;

namespace MassEmail.Models.Comparer
{
    public class UserComparer : IEqualityComparer<ApplicationUser>
    {

        public bool Equals(ApplicationUser x, ApplicationUser y)
        {
            //Check whether the compared objects reference the same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check whether the products' properties are equal.
            return x.Id == y.Id && x.CompleteName == y.CompleteName;
        }
        public int GetHashCode(ApplicationUser user)
        {
            //Check whether the object is null
            if (Object.ReferenceEquals(user, null)) return 0;

            //Get hash code for the Name field if it is not null.
            int hashUserName = user.CompleteName == null ? 0 : user.CompleteName.GetHashCode();

            //Get hash code for the Code field.
            int hashUserCode = user.Id.GetHashCode();

            //Calculate the hash code for the product.
            return hashUserName ^ hashUserCode;
        }
    }
}