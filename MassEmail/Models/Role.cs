using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace MassEmail.Models
{
    public class Role
    {
        [DisplayName("S.No")] 
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        [DisplayName("Rol Ad")] 
        public string RoleName { get; set; }
        [Required]
        [DisplayName("Aktif")] 
        public bool IsActive { get; set; }
        public virtual ICollection<RoleUser> RoleUser_RoleIds { get; set; }
        public virtual ICollection<MenuPermission> MenuPermission_RoleIds { get; set; }

    }
}
