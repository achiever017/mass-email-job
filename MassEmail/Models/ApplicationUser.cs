﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using MassEmail.ChatClass;
using System.Data.Entity;
using System.Web.Mvc;

namespace MassEmail.Models
{
    public class ApplicationUser
    {
       

        [DisplayName("S.No")] 
        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        [DisplayName("Email")] 
        public string Username { get; set; }
        [Required]
        [DisplayName("TC")]
        public string TC { get; set; }
        [Required]
        [StringLength(100)]
        [DisplayName("Şifre")] 
        public string Password { get; set; }
        [Required]
[RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Doğru email adresi giriniz")]        [StringLength(50)]
[DisplayName("Email Tekrar")] 

        public string Email { get; set; }
        [StringLength(100)]
        public string deviceID { get; set; }
        [Required]
        [DisplayName("Ad")]
        public string FirstName { get; set; }
        [StringLength(100)]
        [DisplayName("Soyad")] 
        [Required]
        public string LastName { get; set; }

        [DisplayName("Kurum")] 
        public int? DepartmentId { get; set; }
        public virtual Department Department_DepartmentId { get; set; }

        [DisplayName("Supervisior")]
        public Nullable<bool> Supervisior { get; set; }
        [DisplayName("Başvuru Onay")]
       

        public Nullable<bool> BASVURU_ONAY { get; set; }
        [DisplayName("Keşif Onay")]
        public Nullable<bool> KESIF_ONAY { get; set; }
        [DisplayName("Ruhsat Onay")]
        public Nullable<bool> RUHSAT_ONAY { get; set; }
        [DisplayName("Aktif")] 
        public Nullable<bool> IsActive { get; set; }
        [DisplayName("Announcement")]
        public Nullable<bool> AnnouncementCreat { get; set; }
        [DisplayName("Event")]
        public Nullable<bool> EventCreat { get; set; }
        [DisplayName("Email Yetki")] 
        public Nullable<bool> CanEmail { get; set; }
        [DisplayName("Mesaj Yetki")]
       
        public Nullable<bool> CanMessage { get; set; }

        public String CompleteName
        {
            get
            {
                return this.FirstName + " " + this.LastName;
            }
        }

        public virtual ICollection<FavoriteGroup> FavoriteGroup_UserIds { get; set; }
        public virtual ICollection<RoleUser> RoleUser_UserIds { get; set; }
        public virtual ICollection<MenuPermission> MenuPermission_UserIds { get; set; } 
        public virtual ICollection<EmailMember> EmailMember_ToUserIds { get; set; }

         
        public virtual ICollection<ChatGroupUser> ChatGroupUser_ApplicationUserIds { get; set; }
        public virtual ICollection<ChatMessage> ChatMessage_SendFroms { get; set; }

        public virtual ICollection<BlockList> BlockList_UserIds { get; set; }
        public virtual ICollection<BlockList> BlockList_BlockedUserIds { get; set; }
         
        public virtual ICollection<AnnouncementMember> AnnouncementMember_ToUserIds { get; set; }

    }
}
