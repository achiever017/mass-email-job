﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MassEmail.Models
{
    public class BackupJobManagement
    {
        [Key]
        public int JobtId { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy HH:mm}")]
        public Nullable<DateTime> StartTime { get; set; }

        //[Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy HH:mm}")]
        public Nullable<DateTime> EndTime { get; set; }

        //[Required]
        [DisplayName("Title")]
        public string Title { get; set; }

        public Nullable<DateTime> CreatedAt { get; set; }
        public int CreatedBy { get; set; }

        //[Required]
        [DisplayName("Description")]
        public string Description { get; set; }

        //[Required]
        [DisplayName("JobStatus")]
        public string JobStatus { get; set; }

        public string FileUrl { get; set; }

        public bool IsDeleted { get; set; }

        public Nullable<DateTime> DeletedAt { get; set; }
        public string DeletedBy { get; set; }
    }
}