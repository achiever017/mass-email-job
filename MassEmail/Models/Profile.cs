﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MassEmail.Models
{
    public class Profile
    {
        public int id { get;set; }

        [Required]
        [StringLength(100)]
        [DisplayName("Ad")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100)]
        [DisplayName("Soyad")]
        public string LastName { get; set; }

        [Required]
        [StringLength(100)]
        [DisplayName("Email")]
        public string Username { get; set; }
       
        [Required]
        [StringLength(100)]
        [DisplayName("Şifre")]
        public string Password { get; set; }



    }
}