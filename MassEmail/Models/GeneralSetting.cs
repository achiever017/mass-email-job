using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace MassEmail.Models
{
    public class GeneralSetting
    {
        [DisplayName("S.No")] 
        public int Id { get; set; }
        [Required]
        [StringLength(100)] 
        [DisplayName("Ayar Key")] 
        public string SettingKey { get; set; }
        [DisplayName("Ayar Value")] 
        public string SettingValue { get; set; }
        [DisplayName("Aktif")] 
        public Nullable<bool> IsActive { get; set; }

    }
}
