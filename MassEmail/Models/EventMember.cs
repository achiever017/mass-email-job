﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MassEmail.Models
{
    public class EventMember
    {
        [Key]
        public int EventMemberId { get; set; }

        [Required]
        [DisplayName("Konu")]
        [ForeignKey("Obj_Event")]
        public int EventId { get; set; }

        [Required]
        [DisplayName("Alıcı")]
        [ForeignKey("ReceiverUser")]
        public int ReceiverId { get; set; }

        [DisplayName("Okundu")]
        public bool IsRead { get; set; }

        [DisplayName("Durum")]
        public int EventStatus { get; set; }

        [DisplayName("Reject Reason")]
        public String RejectReason { get; set; }
        public virtual Event Obj_Event { get; set; }
        public virtual ApplicationUser ReceiverUser { get; set; }

    }
}