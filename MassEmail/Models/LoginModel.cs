﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MassEmail.Models
{
    public class LoginModel
    {
        public String email { set; get; }
        public String password { set; get; }
        public String deviceID { set; get; }
    }
}