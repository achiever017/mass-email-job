﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MassEmail.Models
{
    public class BackupDepartment
    {

        [DisplayName("S.No")]
        public int Id { get; set; }
        [Required]
        [StringLength(200)]
        [DisplayName("Başlık")]
        public string Title { get; set; }
        [DisplayName("Aktif")]
        public Nullable<bool> IsActive { get; set; }
        public string DeletedBy { get; set; }

        public Nullable<DateTime> DeleteOn { get; set; }
    }
}