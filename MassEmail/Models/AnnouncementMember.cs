using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace MassEmail.Models
{
    public class AnnouncementMember
    {
        [DisplayName("S.No")] 
        public int Id { get; set; }
        [Required]
        [DisplayName("Konu")] 
        public int? EmailMessageId { get; set; }
        public virtual Announcement Announcement_EmailMessageId { get; set; }
        [Required]
        [DisplayName("Al�c�")] 
        public int? ToUserId { get; set; }
        public virtual ApplicationUser ApplicationUser_ToUserId { get; set; }
        [DisplayName("Okundu")] 
        public Nullable<bool> IsRead { get; set; }

        public virtual ICollection<Announcement> Announcement_FileUrl { get; set; }

    }
}
