using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace MassEmail.Models
{
    public class Department
    {
        [DisplayName("S.No")] 
        public int Id { get; set; }
        [Required]
        [StringLength(200)]
        [DisplayName("Ba�l�k")] 
        public string Title { get; set; }
        [DisplayName("Aktif")]
        public Nullable<bool> IsActive { get; set; }
        [DisplayName("Logo")] 
        public string PhotoPath { get; set; }
        public virtual ICollection<ApplicationUser> ApplicationUser_DepartmentIds { get; set; }
        [Required]
        [DisplayName("Renk")] 
        public string Color { get; set; }
    }
}
