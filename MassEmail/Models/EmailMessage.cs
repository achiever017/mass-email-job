using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace MassEmail.Models
{
    public class EmailMessage
    {
        [DisplayName("S.No")] 
        public int Id { get; set; }
        [Required]
        [DisplayName("G�nderen")] 
        public int? FromUserId { get; set; }
        
        [StringLength(200)]
        [Required]
        [DisplayName("Konu")] 
        public string Subject { get; set; }

        [Required]
        [DisplayName("Mesaj")] 
        public string MailText { get; set; }
        [DisplayName("Tarih")] 
        public Nullable<DateTime> DateSend { get; set; }
        [DisplayName("Sil")] 
        public Nullable<bool> IsDelete { get; set; }
        public virtual ICollection<EmailMember> EmailMember_EmailMessageIds { get; set; }
        public virtual ICollection<EmailAttachment> EmailAttachment_EmailMessageIds { get; set; }

    }
}
