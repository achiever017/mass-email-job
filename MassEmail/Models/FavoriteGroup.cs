﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MassEmail.Models
{
    public class FavoriteGroup
    {
        public FavoriteGroup()
        {
            this.Users = new List<ApplicationUser>();
        }
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public virtual ICollection<ApplicationUser> Users { get; set; }
    }
}