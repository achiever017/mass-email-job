﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MassEmail.Models
{
    public class JobManagementMember
    {
        [Key]
        public int JobMemeberId { get; set; }

        [Required]
        [DisplayName("JobtId")]
        [ForeignKey("Obj_JobManagement")]
        public int JobtId { get; set; }

        //[ForeignKey("ApplicationUser")]
        public int CreatedBy { get; set; }

        [Required]
        [ForeignKey("ReceiverUser")]
        public int UserId { get; set; }  

        public DateTime CreatedAt { get; set; }

        public string JobStatus { get; set; }
        public int? MemberJobStatus { get; set; }
        public string RejectReason { get; set; }
        public bool IsRead { get; set; }

        public virtual JobManagement Obj_JobManagement { get; set; }
        public virtual ApplicationUser ReceiverUser { get; set; }
    }
}