﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;



namespace MassEmail.Models
{
    public class Event
    {
        [Key]
        public int EventId { get; set; }

        [ForeignKey("ApplicationUser")]
        public int SenderId { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy HH:mm}")]
        public DateTime SendDate { get; set; }

        [Required]
        [DisplayName("Konu")] 
        public string Subject { get; set; }

        public DateTime CreateDate { get; set; }


        [Required]
        [DisplayName("Mesaj")] 
        public string Body { get; set; }


        public string FileUrl { get; set; }

        public bool IsDeleted { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
