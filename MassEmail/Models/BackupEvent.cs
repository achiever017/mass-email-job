﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MassEmail.Models
{
    public class BackupEvent
    {
        [Key]
        public int EventId { get; set; }

       
        public int SenderId { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy HH:mm}")]
        public DateTime SendDate { get; set; }

        [Required]
        [DisplayName("Konu")]
        public string Subject { get; set; }

        public DateTime CreateDate { get; set; }


        [Required]
        [DisplayName("Etkinlik Mesaj")]
        public string Body { get; set; }


        public string FileUrl { get; set; }

        public bool IsDeleted { get; set; }

        public string DeletedBy { get; set; }

        public Nullable<DateTime> DeleteOn { get; set; }
    }
}