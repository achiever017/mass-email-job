using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace MassEmail.Models
{
    public class BlockList
    {
        [DisplayName("S.No")] 
        public int Id { get; set; }
        [Required]
        [DisplayName("Kullan�c�")] 
        public int? UserId { get; set; }
        public virtual ApplicationUser ApplicationUser_UserId { get; set; }
        [Required]
        [DisplayName("Blok Kullan�c�")] 
        public int? BlockedUserId { get; set; }
        public virtual ApplicationUser ApplicationUser_BlockedUserId { get; set; }

    }
}
