using MassEmail.ChatClass;
using MassEmail.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace MassEmail.Models
{
    public class SIContext : DbContext
    {
        public SIContext()
            : base("name=SIConnectionString")
        {
        }

         
		public virtual DbSet<Role> Roles { get; set; }
		public virtual DbSet<ApplicationUser> ApplicationUsers { get; set; }
		public virtual DbSet<RoleUser> RoleUsers { get; set; }
		public virtual DbSet<Menu> Menus { get; set; }
		public virtual DbSet<MenuPermission> MenuPermissions { get; set; }
		public virtual DbSet<Department> Departments { get; set; }
		public virtual DbSet<EmailMessage> EmailMessages { get; set; }
		public virtual DbSet<EmailMember> EmailMembers { get; set; }
		public virtual DbSet<EmailAttachment> EmailAttachments { get; set; }
         
        public virtual DbSet<ChatGroup> ChatGroups { get; set; }
        public virtual DbSet<ChatGroupUser> ChatGroupUsers { get; set; }
        public virtual DbSet<ChatMessage> ChatMessages { get; set; }
        public virtual DbSet<GeneralSetting> GeneralSettings { get; set; }
        public virtual DbSet<BlockList> BlockLists { get; set; }
        public virtual DbSet<Announcement> Announcements { get; set; }
        public virtual DbSet<AnnouncementMember> AnnouncementMembers { get; set; }
        public virtual DbSet<Event> Events { get;set; }
        public virtual DbSet<EventMember> EventMembers { get; set; }

        public virtual DbSet<BackupAnnouncement> BackupAnnouncements { get; set; }
        public virtual DbSet<BackupEvent> BackupEvents { get; set; }
        public virtual DbSet<BackupApplicationUser> BackupApplicationUsers { get; set; }

        public virtual DbSet<BackupDepartment> BackupDepartments { get; set; }

        public virtual DbSet<FavoriteGroup> FavoriteGroups { get; set; }
       
        //
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
             
			modelBuilder.Configurations.Add(new MassEmail.Maping.RoleMap());
			modelBuilder.Configurations.Add(new MassEmail.Maping.ApplicationUserMap());
			modelBuilder.Configurations.Add(new MassEmail.Maping.RoleUserMap());
			modelBuilder.Configurations.Add(new MassEmail.Maping.MenuMap());
			modelBuilder.Configurations.Add(new MassEmail.Maping.MenuPermissionMap());
			modelBuilder.Configurations.Add(new MassEmail.Maping.DepartmentMap());
			modelBuilder.Configurations.Add(new MassEmail.Maping.EmailMessageMap());
			modelBuilder.Configurations.Add(new MassEmail.Maping.EmailMemberMap());
			modelBuilder.Configurations.Add(new MassEmail.Maping.EmailAttachmentMap());
             
            modelBuilder.Configurations.Add(new ChatGroupMap());
            modelBuilder.Configurations.Add(new ChatGroupUserMap());
            modelBuilder.Configurations.Add(new ChatMessageMap());

            modelBuilder.Configurations.Add(new MassEmail.Maping.GeneralSettingMap());
            modelBuilder.Configurations.Add(new MassEmail.Maping.BlockListMap());


            modelBuilder.Configurations.Add(new MassEmail.Maping.AnnouncementMap());
            modelBuilder.Configurations.Add(new MassEmail.Maping.AnnouncementMemberMap());


            base.OnModelCreating(modelBuilder);
        }
        //
    }
}
 
