using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace MassEmail.Models
{
    public class EmailAttachment
    {
        [DisplayName("S.No")] 
        public int Id { get; set; }
        [Required]
        [DisplayName("Email Mesaj")] 
        public int? EmailMessageId { get; set; }
        public virtual EmailMessage EmailMessage_EmailMessageId { get; set; }
        [DisplayName("Dosya")] 
        public string FileUrl { get; set; }
        [DisplayName("Sil")] 
        public Nullable<bool> IsDeleted { get; set; }

    }
}
