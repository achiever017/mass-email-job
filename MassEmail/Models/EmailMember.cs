using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace MassEmail.Models
{
    public class EmailMember
    {
        [DisplayName("S.No")] 
        public int Id { get; set; }
        [Required]
        [DisplayName("Email Mesaj")] 
        public int? EmailMessageId { get; set; }
        public virtual EmailMessage EmailMessage_EmailMessageId { get; set; }
        [Required]
        [DisplayName("Kullan�c�")]
        public Nullable<int> ToUserId { get; set; }
        public virtual ApplicationUser ApplicationUser_ToUserId { get; set; }
        [DisplayName("Okundu")] 
        public Nullable<bool> IsRead { get; set; }
        [DisplayName("Tarih")] 
        public Nullable<DateTime> DateUpdated { get; set; }

    }
}
