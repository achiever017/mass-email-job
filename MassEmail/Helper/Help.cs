﻿using MassEmail.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace MassEmail.Helper
{
    public class Help
    {
        private SIContext db = new SIContext();
        public string dashnotification(int? id)
        {

            StringBuilder sb = new StringBuilder();
            string AnnounceId = "";
            string Announcee = "";

            var ann = db.AnnouncementMembers.Where(i => i.IsRead == false && i.ToUserId == id).ToList().LastOrDefault();
           // string Announce = "&nbsp;";
            int TotalAnnouncements = 0;
            string File = "";
            Announcement anno = new Announcement();
            if (ann != null)
            {
                AnnounceId = Convert.ToString(ann.Id);
                Announcee = ann.Announcement_EmailMessageId.Subject;
                 anno = db.Announcements.FirstOrDefault(x => x.Id == ann.Announcement_EmailMessageId.Id);

                File = anno.FileUrl;
                //   var NA = anno.FileUrl;



                 TotalAnnouncements = db.AnnouncementMembers.Count(a => a.ToUserId == id&&a.IsRead==false);
            }

            sb.Append("<div class=\"small-box bg-maroon\" style=\"border-radius: 6px\"><div class=\"inner\"><h3>" +
               TotalAnnouncements + "<p> Güncel Bildirim</p> <sup style=\"font-size: 60px\">  </sup></h3><p style=\"white-space: nowrap; overflow: hidden; text-overflow: ellipsis; max-width: 400px\">" + Announcee + "</p>");

             
                if (File != "")
                {
                    sb.Append("<a href=\"/Uploads/" + File + "\" class=\"btn btn-success2\" target=\"_blank\" >Dosya Ek</a>");

                }
                sb.Append("</div> <div class=\"icon\"><i class=\"fa fa-flag-o\" style=\"font-size: 50px; padding-bottom: 45px; padding-right: 5px\"></i></div>");
                if (AnnounceId != "")
                {
                    sb.Append("<a class=\"small-box-footer\" title=\"Bunu Oku\" href=\"/AnnouncementMember/Details/" +
                        AnnounceId + "\"> Oku <i class=\"fa fa-arrow-circle-right\"></i></a>");
                }

                sb.Append("</div>");





            return sb.ToString();



        }


        public string devent(int id) {

            StringBuilder sb=new StringBuilder();
             int TotalEvents = db.EventMembers.Count(e => e.ReceiverId == id&&e.IsRead==false);

            int intNewEventCount=db.EventMembers.Where(e => e.ReceiverId == id && e.IsRead == false).Count();

            int NewEventCount = intNewEventCount;
            string NewEventSubject ="";
            string NewEventMemberID="";
            string NewEventFile="";
            if (intNewEventCount > 0)
            {
                var evm = db.EventMembers.Where(e => e.ReceiverId == id && e.IsRead == false).ToList().LastOrDefault();
                NewEventSubject = evm.Obj_Event.Subject;
                NewEventMemberID =Convert.ToString(evm.EventMemberId);
                NewEventFile = evm.Obj_Event.FileUrl;
            }





            sb.Append("<div class=\"small-box bg-maroon\" style=\"border-radius: 6px\"><div class=\"inner\"><h3>" +
                TotalEvents + "<p>Güncel Etkinlik</p> <sup style=\"font-size: 60px\">  </sup></h3><p style=\"white-space: nowrap; overflow: hidden; text-overflow: ellipsis; max-width: 400px\">" + NewEventSubject + " </p>");
            if (NewEventFile != "")
            {
                sb.Append("<a href=\"/Uploads/@Html.Raw(Model.Obj_Event.FileUrl)\" class=\"btn btn-success2\" target=\"_blank\">Dosya Ek</a>");
            }
            sb.Append("</div><div class=\"icon\"><i class=\"fa fa fa-calendar\" style=\"font-size: 50px; padding-bottom: 45px; padding-right: 5px\"></i></div>");
            if (NewEventCount > 0)
            {
                sb.Append("<a class=\"small-box-footer\" title=\"Bunu Oku\" href=\"/EventMember/Details/" +
                    NewEventMemberID + "\">Oku <i class=\"fa fa-arrow-circle-right\"></i> </a>");
            }
            sb.Append("</div>");

























            return sb.ToString();
        }


    }
}