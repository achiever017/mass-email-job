using AjaxClassJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace MassEmail
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ModelBinders.Binders.Add(typeof(DateTime), new MassEmail.Models.DateTimeBinder());
            ModelBinders.Binders.Add(typeof(DateTime?), new MassEmail.Models.NullableDateTimeBinder());
            #region 
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.Name; //Create Cookies for user login
            if (AjaxLoad.per()) { } //Create Cookies for user login
            #endregion
            
        }
    }
}

