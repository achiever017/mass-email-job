﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MassEmail.ChatClass
{
    public class DtoMessageInfo
    {
        public string UserName { get; set; }

        public string Message { get; set; }

        public int UserGroup { get; set; }
 

        public string MsgDate { get; set; }
    }
}