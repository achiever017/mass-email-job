using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using MassEmail.Models;

namespace MassEmail.ChatClass
{
    public class ChatMessage
    {
        public int Id { get; set; }
        [StringLength(200)] 
        public string ConnectionIDUser { get; set; }
        [Required]
        public int? ChatGroupId { get; set; }
        public virtual ChatGroup ChatGroup_ChatGroupId { get; set; }
        [Required]
        [StringLength(2000)] 
        public string Msg { get; set; }
        [Required]
        public DateTime SendDate { get; set; }
        [Required]
        public int? SendFrom { get; set; }
        public virtual ApplicationUser ChatUser_SendFrom { get; set; }

        public Nullable<bool> IsDeleted { get; set; }

        public string ImageFile { get; set; }
        public Nullable<bool> IsSeen { get; set; }

    }
    public class ChatMessageMap : EntityTypeConfiguration<ChatMessage>
    {
        public ChatMessageMap()
        {
            HasKey(o => o.Id);
            Property(o => o.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(o => o.ConnectionIDUser).HasMaxLength(200);
            HasRequired(c => c.ChatGroup_ChatGroupId).WithMany(o => o.ChatMessage_ChatGroupIds).HasForeignKey(o => o.ChatGroupId).WillCascadeOnDelete(true);
            HasRequired(c => c.ChatUser_SendFrom).WithMany(o => o.ChatMessage_SendFroms).HasForeignKey(o => o.SendFrom).WillCascadeOnDelete(true);
            Property(o => o.Msg).HasMaxLength(2000);
            Property(o => o.ImageFile).HasMaxLength(200);
            ToTable("ChatMessage");


        }
    }
}
