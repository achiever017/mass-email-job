﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MassEmail.ChatClass
{
    public class DtoUserInfo
    {
        public string ConnectionId { get; set; }
        public string UserName { get; set; }
        public int UserGroup { get; set; } 
        public int FromId { get; set; } 
    }

    public class DtoGrouping
    {
        public int GroupCount { get; set; }
        public int ChatGroupId { get; set; }
    }
}