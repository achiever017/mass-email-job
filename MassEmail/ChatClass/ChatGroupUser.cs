using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using MassEmail.Models;

namespace MassEmail.ChatClass
{
    public class ChatGroupUser
    {
        public int Id { get; set; }
        [Required]
        public int? ChatUserId { get; set; }
        public virtual ApplicationUser ApplicationUser_ApplicationUserId { get; set; }
        [Required]
        public int? ChatGroupId { get; set; }
        public virtual ChatGroup ChatGroup_ChatGroupId { get; set; }

    }
    public class ChatGroupUserMap : EntityTypeConfiguration<ChatGroupUser>
    {
        public ChatGroupUserMap()
        {
            HasKey(o => o.Id);
            Property(o => o.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            HasRequired(c => c.ApplicationUser_ApplicationUserId).WithMany(o => o.ChatGroupUser_ApplicationUserIds).HasForeignKey(o => o.ChatUserId).WillCascadeOnDelete(true);
            HasRequired(c => c.ChatGroup_ChatGroupId).WithMany(o => o.ChatGroupUser_ChatGroupIds).HasForeignKey(o => o.ChatGroupId).WillCascadeOnDelete(true);
            ToTable("ChatGroupUser");


        }
    }
}
