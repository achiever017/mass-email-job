﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MassEmail.ChatClass
{
    public static class Extension
    {
        public static string TimeAgo(this DateTime date)
        { 
            TimeSpan timeSince = DateTime.Now.Subtract(date);

            if (timeSince.TotalMilliseconds < 1)
                return "Henüz değil";
            if (timeSince.TotalMinutes < 1)
                return "Şimdi";
            if (timeSince.TotalMinutes < 2)
                return "1 dakika önce";
            if (timeSince.TotalMinutes < 60)
                return string.Format("{0} dakika önce", timeSince.Minutes);
            if (timeSince.TotalMinutes < 120)
                return "1 saat önce";
            if (timeSince.TotalHours < 24)
                return string.Format("{0} saat önce", timeSince.Hours);
            if (timeSince.TotalDays == 1)
                return "dün";
            if (timeSince.TotalDays < 7)
                return string.Format("{0} gün önce", timeSince.Days);
            if (timeSince.TotalDays < 14)
                return "1 hafta önce";
            if (timeSince.TotalDays < 21)
                return "2 hafta önce";
            if (timeSince.TotalDays < 28)
                return "3 hafta önce";
            if (timeSince.TotalDays < 60)
                return "1 ay önce";
            if (timeSince.TotalDays < 365)
                return string.Format("{0} ay önce", Math.Round(timeSince.TotalDays / 30));
            if (timeSince.TotalDays < 730)
                return "1 yıl önce";

            //last but not least...
            return string.Format("{0} yıl önce", Math.Round(timeSince.TotalDays / 365));

        }

        public static string GetSiteRoot()
        {
            string sOut = "";
            if (System.Web.HttpContext.Current != null)
            {
                string Port = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
                if (Port == null || Port == "80" || Port == "443")
                    Port = string.Empty;
                else
                    Port = ":" + Port;

                string Protocol = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT_SECURE"];
                if (Protocol == null || Protocol.Equals("0"))
                    Protocol = "http://";
                else
                    Protocol = "https://";

                string appPath = System.Web.HttpContext.Current.Request.ApplicationPath;
                if (appPath == "/")
                    appPath = "";

                sOut = Protocol + System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"] + Port + appPath;
            }
            return sOut;
        }
    }
}