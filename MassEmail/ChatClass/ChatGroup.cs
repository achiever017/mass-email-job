using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using MassEmail.Models;

namespace MassEmail.ChatClass
{
    public class ChatGroup
    {
        public int Id { get; set; }
       
        public int? GroupCreatorBy { get; set; }
        
        public Nullable<bool> IsActive { get; set; }
        [StringLength(200)] 
        public string GroupName { get; set; }
        public virtual ICollection<ChatGroupUser> ChatGroupUser_ChatGroupIds { get; set; }
        public virtual ICollection<ChatMessage> ChatMessage_ChatGroupIds { get; set; }

    }
    public class ChatGroupMap : EntityTypeConfiguration<ChatGroup>
    {
        public ChatGroupMap()
        {
            HasKey(o => o.Id);
            Property(o => o.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
             Property(o => o.GroupName).HasMaxLength(200);
            ToTable("ChatGroup"); 
        }
    }
}
