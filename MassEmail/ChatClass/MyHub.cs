﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Threading.Tasks;
using MassEmail.Models;
using System.Text;
using System.Data.Entity;
using AjaxClassJson;
using MassEmail.Controllers;

using System.Data.SqlClient;
using System.Xml; 
using System.Diagnostics;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Security.Authentication;
using System.Net;
using System.Collections.Specialized;
using System.Web.UI.WebControls;
using System.IO;

namespace MassEmail.ChatClass
{
    public class MyHub : Hub
    {
        public static List<DtoUserInfo> UsersList = new List<DtoUserInfo>();
        public static List<DtoMessageInfo> MessageList = new List<DtoMessageInfo>();

        public void Connectnew(string userName, string password)
        {
            if (userName == "" && password == "")
            {
                Clients.Caller.addconnection("false");
            }
            else
            {
                var IsUserAlreadyInList = (from s in UsersList where (s.UserName == userName) select s).FirstOrDefault();
                if (IsUserAlreadyInList == null)
                {
                    int CurrentUserId = Convert.ToInt32(Env.GetUserInfo("userid"));
                    UsersList.Add(new DtoUserInfo { ConnectionId = "", FromId = CurrentUserId, UserName = userName, UserGroup = 0 });
                    Clients.Caller.addconnection("true");
                }
            }



        }
        public void Connect(string userName, string password, int ToUser)
        {
            if (AjaxLoad.per())
            {

                var id = Context.ConnectionId;


                int Groupid = 0;//now static

                bool result = false;

                int CurrentUserId = Convert.ToInt32(Env.GetUserInfo("userid"));
            
                using (var db = new SIContext())
                {
                    var QueryGetGroup = db.Database.SqlQuery<DtoGrouping>("select COUNT( ChatGroupId ) as GroupCount ,ChatGroupId from ChatGroupUser where ChatUserId in(" + CurrentUserId + "," + ToUser + ") group by ChatGroupId");

                    if (QueryGetGroup != null)
                    {
                        foreach (var item in QueryGetGroup)
                        {
                            if (item.GroupCount > 1)
                            {
                                result = true;
                                Groupid = item.ChatGroupId;
                            }
                        }
                    }


                    if (result == true)
                    {
                        //it having a group then take group id and add in Groupid
                    }
                    else
                    {
                        //make group first
                        string sqlCreateGroup = "declare @GroupId int=null \n ";
                        sqlCreateGroup += "insert into ChatGroup(GroupCreatorBy,IsActive) values(" + CurrentUserId + ",1) set @GroupId=SCOPE_IDENTITY() \n ";
                        sqlCreateGroup += "insert into ChatGroupUser (ChatUserId,ChatGroupId) values(" + CurrentUserId + ",@GroupId) \n ";
                        sqlCreateGroup += "insert into ChatGroupUser (ChatUserId,ChatGroupId) values(" + ToUser + ",@GroupId) \n ";
                        sqlCreateGroup += "select @GroupId \n ";

                        var getgroupid = db.Database.SqlQuery<int>(sqlCreateGroup).FirstOrDefault();

                        Groupid = Convert.ToInt32(getgroupid.ToString());
                    }


                    try
                    {


                        var IsUserAlreadyInList = (from s in UsersList where (s.FromId == CurrentUserId) select s).FirstOrDefault();
                        //.FirstOrDefault(i => i.UserGroup != 0);
                        if (IsUserAlreadyInList == null)
                        {
                            UsersList.Add(new DtoUserInfo { ConnectionId = id, FromId = CurrentUserId, UserName = userName, UserGroup = Groupid });

                        }
                        else
                        {
                            IsUserAlreadyInList.ConnectionId = id;
                            IsUserAlreadyInList.UserGroup = Groupid;
                            IsUserAlreadyInList.FromId = CurrentUserId;
                        }
                        Groups.Add(Context.ConnectionId, Groupid.ToString());
                        string tousername = "<b>" + db.ApplicationUsers.FirstOrDefault(x => x.Id == ToUser).FirstName + "  " + db.ApplicationUsers.FirstOrDefault(x => x.Id == ToUser).LastName + "  (" + db.ApplicationUsers.FirstOrDefault(x => x.Id == ToUser).Department_DepartmentId.Title + ") </b>";
                       Clients.Caller.onConnected(id, userName, CurrentUserId, Groupid, tousername);

                        //
                        var messages = db.ChatMessages.OrderByDescending(i => i.SendDate).Take(150).Where(i => i.ChatGroupId == Groupid && i.IsDeleted == false).ToArray();
                        string strgroup = Groupid.ToString();



                        StringBuilder sbMessage = new StringBuilder();


                        foreach (var item in messages.OrderBy(i => i.SendDate))
                        {
                            string seen = "<i class=\"fa fa-times\"></i>";
                            if (item.IsSeen == true)
                            {
                                seen = "<i class=\"fa fa-check\"></i>";
                            }

                            if (item.SendFrom == CurrentUserId)
                                sbMessage.AppendLine("<div class=\"triangle-isosceles right\"> " + item.Msg + " <span class=\"MsgDate\"> " + item.SendDate.TimeAgo() + "  </span> <span class=\"MsgIsSeen\"> " + seen + "   </span> <div onclick=\"deleteMsg(" + item.Id + ")\" class=\"fa fa-bitbucket\"> </div> <span class=\"ClearFix\"></span> </div> ");
                            else
                                sbMessage.AppendLine("<div class=\"triangle-isosceles left\"> " + item.Msg + "<span class=\"MsgDate\"> " + item.SendDate.TimeAgo() + "  </span> <span class=\"ClearFix\"></span> </div>");
                        }

                        Clients.Group(strgroup).OnLoadMsgBinding(sbMessage.ToString(), strgroup, CurrentUserId);


                        //
                        try
                        {
                            string UpdateSql = "update ChatMessage set IsSeen=1 where Id in( select cm.Id from \n";
                            UpdateSql += "ChatMessage cm inner join ChatGroupUser cgu on cm.ChatGroupId=cgu.ChatGroupId where \n";
                            UpdateSql += "cm.IsSeen=0 and cgu.ChatUserId=" + CurrentUserId + " and cm.SendFrom !=" + CurrentUserId + " and cm.ChatGroupId=" + Groupid + ")";

                            db.Database.ExecuteSqlCommand(UpdateSql);
                            db.SaveChanges();
                        }
                        catch (Exception) { }

                        //


                        //
                        if (UsersList != null)
                        {
                            StringBuilder sbOnlineUsers = new StringBuilder();

                            foreach (var u in UsersList)
                            {
                                sbOnlineUsers.AppendLine("<div id='Us_" + u.FromId + "' class=\"list-group\" >  <a href=\"/Messages/To/" + u.FromId + "\"  class=\"list-group-item\"> <i class=\"fa fa-bolt fa-fw\"></i> " + u.UserName + " </a>  </div>");

                            }
                            Clients.All.OnlineUserList(sbOnlineUsers.ToString());
                        }

                    }

                    catch
                    {
                        string msg = "Tüm Yöneticiler meşgul, sabırlı olun ve lütfen tekrar deneyin";

                        Clients.Caller.NoExistAdmin();

                    }
                }
               
            }
        }

        //public void GetOnline()
        //{


        //    Clients.All.getonline();
        //}

        public void getonlineuser()
        {
            using (SIContext db = new SIContext())
            {
                StringBuilder sb = new StringBuilder();

                foreach (var item in UsersList)
                {
                    // var user = getuser(item.UserName); db.ApplicationUsers.FirstOrDefault(x => x.Username == name);
                    var user = db.ApplicationUsers.Where(t => t.Username == item.UserName).FirstOrDefault();
                    if (item.UserName.Trim() != Env.GetUserInfo("name").Trim())
                    {
                        sb.AppendLine("<div id='Us_" + user.Id + "' class=\"list-group\"  data-url=\"" + user.Id + "\"><span  class=\"list-group-item\">" + user.FirstName + " " + user.LastName + "( " +( user.Department_DepartmentId.Title.Length > 10 ? user.Department_DepartmentId.Title.Substring(0,10)+"..." :user.Department_DepartmentId.Title )+ " )<span  id=\"online_" + user.Id + "\" ><span style=\"color:green; top:-3px; right: 15px; position:relative; display:block;\" class=\"pull-right glyphicon glyphicon-one-fine-dot\"></span></span></span></div>");
                        //sb.AppendLine("<div id='Us_" + user.Id + "' class=\"list-group\"  data-url=\"" + user.Id + "\"><span  class=\"list-group-item\">" + user.FirstName + " " + user.LastName + "( " + user.Department_DepartmentId.Title + " )<span  id=\"online_" + user.Id + "\" ><span style=\"color:green;margin:-30px -0px 0px -30px;top:0px;right: 15px; position:relative; display:block;\" class=\"pull-right glyphicon glyphicon-one-fine-dot\"></span></span></span></div>");
                    }
                }
                Clients.Caller.onlinelist(sb.ToString());
            }


        }
        public void getalluser()
        {

            using(SIContext db=new SIContext()){
            StringBuilder sbUsers = new StringBuilder();
            //var Listusers = db.ApplicationUsers.OrderByDescending(i => i.Id).ToArray();

            var Listusers = db.ApplicationUsers.OrderBy(i => i.Department_DepartmentId.Title).ToArray(); 
            if (Listusers != null)
            {
                foreach (var item in Listusers)
                {
                    if (item.Username.Trim() != Env.GetUserInfo("name").Trim())
                        sbUsers.AppendLine("<div id='Us_" + item.Id + "' class=\"list-group\"  data-url=\"" + item.Id + "\"><span  class=\"list-group-item\">" + item.FirstName + " " + item.LastName + "( " +( item.Department_DepartmentId.Title.Length > 10 ? item.Department_DepartmentId.Title.Substring(0,10)+"..." :item.Department_DepartmentId.Title) +" )<span  id=\"online_" + item.Id + "\" ><span style=\"color:red;top:-3px;right: 0px; position:relative; display:block;\" class=\"pull-right glyphicon glyphicon-one-fine-dot\"></span></span></span></div>");
                }
            }



            Clients.Caller.alllist(sbUsers.ToString());
        }
        }

        public ApplicationUser getuser(string name) { 
        using(SIContext db=new SIContext()){

      return  db.ApplicationUsers.FirstOrDefault(x => x.Username == name);
      

        }
        
        }
        public void sendOnTyping(string userName, string gid, string co, string typing)
        {
            //Clients.Group(gid).SeeThis(userName, gid, co, typing);

        }

        public void onLoadfirst(string userName)
        {

            int CurrentUserId = Convert.ToInt32(Env.GetUserInfo("userid"));
            using (var db = new SIContext())
            {

                //
                var messages = (from cm in db.ChatMessages join cgu in db.ChatGroupUsers on cm.ChatGroupId equals cgu.ChatGroupId where cm.IsSeen == false && cgu.ChatUserId == CurrentUserId && cm.SendFrom != CurrentUserId select new { cm.Id, cgu.ChatUserId, cm.Msg, cm.SendDate, cm.SendFrom, SendFromName = cm.ChatUser_SendFrom.Username, cm.IsSeen, cm.IsDeleted }).ToArray();
                StringBuilder sbMsgList = new StringBuilder();
                int MsgCount = 0;
                if (messages != null)
                {

                    List<int> group = new List<int>();
                    foreach (var item in messages)
                    {
                        group.Add(item.SendFrom.Value);
                    }
                    var disGroup = group.Distinct();
                    MsgCount = disGroup.Count();
                    foreach (var g in disGroup)
                    {
                        var nextItem = messages.Where(i => i.SendFrom == g).OrderByDescending(i => i.SendDate).Take(1).ToArray();

                        foreach (var item in nextItem)
                        {
                            var subitem = db.ApplicationUsers.Where(t => t.Id == item.SendFrom).FirstOrDefault();
                            string username = "";
                            if (subitem.LastName != null &&  subitem.LastName != "")
                            {
                                username = subitem.FirstName + " " + subitem.LastName;
                                                            }
                            else
                            {
                                username = subitem.FirstName;
                            }
                            sbMsgList.AppendLine("<li> <a href=\"" + Extension.GetSiteRoot() + "/Messages/To/" + item.SendFrom + "\"> <h5>");
                            sbMsgList.AppendLine(" " + username + "  ");
                            sbMsgList.AppendLine("<small  > <i class=\"fa fa-clock-o\"></i>  " + item.SendDate.TimeAgo() + "  </small>");

                            if (item.Msg.Length > 20)
                                sbMsgList.AppendLine("</h5>  <p> " + item.Msg.Substring(0, 20) + " </p>");
                            else
                                sbMsgList.AppendLine("</h5>  <p> " + item.Msg + " </p>");

                            sbMsgList.AppendLine("</a>  </li> <li class=\"divider\"></li>");

                        }
                    }


                }
                //

                Clients.Caller.OnLoadingMsgList(sbMsgList.ToString(), MsgCount);
               
            }


        }

        //
        public void onLoadfirst1(string userName)
        {

          // int CurrentUserId = Convert.ToInt32(Env.GetUserInfo("userid"));
            int CurrentUserId = 0;
            
            using (var db = new SIContext())
            {
                var usr = db.ApplicationUsers.FirstOrDefault(x => x.Username == userName);
                CurrentUserId = usr.Id;
                //
                var messages = (from cm in db.ChatMessages join cgu in db.ChatGroupUsers on cm.ChatGroupId equals cgu.ChatGroupId 
                                where cm.IsSeen == false && cgu.ChatUserId == CurrentUserId && cm.SendFrom != CurrentUserId 
                                orderby cm.SendDate descending
                                select new { cm.Id, cgu.ChatUserId, cm.Msg, cm.SendDate, cm.SendFrom, SendFromName = cm.ChatUser_SendFrom.Username, cm.IsSeen, cm.IsDeleted }).ToArray();
                StringBuilder sbMsgList = new StringBuilder();
                int MsgCount = 0;
                if (messages != null)
                {

                    List<int> group = new List<int>();
                    foreach (var item in messages)
                    {
                        group.Add(item.SendFrom.Value);
                    }
                    var disGroup = group.Distinct();
                    MsgCount = disGroup.Count();
                    foreach (var g in disGroup)
                    {
                        var nextItem = messages.Where(i => i.SendFrom == g).OrderByDescending(i => i.SendDate).Take(1).ToArray();

                        foreach (var item in nextItem)
                        {
                            var subitem = db.ApplicationUsers.Where(t => t.Id == item.SendFrom).FirstOrDefault();
                            string username = "";
                            if (subitem.LastName != null || subitem.LastName != "")
                            {
                                username = subitem.FirstName + " " + subitem.LastName;
                            }
                            else
                            {
                                username = subitem.FirstName;
                            }
                            sbMsgList.AppendLine("<li> <a href=\"" + Extension.GetSiteRoot() + "/Messages/To/" + item.SendFrom + "\"> <h5>");
                            sbMsgList.AppendLine(" " + username + "  ");
                            sbMsgList.AppendLine("<small  > <i class=\"fa fa-clock-o\" ></i>  " + item.SendDate.TimeAgo() + "  </small>");

                            if (item.Msg.Length > 20)
                                sbMsgList.AppendLine("</h5>  <p> " + item.Msg.Substring(0, 20) + " </p>");
                            else
                                sbMsgList.AppendLine("</h5>  <p> " + item.Msg + " </p>");

                            sbMsgList.AppendLine("</a>  </li> <li class=\"divider\"></li>");
                        }
                    }


                }
                //

                Clients.User(userName).OnLoadingMsgList1(sbMsgList.ToString(), MsgCount);

            }


        }

        /// <summary>
        ///  Send notiifcation android--------------
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        //public string SendNotification(string deviceId, string message)
        //{
        //    string GoogleAppID = "AIzaSyBTBV3XLbx6jdVNVurDx2pQ4jDN_tGxpE4";
        //    var SENDER_ID = "609589726800";
        //    var value = message;
        //    WebRequest tRequest;
        //    tRequest = WebRequest.Create("https://android.googleapis.com/gcm/send");
        //    tRequest.Method = "post";
        //    tRequest.ContentType = " application/x-www-form-urlencoded;charset=UTF-8";
        //    tRequest.Headers.Add(string.Format("Authorization: key={0}", GoogleAppID));

        //    tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));

        //    string value1 = "MASSEMAIL";
        //    string postData = "collapse_key=score_update&time_to_live=108&delay_while_idle=1&data.message=" + value + "&data.title=" + value1 + "&data.time=" + System.DateTime.Now.ToString() + "&registration_id=" + deviceId + "";

        //    Console.WriteLine(postData);
        //    Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
        //    tRequest.ContentLength = byteArray.Length;

        //    Stream dataStream = tRequest.GetRequestStream();
        //    dataStream.Write(byteArray, 0, byteArray.Length);
        //    dataStream.Close();

        //    WebResponse tResponse = tRequest.GetResponse(); 
        //    dataStream = tResponse.GetResponseStream(); 
        //    StreamReader tReader = new StreamReader(dataStream); 
        //    String sResponseFromServer = tReader.ReadToEnd(); 
        //    tReader.Close();
        //    dataStream.Close();
        //    tResponse.Close();
        //    return sResponseFromServer;
        //} 



        public void SendMessageToGroup(string userName, string message, int gid, string co)
        {

            ////--- android 
            //SendNotification("APA91bF9nfyh0ipM2Ii5J-NA3inw5ZBtnR_4CLV-Tscs44AXjVrFBCu8AV_JWe5H2wi9ZYsnFqTJo7tbnWcCTKsnXJRaIznT43MhcWmU56qFNHhyf8h_j5MmKYDMoVE4HlijC66XxahS", message);




            string tu = "";
            if (UsersList.Count != 0)
            {
                var strg = (from s in UsersList where (s.UserName == userName) select s).First(i => i.UserGroup != 0);

                string strgroup = gid.ToString();

                //isSeen
                string seen = "<i class=\"fa fa-times\"></i>";
                bool isseen = false;
                var OnlineUsersList = (from s in UsersList where (s.UserGroup == gid) select s).ToArray();
                if (OnlineUsersList != null)
                {
                    if (OnlineUsersList.Count() > 1)
                    {
                        isseen = true;
                        if (isseen)
                        {
                            seen = "<i class=\"fa fa-check\"></i>";
                        }
                    }
                    else
                    {

                      
                        //send email to user if offline
                        BulkEmailController b = new BulkEmailController();
                        using (var db = new SIContext())
                        {
                            var user = db.ChatGroupUsers.Where(i => i.ChatGroupId == gid).ToArray();
                            string fromUser = string.Empty, ToUser = string.Empty;
                            foreach (var item1 in user)
                            {
                                if (userName == item1.ApplicationUser_ApplicationUserId.Email)
                                {
                                    fromUser = item1.ApplicationUser_ApplicationUserId.Email;
                                }
                                else
                                {
                                    ToUser = item1.ApplicationUser_ApplicationUserId.Email;
                                    tu = ToUser;
                                   
                                }
                            }

                            //String deviceID = db.ApplicationUsers.FirstOrDefault(i => i.Username == ToUser).deviceID;
                            //PushNotification.SendNotification(deviceID, "Yeni Bir Mesajınız Var: " + fromUser + " - " + message);

                            b.SendAsyncMessage(fromUser, ToUser, "Online Message", message);
                        }

                    }
                }


                //

                //

                Clients.Group(strgroup).getMessages(userName, message, gid, seen, DateTime.Now.TimeAgo());

                try
                {
                    using (var db = new SIContext())
                    {
                        var Q = db.Database.SqlQuery<int>("select Id from BlockList where UserId in(select ChatUserId from ChatGroupUser where ChatGroupId=" + gid + ") and BlockedUserId in(select ChatUserId from ChatGroupUser where ChatGroupId=" + gid + ")");

                        var IsMsg = db.ApplicationUsers.FirstOrDefault(i => i.Id == strg.FromId).CanMessage;
                        if (IsMsg == null)
                            IsMsg = false;
                        if (IsMsg.HasValue && IsMsg == false)
                        {
                            Clients.Group(strgroup).getMessages(userName, "Mesaj Yazma Yetkiniz Yoktur!", gid, seen, DateTime.Now.TimeAgo());
                        }
                        if (Q.Count() > 0)
                        {
                            Clients.Group(strgroup).getMessages(userName, "Blok Listesindesiniz.", gid, seen, DateTime.Now.TimeAgo());
                        }

                        if (IsMsg != false && Q.Count()==0)
                        {
                            SendMessageStark(message, gid, co, strg, isseen, db);
                           // Clients.User(tu).showalert(strg.UserName);
                            onLoadfirst1(tu);
                        }
                    }

                }
                catch (Exception)
                {
                }

            }

        }

        private static void SendMessageStark(string message, int gid, string co, DtoUserInfo strg, bool isseen, SIContext db)
        {
            ChatMessage con = new ChatMessage();
            con.Msg = message;
            con.ConnectionIDUser = co;
            con.SendDate = DateTime.Now;
            con.SendFrom = strg.FromId;
            con.ChatGroupId = gid;
            con.IsDeleted = false;
            //con.ImageFile=null //
            con.IsSeen = isseen;
            db.ChatMessages.Add(con);
            db.SaveChanges();
        }

        public override Task OnDisconnected(bool sws)
        {

            var item = UsersList.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
            if (item != null)
            {
                UsersList.Remove(item);

                var id = Context.ConnectionId;
                using (var db = new SIContext())
                {
                    //Clients.All.getoffline("#online_" + db.ApplicationUsers.FirstOrDefault(x=>x.Username==item.UserName).Id);
                }
                try
                {
                    var stradmin = (from s in UsersList where (s.UserGroup == item.UserGroup) select s).First();

                }
                catch
                {

                    Clients.Caller.NoExistAdmin();
                }


            }

            //Online Users list load rebind
            if (UsersList != null)
            {
                StringBuilder sbOnlineUsers = new StringBuilder();

                foreach (var u in UsersList)
                {
                    sbOnlineUsers.AppendLine("<div id='Us_" + u.FromId + "' class=\"list-group\" >  <a href=\"/Messages/To/" + u.FromId + "\"  class=\"list-group-item\"> <i class=\"fa fa-bolt fa-fw\"></i> " + u.UserName + " </a>  </div>");
                }

                Clients.All.OnlineUserList(sbOnlineUsers.ToString());
            }

           



            return base.OnDisconnected(true);
        }
    }
}